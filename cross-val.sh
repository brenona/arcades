#! /usr/bin/env bash

set -eux

cmd="${1:-""}"
train_dir="${2:-""}"

[[ -n "${cmd}" && -n ${train_dir} ]] || ( \
  echo "Usage: $0 <cmd> <data_dir> <start_fold:1> <end_fold:#dir>" 1>&2
  exit -1 \
)

train_dir="${train_dir%/}"
test_dir="${train_dir}-test"
start_fold=${3:-"1"}
end_fold="${4:-$(ls -1d "${train_dir}"/* | wc -l)}"

function trap_exit() {
  if [[ -d ${test_dir} ]]
  then
    mv -vn "${test_dir}"/* "${train_dir}" 2>/dev/null || true
    rmdir -v "${test_dir}"
  fi
}
trap trap_exit SIGINT SIGTERM ERR

mkdir "${test_dir}"

for I in $(seq ${start_fold} ${end_fold})
do
    test_data="$(ls -1d "${train_dir}"/* | head -n ${I} | tail -n1)"
    mv "${test_data}" "${test_dir}"

    echo "$(date +%Y-%m-%dT%H:%M:%S) Running '${cmd}'" >> cross-val-log.txt
    echo "Test data is : ${test_data}" >> cross-val-log.txt

    ${cmd} || exit $?

    echo "$(date +%Y-%m-%dT%H:%M:%S) End of command." >> cross-val-log.txt

    mv -vn "${test_dir}"/* "${train_dir}"
done

rmdir "${test_dir}"

