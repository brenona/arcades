# ARCADES
Author: Alexis BRENON, Date: 2017-12-05

## What is it?

ARCADES stands for Adaptive Reinforced Context-Aware Deep Decision System.
It is, at the same time, a proof a concept of using Deep Q-Network for decision making in smart-home, and a library implementing deep reinforcement learning.

For more informations, let's refer to these publications:

 * Brenon (2017). [Modèle profond pour la prise de décision adaptative en habitat intelligent](https://www.dropbox.com/s/xtx2ujlozbm6xvi/Alexis_BRENON_PhD.pdf)
 * Mnih et al. (2015). [Human-level control through deep reinforcement learning](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)

## How to use it?

This repository is composed of different branches. 
The `master` branch contains most of the source code:

  * The `arcades` folder contains the code of the library.
  * The `assets` folder contains some binary files used by the code.
  * The `data` folder contains some real data.
  * The `public` folder contains the generated documentation (if any).
  * More folders will come with different tools like `tsne` or `deepdream`.
  * Many scripts, detailed below.

The `xp/*` branches are related to an experiment.
Most of the time they are closed with a tag (and if not, they should) allowing reproducibility of the experiments.

At the root of the repository, you can find some scripts (shell or Lua ones).
Here is a brief description of each:

  * `install_dependencies.sh` try to install all dependencies (listed in **Dependencies**) required
  * `main.lua` load ARCADES and launch an experiment given its arguments. _ARCADES should be updated to rely on a configuration file (JSON or so) to avoid these tricky parsing and instantiations._
  * `run.lua` is a wrapper, which builds a well formed arguments list and finally calls `main.lua`.
  * `expe_template.lua` is a template file used to create experiments. It defines parameters and then call `run.lua`.
  * `debug.lua` is an instance of the `expe_template`, used to... Debug?
  * `test.lua` is an instance of the `expe_template` used to test code (short experiment).
  * `cross-val.sh` run multiple experiment and changing training and testing sets each time.
  * `cross-val-results.sh` looks for experiment results in a directory and build a resulting metrics file.

Finally, the `config.ld` file is the configuration file of the [LDoc](http://stevedonovan.github.io/ldoc/) utility used to build the documentation.

## Dependencies

This project rely on dependencies installed by the `install_dependencies.sh` script.
Some are Lua libraries, such as:

 * [`torch`](http://www.torch.ch): The neural network framework.
 * [`hash`](https://github.com/torch/hash): hashing functions (distributed by Torch).

Others are C libraries that you have to install yourself:

 * [libglib2.0-0](https://developer.gnome.org/platform-overview/stable/tech-glib.html): GLib objects management.
 * [libcairo2](https://cairographics.org/): graphics rendering (using GLib objects).
 * [librsvg2-2](https://wiki.gnome.org/Projects/LibRsvg): SVG rendering.
