--- Entry point to launch an experiment.
--
-- The run function takes care of the arguments table, and launch the main
-- script of the project.
-- @author Alexis BRENON <brenon.alexis@gmail.com>

local paths = require('paths')
local write = require('pl.pretty').write

local function run(args)

  -- Restrict the main script to the desired number of threads
  if args.torch.threads then
    local ffi = require('ffi')
    ffi.cdef("int setenv(const char *name, const char *value, int overwrite);")
    ffi.C.setenv("OMP_NUM_THREADS", tostring(args.torch.threads), 1)
  end

  local persist_command = ""
  if args.output.path then
    os.execute("mkdir -p " .. args.output.path)
    persist_command = string.format(
      " > >(tee %s) 2> >(tee %s >&2)",
      paths.concat(args.output.path, "output.log"),
      paths.concat(args.output.path, "error.log")
    )
  end

  local str_args = ""
  for k, v in pairs(args) do
    str_args = str_args .. string.format(' -%s %q', k, write(v, ""))
  end

  local command = "th ./main.lua" .. str_args .. persist_command
  print(command)

  os.execute(string.format("bash -c %q", command))
end

return run
