#! /usr/bin/env th

--- Entry point of the program.
--
-- Run this file from your computer with the right arguments to execute the
-- experiment.
-- @script main
-- @author Alexis BRENON <alexis.brenon@imag.fr>

--- @usage
local _ = [[
th ./main.lua <options>

See arcades/utils/argparse.lua for a full list of options
]]

local torch = require('torch')
local paths = require('paths')

package.path = "./?/init.lua;" .. package.path

local argparse = require('arcades.utils.argparse')
local setup = require('arcades.utils.setup')
local Logger = require('arcades.utils.Logger')

local function save_arguments(args)
  paths.mkdir(args.output.path)
  local args_file = torch.DiskFile(paths.concat(args.output.path, "args.lua"), "w")
  args_file:writeString("args = " .. require('pl.pretty').write(args))
  args_file:close()
end

local function save_git_commit(output_path)
  local git_commit = io.popen("git rev-parse HEAD", "r"):read()
  local git_branch_name = io.popen("git rev-parse --abbrev-ref HEAD", "r"):read()
  local git_file = torch.DiskFile(paths.concat(output_path, "commit.txt"), "w")
  git_file:writeString(string.format(
    "%s\n%s", git_commit, git_branch_name
  ))
  git_file:close()
end

local function save_components(experiment)
  Logger.main_logger:debug("Saving components")
  paths.mkdir(experiment.output.path)
  local file = torch.DiskFile(paths.concat(experiment.output.path, "components.log"), "w")
  file:writeString(tostring(experiment))
  file:close()
end


local function main()
  local parser = argparse.ArgumentParser()
  local args = parser:parse(arg)
  if args.output and args.experiment.params then
    args.output.progress_indicator = math.max(10, (args.experiment.params.eval_freq or 0)/25)
  end

  save_arguments(args)
  save_git_commit(args.output.path)
  local experiment = setup.setup(args)
  save_components(experiment)
  experiment:run()
end

main()
