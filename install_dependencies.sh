#!/usr/bin/env bash
set -eu

BASEDIR="$(cd "$(dirname "$0")"; pwd)"
PREFIX="${1:-${BASEDIR}/deps}"
echo "Installing dependencies into: ${PREFIX}"
mkdir -p "${PREFIX}"

######################################################################
# Torch install
######################################################################

if [[ "$(uname)" != 'Linux' ]]; then
  echo 'Platform unsupported, only available for Linux'
  exit
fi

# Check hard to install dependencies
command -v git &> /dev/null || { echo 'ERROR: git not found. Contact your administrator' >&2 ; exit 1; }
command -v curl &> /dev/null || { echo 'ERROR: curl not found. Contact your administrator' >&2 ; exit 1; }
[[ -n "$(dpkg-query -W libglib2.0-0 2>/dev/null)" || -n "$(find "${PREFIX}" -name 'libglib2*.so*')" ]] || {
  echo -en 'WARNING: libglib2.0-0 not found. You have to install it to use this software.
  Installation is going to continue. (Press Enter)' >&2
  read -r _
}
[[ -n "$(dpkg-query -W libcairo2 2>/dev/null)" || -n "$(find "${PREFIX}" -name 'libcairo*2*.so*')" ]] || {
  echo -en 'WARNING: libcairo2 not found. You have to install it to use this software.
  Installation is going to continue. (Press Enter)' >&2
  read -r _
}
[[ -n "$(dpkg-query -W librsvg2-2 2>/dev/null)" || -n "$(find "${PREFIX}" -name 'librsvg*2*.so*')" ]] || {
  echo -en 'WARNING: librsvg2-2 not found. You have to install it to use this software.
  Installation is going to continue. (Press Enter)' >&2
  read -r _
}

# Build and install Torch
th_luarocks=false
echo "Intalling Torch"
git config --global url.https://github.com/.insteadOf git://github.com/ # Use https instead of ssh
if [[ ! -x "$(command -v th)" ]]
  then
  th_git="$(mktemp -d)"
  th_luarocks="${PREFIX}/bin/luarocks"
  nvcc_found="$(command -v nvcc || true)"
  [[ -x "${nvcc_found}" ]] || echo "WARNING: nvcc not found. CUDA capabilities won't be available."

  (
  cd /tmp
  git clone https://github.com/torch/distro.git "${th_git}" --recursive 1> /dev/null
  cd "${th_git}"
  PREFIX="${PREFIX}" ./install.sh
  )
  rm -fr "${th_git}"

  # Base packages installed by Torch install script:
  # luafilesystem
  # penlight
  # lua-cjson
  # luaffifb
  # sundown
  # cwrap
  # paths
  # torch
  # dok
  # trepl
  # sys
  # xlua
  # nn
  # graph
  # nngraph
  # image
  # optim

  # cutorch
  # cunn

  # gnuplot
  # env
  # nnx
  # qtlua
  # qttorch
  # threads
  # argcheck

  echo ""
  echo "=> Torch has been installed successfully"
  echo "   Update your PATH now using the command line below:"
  echo ". $PREFIX/bin/torch-activate"
  echo ""
else
  echo ""
  echo "=> Torch already found"
  echo ""
  th_luarocks=luarocks
fi

# Install some Lua third libs
echo "Intalling Lua third libraries"
set -x
$th_luarocks install hash 1> /dev/null
set +x
echo ""
echo "=> Lua third libraries has been installed successfully"
echo ""

# Clean up
[[ "$(find "${PREFIX}" -mindepth 1 -maxdepth 1 | wc -l)" -eq 0 ]] && rmdir "${PREFIX}"
git config --global --unset url.https://github.com/.insteadof

true
