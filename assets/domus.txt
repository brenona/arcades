# URI	name	type	location	profile	comments

fffe2b37eab9	L1	Lamp	Kitchen	Dimmed	Ceiling lamp 100 W
fffecbb299a8	L2-L2bis	Lamp	Kitchen	Dimmed	Sink spots 2*50 W
fffeb88797bc	L3	Lamp	Bathroom	Dimmed	Ceiling lamp 25 W
fffedc6caa6a	SP1-SP2	Lamp	Study	Dimmed	North (wall) spots 2*50 W
fffe8cc9bad0	SP3-SP4	Lamp	Study	Dimmed	Sounth (window) spots 2*50 W
fffea4acfdcc	SP5-SP6	Lamp	Bedroom	Dimmed	North (wall) Spots 2*50 W
fffef9ef7d48	SP7-SP8	Lamp	Bedroom	Dimmed	South (window) Spots 2*50 W
fffeb6a85dbb	PCV1	Lamp	Bedroom	Dimmed	North (wall) bedside lamp 100 W
fffefa94abc7	PCV1bis	Lamp	Bedroom	Dimmed	South (window) bedside lamp 100 W
fffefb6453c9	PCV2	Lamp	Study	Dimmed	Table lamp 100 W

fffe67daa0c3	PC3	Power plug	Kitchen	Binary	Under sink
fffeaa3de849	PC4	Power plug	Kitchen	Binary	Under sink
fffe8ea1e6dd	PC5	Power plug	Kitchen	Binary	Under sink
fffed3e7eceb	PC6	Power plug	Bedroom	Binary	North-west plug
fffeda97bb69	PC7	Power plug	Bedroom	Binary	South-west plug
fffeb853ba76	PC8	Power plug	Kitchen	Binary	North (wall) table plug
fffeb67c8973	PC8bis	Power plug	Kitchen	Binary	South (window) table plug
fffeab498e8a	PC9	Power plug	Bedroom	Binary	North (wall) bedside plug
fffe9e7ea9a6	PC9bis	Power plug	Bedroom	Binary	South (window) bedside plug
fffeaa6a458e	PC10	Power plug	Study	Binary	North-east plug
fffebb9b539b	PC11	Power plug	Study	Binary	North-west plug
fffe98d7daa8	PC12	Power plug	Study	Binary	South-west plug

fffe63c3a7b2	VR1	Shutter	Kitchen	Shutter
fffe61dc66ba	VR2	Shutter	Bathroom	Shutter
fffec7396cb7	VR3	Shutter	Bedroom	Shutter	West shutter
fffec659b23d	VR4	Shutter	Bedroom	Shutter	East shutter
fffeabb51aca	VR5	Shutter	Study	Shutter	West shutter
fffed50b88c7	VR6	Shutter	Study	Shutter	East shutter
fffe3b6e8d53	CU1	Curtain	Bedroom	Shutter

fffe793bf9a3	Op1	Opening detector	Kitchen	Opening	Entry door
fffe8d5b3199	Op2	Opening detector	Bedroom	Opening	Kitchen / Bedroom door
fffecaaa7d8f	Op3	Opening detector	Study	Opening	Bedroom / Study door
fffe79b6b39c	Op4	Opening detector	Bathroom	Opening	Bedroom / Bathroom door

fffe1ec74cb8	Op5	Opening detector	Kitchen	Opening	Window
fffe9c5ebcd8	Op6	Opening detector	Bathroom	Opening	Window
fffecaba26b6	Op7	Opening detector	Bedroom	Opening	West window
fffeb595a7b1	Op8	Opening detector	Bedroom	Opening	East window
fffe8658caaa	Op9	Opening detector	Study	Opening	West window
fffe7aab3aa5	Op10	Opening detector	Study	Opening	East window

fffe17bab4ba	Op11	Opening detector	Bathroom	Opening	Left closet door
fffeaa5ace9d	Op12	Opening detector	Bathroom	Opening	Middle closet door
fffeb8a8da6a	Op13	Opening detector	Bathroom	Opening	Right closet door
fffe9b5b62c8	Op14	Opening detector	Kitchen	Opening	Closet door
fffe919b82b3	Op15	Opening detector	Kitchen	Opening	Fridge door

fffecc84d66b	P1	PIR	Kitchen	Presence
fffe9992f78d	P2	PIR	Study	Presence
fffea8eaa68b	P3	PIR	Bathroom	Presence
fffed97b3486	P4	PIR	Bedroom	Presence
fffe725aa973	P5	PIR	Kitchen	Presence
fffe6a74e37b	P6	PIR	Kitchen	Presence
fffe9289ac6e	P7	PIR	Bedroom	Presence
fffedddcca35	P8	PIR	Bathroom	Presence
fffe6d8b552a	P9	PIR	Study	Presence

fffe48ca9395	P1-Lum 	Luminosity	Kitchen	Luminosity
fffe659cdfb3	P2-Lum 	Luminosity	Study	Luminosity

fffe9da3a50a	Temp-Study	Air quality	Study	Temperature	Celsius degree
fffeb8a8ccbb	Temp-Bedroom 	Air quality	Bedroom	Temperature	Celsius degree
fffe97678aad	CO2-Bedroom concentration	Air quality	Bedroom	CO2	ppm
fffe3ce1bcaa	Hum1	Air quality	Bedroom	Relative Humidity	%

fffed998e65a	Global consumption since beginning	Electricity	None	Counting	kW/h
fffedbaaba48	Current	Electricity	None	Counting	A
fffedbb898de	Instant power on phase 1	Electricity	None	Counting	W/h
fffe755b9a41	Total instant power	Electricity	None	Counting	W/h
fffe7aa3a7ce	Voltage	Electricity	None	Counting	V

fffe395a4d9c	Hot water global consumption since beginning	Water	None	Counting	L
fffe8a746a6d	Hot water flow	Water	None	Counting	L/h
fffe3e7aa49b	Cold water global consumption since beginning	Water	None	Counting	L
fffedaabbcb5	Cold water flow	Water	None	Counting	L/h

*	microphone-1	Microphone	Kitchen	SNR	Audio SNR for channel 1 (kitchen south)
*	microphone-2	Microphone	Kitchen	SNR	Audio SNR for channel 2 (kitchen north)
*	microphone-3	Microphone	Bathroom	SNR	Audio SNR for channel 3 (bathroom)
*	microphone-4	Microphone	Bedroom	SNR	Audio SNR for channel 4 (bedroom south)
*	microphone-5	Microphone	Bedroom	SNR	Audio SNR for channel 5 (bedroom north)
*	microphone-6	Microphone	Study	SNR	Audio SNR for channel 6 (study south)
*	microphone-7	Microphone	Study	SNR	Audio SNR for channel 7 (study north)
