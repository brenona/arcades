# Context Représentation - assets
Auteur: Alexis BRENON, Date: 2016-08-23

Ce dossier contient les fichiers _binaires_ nécessaires à l'exécution du l'expérimentation.

 * `domus.svg` : plan de domus au format SVG
 * `domus.txt` : liste des capteurs représentés dans le plan (sous-ensemble de tous les capteurs disponibles)
