#! /usr/bin/env th

--- A small configuration to debug an agent
-- @script debug.lua
-- @author Alexis BRENON <brenon.alexis@gmail.com>

local args = {
  -- Torch
  torch = {
    gpu = arg[1] and (arg[1] == "gpu" and tonumber(arg[2])) or -1,
    threads=4,
    seed=3,
  },

  -- Output
  output = {
    path = paths.concat("outputs", "debug"),
    log_level=10,
    save_freq=250,
  },

  -- Environments
  training_environment = {
    class="DebugEnvironment",
    params = {
    },
  },
  testing_environment = {
    class="DebugEnvironment",
    params = {
    },
  },

  -- Agent
  agent = {
    class="NeuralQLearner",
    params = {
      -- Networks
      inference = {
        class = "DebugNetwork",
        params = {},
      },
      --  Experience pool params
      memory = {
        pool_size = 10,
        history_length = 1,
        history_type = 'linear',
        history_spacing = 1
      },

      learn_start=5,
      update_freq=1,
      minibatch_size=2,
      n_replay=1,
      rescale_r=false,

      max_reward=1,
      min_reward=-1,

      ep_start=1,
      ep_end=0.1,
      ep_endt=100,
      ep_eval=0.05,

      lr=0.01,

      discount=0.99,
      clip_delta=1,
      target_q=5,
      wc=0,
    },
  },

  -- Experiment
  experiment = {
    class="BaseExperiment",
    params = {
      steps=6,
      eval_freq=5,
      eval_steps=2,
    },
  },
}

require('run')(args)
