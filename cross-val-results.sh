#! /usr/bin/env bash

set -eu

BASEFOLDER=${1:-"./outputs"}
MINDATE="${2:-"$(date -Iseconds -d"@0")"}"
MAXDATE="${3:-"$(date -Iseconds)"}"
DATAFILES=""

[[ "${1}" = "-h" ]] && ( \
  echo "Usage: $0 [base_folder:./outputs [min_timestamp:$(date -Iseconds -d"@0") [max_timestamp:$(date -Iseconds) ]]]" 1>&2
  exit -1 \
)

function get_timestamp {
    date -d "$( \
        echo "$1" | \
        sed -re 's/^([[:digit:]]{4})-?([[:digit:]]{2})-?([[:digit:]]{2})[-Tt ]([[:digit:]]{2}):?([[:digit:]]{2}):?([[:digit:]]{2}).*$/\1-\2-\3T\4:\5:\6/'
    )" \
    +%s
}

MINDATE=$(get_timestamp "${MINDATE}")
MAXDATE=$(get_timestamp "${MAXDATE}")

for F in ${BASEFOLDER}/*
do
    FOLDER=$(basename "$F")
    if [[ $FOLDER =~ ^[-0-9-]*[-Tt][:0-9]* ]]
    then
        FDATE=$(get_timestamp "$FOLDER")
        if [[ $MINDATE -le $FDATE ]] \
            && [[ $FDATE -le $MAXDATE ]] \
            && [[ -e "$F/metrics.dat" ]]
        then
            DATAFILES="${DATAFILES}\n$F/metrics.dat"
        fi
    fi
done

DATAFILES="${DATAFILES#"\n"}"; DATAFILES="${DATAFILES%"\n"}"

LC_ALL=C

FILES_LIST="$(printf "$DATAFILES")"
FIRST_FILE="$(echo "$FILES_LIST" | head -1)"
NBFILES=$(echo "$FILES_LIST" | wc -l)

HEADER="$(head -n1 "$FIRST_FILE")"
STEPS="$(cut -f1 "${FIRST_FILE}" | tail -n+2)"
NB_METRICS=$(echo "${HEADER}" | tr '\t' '\n' | wc -l)

echo "${HEADER}"
for STEP in ${STEPS}; do
  STEP_METRICS="${STEP}"
  for METRIC_FIELD in $(seq 2 ${NB_METRICS}); do
    SUM="$(\
        grep -h "^${STEP}$(printf '\t')" $FILES_LIST | \
        cut -f${METRIC_FIELD} | \
        tr '\n' '+'
    )"
    SUM="${SUM#+}"; SUM="${SUM%+}"
    COMPUTATION="scale=2; (1/${NBFILES}) * ( ${SUM} )"
    RESULT=$(printf "%.2f" "$(echo "$COMPUTATION" | bc)") # Use printf to output the leading 0
    STEP_METRICS="${STEP_METRICS}\t${RESULT}"
  done
  printf "${STEP_METRICS}\n"
done

#################################
# Example of gnuplot usage
#
# blue_000 = "#A9BDE6" # = rgb(169,189,230)
# blue_025 = "#7297E6" # = rgb(114,151,230)
# red_050 = "#E62B17" # = rgb(230,43,23)
# set pointsize "1"
# set border 31 lw 2 lc rgb "#222222"
#
# set term pngcairo size 1024,768
# set ylabel "F1-score"
# set yrange [0:100]
# set xlabel "Learning steps (x1000)"
# set xrange [0:500]
#
# set output './metric.png'
# plot 'data_file.dat' u ($1/1000):2 t '' w lines lw 0.5 lc rgbcolor blue_000, '' u ($1/1000):2 t '' w points ps 1 pt 7 lc rgbcolor blue_025, '' u ($1/1000):2 t '' smooth bezier w lines lw 4 lc rgbcolor red_050
