#! /usr/bin/env th

--- An example of configuration to run a real experiment.
--
-- This configuration is intended to be copied and modified to suit your needs
-- and will become your experiment's start script.
-- @script expe_template.lua
-- @author Alexis BRENON <brenon.alexis@gmail.com>

local paths = require('paths')

local args = {
  -- Torch
  torch = {
    gpu = arg[1] and (arg[1] == "gpu" and tonumber(arg[2])) or -1,
    threads = 4,
    seed = 3,
    tensor_type = "DoubleTensor",
  },

  -- Output
  output = {
    path = paths.concat("outputs", os.date("%FT%T")),
    log_level = 10,
    save_freq = 200000,
  },

  -- Environments
  training_environment = {
    class = "smarthome.sweethome.GraphicalSensorSweetHome",
    params = {
      map_path = paths.concat("assets", "domus.svg"),
      history_length=3,
      deterministic = true,
    },
  },
  testing_environment = {
    class = "smarthome.sweethome.GraphicalSensorSweetHome",
    params = {
      map_path = paths.concat("assets", "domus.svg"),
      history_length = 3,
      deterministic = true,
    },
  },

  -- Agent
  agent = {
    class = "NeuralQLearner",
    params = {
      preprocess = {
        class = "Downsample",
        params = {
        },
      },
      inference = {
        class = "Inference",
        params = {
        },
      },
      experience_pool = {
        pool_size = 100,
        history_length=3,
      },
    }
  },

  -- Experiment
  experiment = {
    class = "BaseExperiment",
    params = {
      steps = 10e6,
      eval_freq = 100e3,
      eval_steps = 5000,
    },
  },
}

require('run')(args)
