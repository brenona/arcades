--- A set of different experiments which link an `environment` and an `agent`.
--
-- List of the classes in this package:
--
--  * @{experiment.BaseExperiment|BaseExperiment}
-- 
-- @package experiment
-- @mtodo Make `BaseExperiment` abstract and implement a `MonitoredExperiment` instead.
-- @author Alexis BRENON <alexis.brenon@imag.fr>

return require('arcades.utils.package_loader')(...)
