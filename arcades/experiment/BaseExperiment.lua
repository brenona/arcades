--- Classical (monitored) experiment class.
--
-- This class can be inherited to add more features
-- @classmod experiment.BaseExperiment
-- @alias class
-- @inherit true
-- @see ArcadesComponent
-- @mtodo Refactor class following `ArcadesComponent` architecture.
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local paths = require('paths')
local torch = require('torch')
local gnuplot = require('gnuplot')

local arcades = require('arcades')
local Logger = require('arcades.utils.Logger')

local module = {}
local class, super = torch.class('BaseExperiment', 'ArcadesComponent', module)

--- Data Types
-- @section data-types

--- Argument used for instanciation.
-- @tfield {train=environment.BaseEnvironment,test=environment.BaseEnvironment} environment Table with `train` and `test` @{environment.BaseEnvironment|environments}
-- @tfield agent.BaseAgent agent The @{agent.BaseAgent|agent} to use
-- @tfield OutputArgument output Output options
-- @tfield[opt=math.huge] number steps Total number of steps to do (excluding evaluation steps)
-- @tfield number eval_freq Number of steps between two evaluations
-- @tfield number eval_steps Number of evaluation steps
-- @tfield[opt] number save_at Next interation at which to save the experiment
-- @tfield[opt] {train=table,test=table} loop Results of the last iteration
-- @tfield[opt] {train=table,test=table} metrics Table of saved metrics
-- @tfield[opt] number step Current step
-- @mtodo Extract some arguments to `Dump`.
-- @table InitArgument

--- Dump.
-- @table Dump

--- Arguments to describe the output.
-- @table OutputArgument

--- Result of a set of interactions.
-- @tfield integer num_it Actual number of interactions done
-- @tfield {real=number,sys=number,user=number} time Times elapsed by interactions
-- @tfield integer num_rewards Number of non-zero rewards received
-- @tfield integer num_episodes Number of episodes (terminal states)
-- @tfield number total_reward Sum of rewards obtained
-- @tfield torch.Tensor/tensor.md/ confusion_matrix Confusion matrix
-- @tfield InteractionsTable interactions Details of interactions
-- @tfield ClassificationTable classification_metrics Sumed up metrics
-- @tfield {torch.Tensor/tensor.md/,...} inputs Inputs of the interactions
-- @table InteractionsResult

--- A list of interactions.
-- @tfield torch.Tensor/tensor.md/ expected_actions Actions expected by the environment
-- @tfield torch.Tensor/tensor.md/ actions Actions executed
-- @tfield torch.Tensor/tensor.md/ rewards Rewards obtained
-- @tfield torch.Tensor/tensor.md/ terminals Terminal signal of the input state
-- @table InteractionsTable

--- List of classification metrics computed.
--
-- See some explanations about metrics on this site:
-- [`http://blog.revolutionanalytics.com/2016/03/com_class_eval_metrics_r.html`](http://blog.revolutionanalytics.com/2016/03/com_class_eval_metrics_r.html)
-- @tfield number accuracy
-- @tfield number average_accuracy
-- @tfield number precision
-- @tfield number recall
-- @tfield number f1
-- @tfield number macro_precision
-- @tfield number macro_recall
-- @tfield number macro_f1
-- @tfield number micro_precision
-- @tfield number micro_recall
-- @tfield number micro_f1
-- @table ClassificationTable

--- Fields
-- @section fields

--- Table with `train` and `test` @{environment.BaseEnvironment|environments}
-- @tfield {train=environment.BaseEnvironment,test=environment.BaseEnvironment} self.environment

--- The @{agent.BaseAgent|agent} to use
-- @tfield agent.BaseAgent self.agent

--- Output options
-- @tfield OutputArgument self.output

--- Total number of steps to do (excluding evaluation steps)
-- @tfield number self.steps

--- Number of steps between two evaluations
-- @tfield number self.eval_freq

--- Number of evaluation steps
-- @tfield number self.eval_steps

--- A @{torch.Timer/timer.md/|Timer} used to time the experiment.
-- @tfield torch.Timer/timer.md/ self.timer

--- Next interation at which to save the experiment
-- @tfield number self.save_at

--- Results of the last iteration
-- @tfield {train=table,test=table} self.loop

--- Table of saved metrics
-- @tfield {train=table,test=table} self.metrics

--- @{torch.DiskFile/diskfile.md/|File} used as output.
-- @tfield torch.DiskFile/diskfile.md/ self.metrics_file

--- Current step
-- @tfield number self.step

--- @section end

--- Default constructor.
-- @tparam InitArgument args
function class:__init(args)
  super.__init(self, args)

  self.environment = args.environment or error("No environments provided.")
  self.agent = args.agent or error("No agent provided.")

  self.output = args.output
  self.steps = args.steps or math.huge
  self.eval_freq = args.eval_freq
  self.eval_steps = args.eval_steps

  self.timer = torch.Timer():stop():reset()
  self._no_dump_list.timer = true

  self.save_at = args.save_at or self.output.save_freq
  self.loop = args.loop or {
    train = {},
    test = {}
  }
  self.metrics = args.metrics or {
    train = {},
    test = {}
  }
  self.metrics_file = torch.DiskFile(paths.concat(self.output.path, "metrics.dat"), "w")
  self._no_dump_list.metrics_file = true
  self.metrics_file:writeString(
    "#1-step\t2-total_time (s)\t" ..
    "3-training_time (s)\t4-training_rate (it/s)\t" ..
    "5-training_reward\t6-training_episodes\t" ..
    "7-training_accuracy (%)\t8-training_avg_accuracy\t9-training_precision\t10-training_recall\t11-training_f1\t" ..
    "12-testing_time (s)\t13-testing_rate (it/s)\t" ..
    "14-testing_reward\t15-testing_episodes\t" ..
    "16-testing_accuracy (%)\t17-testing_avg_accuracy\t18-testing_precision\t19-testing_recall\t20-testing_f1\n"
  )
  self.step = args.step or 0
end

--- Public Methods
-- @section public-methods

--- Start the experiment.
--
-- This function will run `steps` learning interactions, separated by
-- `eval_steps` evaluation interactions each `eval_freq` interactions.
function class:run()
  self._logger:debug("Starting experiment...")
  local steps_decimal_length = math.ceil(math.log(self.steps, 10)) + 1
  local set_save_path
  if self.output.save_versions then
    set_save_path = function()
      self.save_path = paths.concat(
        self.output.path,
        string.format(
          "%." .. steps_decimal_length .. "f",
          self.step / (10 ^ steps_decimal_length)
        ):sub(3)
      )
      paths.mkdir(self.save_path)
    end
  else
    self.save_path = self.output.path
    set_save_path = function() end
  end

  if self.output.profiling then
    self._logger:info("Profiling the experiment")
    require('ProFi'):start()
  end
  self.timer:resume()

  -- Initial testing
  self.loop.train = self:_train(0)
  self.loop.test = self:_test(self.eval_steps)
  set_save_path()
  self:report():save()
  collectgarbage()
  while self.step < self.steps do

    self.loop.train = self:_train(self.eval_freq)
    self.step = self.step + self.eval_freq
    self.loop.test = self:_test(self.eval_steps)

    set_save_path()
    self:report():save()
    collectgarbage()
  end
  self.timer:stop()
  if self.output.profiling then
    require('ProFi'):stop()
    require('ProFi'):writeReport(
      paths.concat(
        self.output.path,
        "ProFi.txt"
      )
    )
  end
  self.metrics_file:close()
end

--- Report loop results.
-- @return `self`
function class:report()
  local _
  _ = self.output.report.metrics and self:_metrics_report()
  _ = self.output.report.text and self:_text_report()
  _ = self.output.report.torch and self:_torch_report()
  _ = self.output.report.graphical and self:_graphical_report()
  _ = self.output.report.input > 0 and self:_inputs_report()
  return self
end

--- Save the current experiment and dependencies.
-- @return `self`
function class:save()
  if self.step >= self.save_at or self.step >= self.steps then
    self.save_at = self.save_at + self.output.save_freq
    self._logger:debug("Saving experiment at '%s'", self.save_path)

    -- Build filenames
    local suffix = ""
    if self.output.save_versions then
      suffix = "_" .. self.step
    end
    local agent_filename = "agent" .. suffix .. ".t7"
    local environment_filename = "environment" .. suffix .. ".t7"
    local experiment_filename = "experiment" .. suffix .. ".t7"

    -- Get a serializable version of the experiment
    local dump = self:dump()

    -- Save agent and environments separately
    torch.save(
      paths.concat(self.save_path, agent_filename),
      dump._args.agent
    )
    torch.save(
      paths.concat(self.save_path, "train_" .. environment_filename),
      dump._args.environment.train
    )
    torch.save(
      paths.concat(self.save_path, "test_" .. environment_filename),
      dump._args.environment.test
    )
    -- Don't resave agent and environment, use their path instead
    dump.agent = paths.concat(self.save_path, agent_filename)
    dump._args.agent = dump.agent
    dump.environment.train = paths.concat(self.save_path, "train_" .. environment_filename)
    dump.environment.test = paths.concat(self.save_path, "test_" .. environment_filename)
    dump._args.environment = require('pl.tablex').copy(dump.environment)
    -- Save the experiment
    torch.save(
      paths.concat(self.save_path, experiment_filename),
      dump
    )
  end

  return self
end

--- Update the total number of steps to execute.
--
-- Use this function if you want to continue a previous experiment.
-- @tparam number steps New number of steps to do
-- @return `self`
function class:setSteps(steps)
  if self.steps < steps then
    for _, metric in pairs(self.metrics) do
      if self.output.metrics.rl then
        metric.reward_per_ep = (
        metric.reward_per_ep and
        metric.reward_per_ep:resize(math.ceil(steps/self.eval_freq))
        )
      end
      if metric.classification then
        metric.f1_score = (
        metric.f1_score and
        metric.f1_score:resize(math.ceil(steps/self.eval_freq))
        )
      end
    end
  end
  self.steps = steps

  return self
end

--- Private Methods
-- @section private-methods

--- Build and save metrics string.
-- @return `self`
function class:_metrics_report()
  local total_time = self.timer:time().real

  local training_time = self.loop.train.time.real
  local training_rate = self.loop.train.num_it / training_time
  local training_reward = self.loop.train.total_reward
  local training_episodes = self.loop.train.num_episodes
  local training_accuracy = self.loop.train.classification_metrics.accuracy * 100
  local training_avg_accuracy = self.loop.train.classification_metrics.average_accuracy * 100
  local training_precision = self.loop.train.classification_metrics.macro_precision * 100
  local training_recall = self.loop.train.classification_metrics.macro_recall * 100
  local training_f1 = self.loop.train.classification_metrics.macro_f1 * 100

  local testing_time = self.loop.test.time.real
  local testing_rate = self.loop.test.num_it / testing_time
  local testing_reward = self.loop.test.total_reward
  local testing_episodes = self.loop.test.num_episodes
  local testing_accuracy = self.loop.test.classification_metrics.accuracy * 100
  local testing_avg_accuracy = self.loop.test.classification_metrics.average_accuracy * 100
  local testing_precision = self.loop.test.classification_metrics.macro_precision * 100
  local testing_recall = self.loop.test.classification_metrics.macro_recall * 100
  local testing_f1 = self.loop.test.classification_metrics.macro_f1 * 100

  -- step, total_time (s),
  --   train_time (s), train_rate (it/s),
  --     train_reward, train_episodes,
  --     train_acc (%), train_avg_acc, train_precision, train_recall, train_f1
  --   test_time (s), test_rate (it/s),
  --     test_reward, test_episodes,
  --     test_acc (%), test_avg_acc, test_precision, test_recall, test_f1
  local metrics_line = string.format(
    "%d\t%.3f\t" ..
    "%.3f\t%.2f\t" ..
    "%d\t%d\t" ..
    "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t" ..

    "%.3f\t%.2f\t" ..
    "%d\t%d\t" ..
    "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n",
    self.step, total_time,
    training_time, training_rate,
    training_reward, training_episodes,
    training_accuracy, training_avg_accuracy, training_precision, training_recall, training_f1,

    testing_time, testing_rate,
    testing_reward, testing_episodes,
    testing_accuracy, testing_avg_accuracy, testing_precision, testing_recall, testing_f1
  )

  self.metrics_file:writeString(metrics_line)
  self.metrics_file:synchronize()

  return self
end

--- Build and print a quick textual report.
-- @return `self`
function class:_text_report()
  local training_time = self.loop.train.time.real
  local training_rate = self.loop.train.num_it / training_time
  local testing_time = self.loop.test.time.real
  local testing_rate = self.loop.test.num_it / testing_time
  local total_reward = self.loop.test.total_reward
  local num_episodes = self.loop.test.num_episodes
  local reward_per_ep = total_reward/num_episodes

  if self.output.metrics.generic then
    self._logger:info(
      "Generic metrics name;" ..
      "step;" ..
      "total_time (s);" ..
      "training_time (s);" ..
      "training_rate (it/s);" ..
      "testing_time (s);" ..
      "testing_rate (it/s)"
    )
    self._logger:info(
      "Generic metrics value;%d;%.3f;%.3f;%.2f;%.3f;%.2f",
      self.step, self.timer:time().real,
      training_time, training_rate,
      testing_time, testing_rate
    )
  end
  if self.output.metrics.rl then
    self._logger:info(
      "RL metrics name;" ..
      "total_reward;" ..
      "num_episodes;" ..
      "reward_per_ep"
    )
    self._logger:info(
      "RL metrics value;%d;%d;%.2f",
      total_reward, num_episodes, reward_per_ep
    )
  end
  if self.output.metrics.classification then
    self._logger:info(
      "Classification metrics name;" ..
      "accuracy (%);" ..
      "average_accuracy (%);" ..
      "macro_precision (%);" ..
      "macro_recall (%);" ..
      "macro_f1 (%)"
    )
    self._logger:info(
      "Classification metrics value;%.2f;%.2f;%.2f;%.2f;%.2f",
      self.loop.test.classification_metrics.accuracy * 100,
      self.loop.test.classification_metrics.average_accuracy * 100,
      self.loop.test.classification_metrics.macro_precision * 100,
      self.loop.test.classification_metrics.macro_recall * 100,
      self.loop.test.classification_metrics.macro_f1 * 100
    )
  end
  return self
end

--- Save Torch components.
-- @return `self`
-- @mtodo save NN weights
function class:_torch_report()
  if self.output.metrics.classification then
    torch.save(
      paths.concat(self.save_path, "test_confusion_matrix.t7"),
      self.loop.test.confusion_matrix
    )
    torch.save(
      paths.concat(self.save_path, "train_confusion_matrix.t7"),
      self.loop.train.confusion_matrix
    )
  end
  if self.output.metrics.rl then
    torch.save(
      paths.concat(self.save_path, "train_interactions.t7"),
      self.loop.train.interactions
    )
    torch.save(
      paths.concat(self.save_path, "test_interactions.t7"),
      self.loop.test.interactions
    )
  end
  return self
end

--- Save some informations graphically.
--
-- This function will plot some graph, save images or something like this about
-- the elements of the experiment.
-- @return `self`
function class:_graphical_report()
  self._logger:debug("Saving graphical representations.")
  for phase, metric in pairs(self.metrics) do
    if self.output.metrics.rl then
      metric.reward_per_ep = (
        metric.reward_per_ep or
        torch.Tensor(math.ceil(self.steps/self.eval_freq)+1):fill(math.huge)
      )
      metric.reward_per_ep[(self.step/self.eval_freq)+1] = (
        self.loop[phase].total_reward/self.loop[phase].num_episodes
      )
    end
    if self.output.metrics.classification then
      metric.f1_score = (
        metric.f1_score or
        torch.Tensor(math.ceil(self.steps/self.eval_freq)+1):fill(math.huge)
      )
      metric.f1_score[(self.step/self.eval_freq)+1] = (
        self.loop[phase].classification_metrics.macro_f1
      )
    end
  end
  if self.output.metrics.rl then
    self:_plot_reward_per_ep()
  end
  if self.output.metrics.classification then
    self:_plot_confusion_matrix()
    self:_plot_f1_score()
  end
  if self.output.metrics.nn then
    self:_plot_network_filters()
  end

  gnuplot.closeall()
  return self
end

--- Plot evolution of reward per episode
-- @return `self`
function class:_plot_reward_per_ep()
  -- Plot reward per episode
  local abscissa = torch.range(0, self.step, self.eval_freq)/1000
  local ordinate_train = self.metrics.train.reward_per_ep[{{1, (self.step/self.eval_freq)+1}}]
  local ordinate_test = self.metrics.test.reward_per_ep[{{1, (self.step/self.eval_freq)+1}}]
  gnuplot.pngfigure(paths.concat(self.save_path, "reward_per_ep.png"))
  gnuplot.raw("set terminal pngcairo size 1024,768")
  gnuplot.movelegend('right', 'bottom')
  gnuplot.ylabel('Average reward/ep')
  gnuplot.xlabel('Learning steps (x1000)')
  gnuplot.axis({0, (self.step + self.eval_freq)/1000, '', ''})
  gnuplot.plot(
    {"", abscissa, ordinate_train, 'with lines lw 0.5 lc rgb "#FC8D62"'}, -- Red
    {"train samples", abscissa, ordinate_train, 'with points ps 1 lc rgb "#8A4D36"'}, -- Dark Red
    {"", abscissa, ordinate_train, 'smooth bezier with lines lw 3 lc rgb "#FDCBB7"'}, -- Light Red
    {"", abscissa, ordinate_test, 'with lines lw 0.5 lc rgb "#8DA0CB"'}, -- Blue
    {"test samples", abscissa, ordinate_test, 'with points ps 1 lc rgb "#4D586F"'}, -- Dark Blue
    {"", abscissa, ordinate_test, 'smooth bezier with lines lw 3 lc rgb "#CBD3E7"'} -- Light Blue
  )
  gnuplot.plotflush()

  return self
end

--- Save a graphical version of confusion matrix.
-- @return `self`
function class:_plot_confusion_matrix()
  -- Save the confusion matrix as a PNG image
  local confusion_matrix = self.loop.test.confusion_matrix:clone()
  gnuplot.pngfigure(paths.concat(self.save_path, "confusion_matrix_test.png"))
  gnuplot.raw("set terminal pngcairo size 1024,768")
  gnuplot.raw("load '" .. paths.concat("assets", "viridis.pal") .. "'")
  gnuplot.raw("set cbrange[0:1]")
  gnuplot.ylabel('True action')
  gnuplot.xlabel('Predicted action')
  gnuplot.imagesc(
    confusion_matrix:cdiv( -- Normalize confusion matrix over rows
      confusion_matrix:sum(2):add(1e-6):expand(
        confusion_matrix:size(1),
        confusion_matrix:size(2)
      )
    ),
    'color'
  )
  gnuplot.plotflush()

  confusion_matrix = self.loop.train.confusion_matrix:clone()
  gnuplot.pngfigure(paths.concat(self.save_path, "confusion_matrix_train.png"))
  gnuplot.raw("set terminal pngcairo size 1024,768")
  gnuplot.raw("load '" .. paths.concat("assets", "viridis.pal") .. "'")
  gnuplot.raw("set cbrange[0:1]")
  gnuplot.ylabel('True action')
  gnuplot.xlabel('Predicted action')
  gnuplot.imagesc(
    confusion_matrix:cdiv( -- Normalize confusion matrix over rows
      confusion_matrix:sum(2):add(1e-6):expand(
        confusion_matrix:size(1),
        confusion_matrix:size(2)
      )
    ),
    'color'
  )
  gnuplot.plotflush()

  return self
end

--- Plot evolution of the F1-Score.
-- @return `self`
function class:_plot_f1_score()
  -- Plot reward per episode
  local abscissa = torch.range(0, self.step, self.eval_freq)/1000
  local ordinate_train = self.metrics.train.f1_score[{{1, (self.step/self.eval_freq)+1}}]
  local ordinate_test = self.metrics.test.f1_score[{{1, (self.step/self.eval_freq)+1}}]
  gnuplot.pngfigure(paths.concat(self.save_path, "f1_score.png"))
  gnuplot.raw("set terminal pngcairo size 1024,768")
  gnuplot.movelegend('left', 'top')
  gnuplot.ylabel('F1-score')
  gnuplot.xlabel('Learning steps (x1000)')
  gnuplot.axis({0, (self.step + self.eval_freq)/1000, 0, 1})
  gnuplot.plot(
    {"", abscissa, ordinate_train, 'with lines lw 0.5 lc rgb "#FC8D62"'}, -- Red
    {"train samples", abscissa, ordinate_train, 'with points ps 1 lc rgb "#8A4D36"'}, -- Dark Red
    {"", abscissa, ordinate_train, 'smooth bezier with lines lw 3 lc rgb "#FDCBB7"'}, -- Light Red
    {"", abscissa, ordinate_test, 'with lines lw 0.5 lc rgb "#8DA0CB"'}, -- Blue
    {"test samples", abscissa, ordinate_test, 'with points ps 1 lc rgb "#4D586F"'}, -- Dark Blue
    {"", abscissa, ordinate_test, 'smooth bezier with lines lw 3 lc rgb "#CBD3E7"'} -- Light Blue
  )
  gnuplot.plotflush()

  return self
end

--- Save some inputs.
-- 
-- This can be used for checks and/or post-mortem debug.
-- @return `self`
function class:_inputs_report()
  self._logger:debug("Saving input frames.")
  if #self.loop.train.inputs > 0 then
    image.savePNG(
      paths.concat(self.save_path, "train_inputs.png"),
      image.toDisplayTensor({
        input = self.loop.train.inputs,
        padding = 6
      })
    )
  end
  if #self.loop.test.inputs > 0 then
    image.savePNG(
      paths.concat(self.save_path, "test_inputs.png"),
      image.toDisplayTensor({
        input = self.loop.test.inputs,
        padding = 6
      })
    )
  end
  return self
end

--- Save a graphical representation of agent network filters
-- @return `self`
function class:_plot_network_filters()
  if self.agent.inference and self.agent.inference.network then
    local output_path = paths.concat(self.save_path, "filters")
    paths.mkdir(output_path)
    self.draw_filters(output_path, self.agent.inference.network)
  else
    self._logger:warn("Can't draw filters, no inference network found...")
  end

  return self
end

--- Dump images of the filters of the convolutionnal network.
-- @tparam string output_path Base output path for images
-- @tparam nn.Container/containers.md/ network Network to dump
function class.draw_filters(output_path, network)
  for i, net_module in ipairs(network:listModules()) do
    if torch.type(net_module.__metatable) == "nn.Module" and
       torch.type(net_module) == "nn.SpatialConvolution" then
      -- Draw filters of a Convolutionnal layer
      local image = require('image')
      local params = net_module:parameters()[1]
      local filters = torch.add(params, -params:min()):div(params:max()-params:min())
      filters = filters:view(torch.LongStorage({
        filters:size(1) * filters:size(2),
        1,
        filters:size(3), filters:size(4)
      }))
      image.save(
        paths.concat(
          output_path,
          string.format("Layer_%02d-conv.png", i)
        ),
        image.toDisplayTensor(filters, 2)
      )
    end
  end
end

--- Do some training interactions.
-- @tparam number steps Number of interactions to do
-- @treturn InteractionsResult Result of the interactions
function class:_train(steps)
  local timer = torch.Timer()
  self.environment.train:reset()
  self.agent:training()

  timer:reset()
  local interactions = self:_interact(self.environment.train, steps)
  timer:stop()

  local classification_metrics = {}
  if self.output.metrics.classification then
    classification_metrics = self._compute_classification_metrics(interactions.confusion_matrix)
  end

  local rewards = interactions.interactions.rewards
  local terminals = interactions.interactions.terminals
  local num_episodes
  if interactions.num_it == 0 then
    num_episodes = 0
  else
    num_episodes = (terminals:nonzero():dim() > 0 and terminals:nonzero():size(1) or 0) + 1
  end
  return {
    num_it = interactions.num_it,
    time = timer:time(),
    num_rewards = rewards:nonzero():dim() > 0 and rewards:nonzero():size(1) or 0,
    num_episodes = num_episodes,
    total_reward = rewards:sum(),
    confusion_matrix = interactions.confusion_matrix,
    interactions = interactions.interactions,
    classification_metrics = classification_metrics,
    inputs = interactions.inputs,
  }
end

--- Do some testing/evaluation interactions.
-- @tparam number steps Number of interactions to do
-- @treturn InteractionsResult Result of the interactions
function class:_test(steps)
  local timer = torch.Timer()
  self.environment.test:reset()
  self.agent:evaluate()

  timer:reset()
  local interactions = self:_interact(self.environment.test, steps)
  timer:stop()

  local classification_metrics = {}
  if self.output.metrics.classification then
    classification_metrics = self._compute_classification_metrics(interactions.confusion_matrix)
  end

  local rewards = interactions.interactions.rewards
  local terminals = interactions.interactions.terminals
  return {
    num_it = interactions.num_it,
    time = timer:time(),
    num_rewards = rewards:nonzero():dim() > 0 and rewards:nonzero():size(1) or 0,
    num_episodes = (terminals:nonzero():dim() > 0 and terminals:nonzero():size(1) or 0) + 1,
    total_reward = rewards:sum(),
    confusion_matrix = interactions.confusion_matrix,
    interactions = interactions.interactions,
    classification_metrics = classification_metrics,
    inputs = interactions.inputs,
  }
end

--- Actually do interactions.
--
-- This function does the interactions without checking anything prior to it
-- (agent mode, environment state, etc.). It resets the environment during the
-- interactions if necessary.
-- @tparam environment.BaseEnvironment environment Environment with witch to interact
-- @tparam number steps Number of interactions to execute
-- @treturn InteractionsResult Only a subset (`num_it`, `interactions`, `confusion_matrix`,
-- `inputs`) of fields are defined
function class:_interact(environment, steps)
  local num_it = 0
  local state, action, reward
  local interactions = {
    expected_actions = torch.zeros(steps),
    actions = torch.zeros(steps),
    rewards = torch.zeros(steps),
    terminals = torch.zeros(steps)
  }
  local env_num_actions = #(environment:actions())
  local confusion_matrix = torch.zeros(env_num_actions, env_num_actions)
  local inputs = {}

  for step = 1,steps do
    num_it = step
    if step % self.output.progress_indicator == 0 then Logger.main_logger:progress(step) end
    -- State
    state = environment:get_observable_state()
    self.agent:integrate_observation(state)
    local true_action = environment:get_true_action()
    -- Action
    action = self.agent:get_action()
    environment:perform_action(action)
    -- Reward
    reward = environment:get_reward()
    self.agent:give_reward(reward)

    if self.output.metrics.rl then
      interactions.expected_actions[step] = true_action
      interactions.actions[step] = action
      interactions.rewards[step] = reward
      interactions.terminals[step] = (state.terminal and 1) or 0
    end

    if self.output.metrics.classification and not state.terminal then
      confusion_matrix[true_action][action] = confusion_matrix[true_action][action] + 1
    end

    if step > steps - self.output.report.input then
      table.insert(inputs, state.observation)
    end

    if state.terminal then
      assert(action == 0)
      environment:reset()
    end
  end

  return {
    num_it = num_it,
    interactions = interactions,
    confusion_matrix = confusion_matrix,
    inputs = inputs
  }
end

--- Static Functions
-- @section static-functions

--- Compute classification metrics from a multi-classes confusion matrix.
--
-- See some explanations about metrics on this site:
-- [`http://blog.revolutionanalytics.com/2016/03/com_class_eval_metrics_r.html`](http://blog.revolutionanalytics.com/2016/03/com_class_eval_metrics_r.html)
-- @tparam torch.Tensor/tensor.md/ confusion_matrix A 2D matrix
-- @treturn ClassificationTable Classification metrics
function class._compute_classification_metrics(confusion_matrix)
  local diag = confusion_matrix:diag()
  local rowsums = confusion_matrix:sum(2):squeeze()
  local colsums = confusion_matrix:sum(1):squeeze()
  local total = confusion_matrix:sum()
  local num_classes = confusion_matrix:size(1)

  local accuracy =
    diag:sum() / -- Correctly classified instances over
    total -- Total number of instances

  local precision =
    torch.cdiv( -- Ratio of
      diag, -- Correctly classified instances for class i over
      colsums + 1e-9 -- all instances classified as class i
    )
  local recall =
    torch.cdiv( -- Ratio of
      diag, -- Correctly classified instances for class i over
      rowsums + 1e-9 -- all instances would should be classified as class i
    )
  local f1 =
    torch.cdiv(
      torch.cmul(
        precision,
        recall
      ):mul(2),
      (
        precision +
        recall
      ):add(1e-9)
    )

  local macro_precision, macro_recall, macro_f1
  do
    local non_zero_col = colsums:nonzero()
    local non_zero_row = rowsums:nonzero()
    if non_zero_col:nElement() > 0 then
      macro_precision =
        torch.gather(precision, 1, non_zero_col:resize(non_zero_col:nElement())):mean() -- Remove classes with no instances
    end
    if non_zero_row:nElement() > 0 then
      macro_recall =
        torch.gather(recall, 1, non_zero_row:resize(non_zero_row:nElement())):mean() -- Remove classes with no instances
    end
    if macro_precision and macro_recall then
      macro_f1 = (
        (2 * macro_precision * macro_recall) /
        (macro_recall + macro_precision + 1e-9)
      )
    end
  end

  local binary_classification_matrix = torch.Tensor(num_classes, 2, 2)
  for i=1,num_classes do
    binary_classification_matrix[i][1][1] =
      diag[i]
    binary_classification_matrix[i][1][2] =
      rowsums[i] - diag[i]
    binary_classification_matrix[i][2][1] =
      colsums[i] - diag[i]
    binary_classification_matrix[i][2][2] =
      total - rowsums[i] - colsums[i] - diag[i]
  end
  binary_classification_matrix = binary_classification_matrix:sum(1):squeeze()

  local average_accuracy =
    binary_classification_matrix:diag():sum() /
    binary_classification_matrix:sum()
  local micro_precision =
    binary_classification_matrix[1][1] /
    binary_classification_matrix:sum(1):squeeze()[1]
  local micro_recall =
    binary_classification_matrix[1][1] /
    binary_classification_matrix:sum(2):squeeze()[1]
  local micro_f1 =
    (2 * micro_precision * micro_recall) /
    (micro_recall + micro_precision + 1e-9)

  return {
    accuracy = accuracy,
    average_accuracy = average_accuracy,
    precision = precision,
    recall = recall,
    f1 = f1,
    macro_precision = macro_precision or 0/0, -- NaN
    macro_recall = macro_recall or 0/0, -- NaN
    macro_f1 = macro_f1 or 0/0, -- NaN
    micro_precision = micro_precision,
    micro_recall = micro_recall,
    micro_f1 = micro_f1
  }
end

--- @section end

return module.BaseExperiment
