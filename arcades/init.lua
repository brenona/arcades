--- Base of any ARCADES related object.
-- ArcadesComponent is a base class for any
-- ARCADES class, providing default behavior for logging,
-- string representation, dumping and loading.
-- @classmod ArcadesComponent
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local tablex = require('pl.tablex')

local Logger = require('arcades.utils.Logger')

local arcades = require('arcades.utils.package_loader')(...)
local class = torch.class('ArcadesComponent', arcades)

--- Data Types
-- @section data-types

--- Arguments used to instanciate a new object.
-- @mtodo Complete doc
-- @table InitArgument

--- Serializable dump of an object.
-- @mtodo Complete doc
-- @table Dump

--- Fields
-- @section fields

--- Table (shallow copy) of arguments used for instanciation
-- @tfield table self._args

--- Identifier name of the object
-- @tfield string self._name

--- List of attributes that does not need to be dumped
-- @tfield table self._no_dump_list

--- Object's logger
-- @see utils.Logger
-- @tfield utils.Logger self._logger

--- @section end

--- Object constructor.
--
-- Instanciate a new ARCADES object.
-- @tparam InitArgument args Table of attributes/arguments to pass to the object
-- @tparam Dump dump Dump of an object to re-instanciate
function class:__init(args, dump)
  args = args or {}; dump = dump or {}
  self._args = tablex.copy(args)
  self._name = string.format(
    "%s(%X)",
    torch.typename(self),
    torch.pointer(self)
  )
  self._no_dump_list = {}
  self._logger = Logger(self._name)
  self._logger:debug("Instanciating a new object")
end

--- Get a string representation of the object.
-- @treturn string A string representing the object
function class:__tostring__()
  local dump = self:dump()
  result = string.format(
    "%s(\n%s)",
    dump.classname,
    arcades.utils.utils.pretty_repr(dump._args)
  )
  return result
end

--- Public Methods
-- @section public-methods

--- Dump an object.
--
-- Build a serializable table containing enough information to instanciate
-- and initialize a new object at the same state than `self`.
-- @tparam[opt={}] table cycles Already dumped fields to avoid loops
-- @treturn Dump A serializable table
function class:dump(cycles)
  cycles = cycles or {}
  if self._logger then self._logger:debug("Dumping...") end
  cycles[torch.pointer(self)] = true
  local args_dump = class._dump(self._args, cycles) -- First dump the instanciation arguments
  local dump = class._dump(self, cycles) -- Then dump local attributes
  dump.classname = torch.typename(self) -- Used to identify the class from the dump
  dump._args = args_dump
  return dump
end

--- Load an object from a dump.
--
-- Given a @{dump}, find the associated class, instanciate an object and initialize it.
-- @tparam Dump dump Dump to restore
-- @return A newly instanciated object
function class.load(dump)
  --- Find a class path given a class name.
  --
  -- Recurcively look for a class named `classname` in loaded packages/modules.
  -- @tparam string classname Name of the class to find
  -- @tparam[opt=_G] package Base environment/package to search
  -- @treturn function Class constructor
  -- @error nil
  -- @todo Dump classpath to avoid this expensive search
  local function find_class_by_name(classname, package)
    package = package or _G
    for k, v in pairs(package) do -- look for each exported name
      if k == classname then
        return v
      elseif (
        string.lower(string.sub(k, 1, 1)) == string.sub(k, 1, 1) and -- packages are lower cased (while classes are upper cased)
        type(v) == "table"
      ) then -- search sub packages
        local constructor = find_class_by_name(classname, v)
        if constructor then return constructor end
      end
    end
    return nil
  end

  --- Recurcively load a dump.
  --
  -- Instanciate any object referenced in `dump` before actually loading it.
  -- @tparam Dump dump Dump to load
  -- @return Newly instantiated object
  local function recursive_load(dump)
    local class = find_class_by_name(dump.classname, arcades)
    if not class then
      error(string.format(
        "Unable to find '%s' class in arcades hierarchy.",
        dump.classname
      ))
    end

    for k, v in pairs(dump._args) do
      if (
        type(v) == "table" and
        v.classname and v._args
      ) then
        dump._args[k] = recursive_load(v)
      end
    end

    return class(dump._args, dump)
  end

  arcades.load_all() -- Import all packages of the arcades library
  return recursive_load(dump)
end

--- Static Functions
-- @section static-functions

--- Dump an object.
--
-- Dump any object, avoiding cycles.
-- The dump will contain any attribute excepted:
--
--   * functions
--   * attributes with name starting with `_` (underscore)
--   * attributes with name present in @{_no_dump_list}
--
-- If possible, tables and objects will be recurcively dumped.
-- @param[opt] o Object to dump
-- @tparam[opt={}] table cycles Already dumped tables/objects
-- @treturn table|nil A serializable table
function class._dump(o, cycles)
  if not o then return nil end
  cycles = cycles or {}
  local dump = {}
  for k, v in pairs(o) do
    if (
      type(v) ~= "function" and -- You cannot safely save a function
      string.sub(k, 1, 1) ~= "_" and -- Don't dump private fields
      (not (o._no_dump_list and o._no_dump_list[k])) -- Do not dump attributes listed as undumpable
    ) then
      if (
        (torch.typename(v) or torch.type(v) == "table") and
        cycles[torch.pointer(v)]
      ) then
        dump[k] = string.format(
          "cycle<%s>",
          v._name or "table("..torch.pointer(v)..")"
        )
      elseif ( -- Call dump function on Torch object if available
        torch.typename(v) and
        v.dump
      ) then
        dump[k] = v:dump(cycles)
      elseif (torch.type(v) == "table") then -- Recurcively dump tables
        cycles[v] = true
        dump[k] = class._dump(v, cycles)
      else
        dump[k] = v
      end
    end
  end
  return dump
end

return arcades
