--- A neural network for debug purpose.
-- This network acts like a convolutionnal neural network, taking a 3D input
-- and returning a 2D output with first dimension of size 1.
-- @classmod network.DebugNetwork
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')
local nn = require('nn')

local module = {}
local class, parent = torch.class('DebugNetwork', 'nn.Sequential', module)

--- Default constructor
function class:__init()
  parent.__init(self)

  local input_size = {1, 1, 1}
  local output_size = {1, 1}

  -- Assert input match the right dimensions
  self:add(nn.Reshape(unpack(input_size)))

  -- Convert multidimensionnal output to 1D input
  local nelements = self:forward(torch.zeros(unpack(input_size))):nElement()
  self:add(nn.Reshape(nelements))

  -- Output layer
  self:add(nn.Reshape(unpack(output_size)))
end

return module.DebugNetwork
