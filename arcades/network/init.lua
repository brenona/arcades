--- A module to build neural network from a @{NetworkDescription|description}.
--
-- An example of description can be found in this file: @{Inference.lua}
-- @module network
-- @alias module
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local module = require('arcades.utils.package_loader')(...)

local torch = require('torch')
local nn = require('nn')

--- Data Types
-- @section data-types

--- Description of a neural networks.
-- @tfield table input_size Size of the input @{torch.Tensor/tensor.md/|Tensor} `{d, w, h}`
-- @tfield {ConvolutionDescription,...} conv_layers List of @{nn.Convolution/convolution.md#nn.SpatialModules/|convolution} layers
-- @tfield {number,...} fc_layers List of width of @{nn.Linear/simple.md#nn.Linear/} fully connected layers
-- @tfield string nl_layer Name of the @{nn.Transfert/transfer.md/|transfert function} to use
-- @tfield table output_size Size of the output @{torch.Tensor/tensor.md/|Tensor} `{w, h}`
-- @table NetworkDescription

--- Description of a convolutionnal layer.
-- @tfield int n_filters Number of filters to use
-- @tfield DimenTable field_size Size of the receptive field
-- @tfield DimenTable stride Size of the strides
-- @tfield DimenTable zero_padding Size of the paddings
-- @table ConvolutionDescription

--- Description of a 2D dimension.
-- @tfield int width
-- @tfield int height
-- @table DimenTable

--- Public Methods
-- @section public-methods

--- Build a network.
-- @tparam NetworkDescription args Description of the network to build
-- @treturn nn.Sequential/containers.md#nn.Sequential/  Built @{nn.Module/containers.md#nn.Sequential/|network}
function module.create_network(args)

    local net = nn.Sequential()
    net:add(nn.View(unpack(args.input_size)))

    -- Convolutionnal layers
    for i, layer in ipairs(args.conv_layers) do
      net:add(nn.SpatialConvolution(
        (args.conv_layers[i-1] and args.conv_layers[i-1].n_filters) or args.input_size[1],
        layer.n_filters,
        layer.field_size.width, layer.field_size.height,
        layer.stride.width, layer.stride.height,
        layer.zero_padding.width, layer.zero_padding.height
      ))
      net:add(nn[args.nl_layer](true))
    end

    -- Convert multidimensionnal output to 1D input
    local nelements = net:forward(torch.zeros(1,unpack(args.input_size))):nElement()
    net:add(nn.View(nelements))

    -- Hidden Linear layers
    for _, layer_size in ipairs(args.fc_layers) do
      net:add(nn.Linear(nelements, layer_size))
      net:add(nn[args.nl_layer](true))
      nelements = layer_size
    end

    -- Output layer
    net:add(nn.Linear(nelements, torch.Tensor(args.output_size):prod()))
    net:add(nn.View(unpack(args.output_size)))
    return net
end

return module
