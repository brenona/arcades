--- Utilities functions.
--
-- A set of functions that are not tidly linked to the
-- arcades logic and that can be freely used.
-- @module utils.utils
-- @alias module
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local module = {}

--- Public Methods
-- @section public-methods

--- Randomly sample a value in a table.
-- @tparam table t List to sample from.
-- @return An element of the table
-- @treturn number The index of the returned element
function module.sample_in(t)
  local idx = math.random(1, #t)
  return t[idx], idx
end

--- Load a string representation of a table.
-- @param t Object to load
-- @treturn[1] table A table from the given string
-- @treturn[2] table `t` if it is a table
-- @treturn[3] nil
function module.load_table(t)
  if type(t) == "string" then
    local status, result = pcall(
      load,
      string.format(
        'return %s',
        t
      )
    )
    if status then
      status, result = pcall(result)
    end
    return status and result or nil
  elseif type(t) == "table" then
    return t
  else
    return nil
  end
end

--- Cast a value (if possible).
--
-- If this value is a string it will try to cast it to a number, a boolean, or
-- a table (casting its own values).
-- @param v The value to cast
-- @return The casted value
-- @treturn string The type of the returned value
function module.cast(v)
  if type(v) == "string" then
    local number
    number = tonumber(v);
    if number then return number, type(number) end

    local bool
    if string.match(v, "^[Tt][rue]*$") then bool = true elseif string.match(v, "^[Ff][alse]*$") then bool = false end
    if bool ~= nil then return bool, type(bool) end

    local tbl
    if string.sub(v, 1, 1) == '{' and string.sub(v, -1, -1) == '}' then
      tbl = module.load_table(v)
      for key, value in ipairs(tbl) do
        tbl[key] = module.cast(value)
      end
    end
    if tbl then return tbl, type(tbl) end
  end
  return v, type(v)
end

--- Load all arcades packages and modules.
-- @note Is it equivalent to `require('arcades').load_all()`?
function module.load_all()
    require('arcades.agent').load_all()
    require('arcades.environment').load_all()
    require('arcades.experiment').load_all()
    require('arcades.network').load_all()
    require('arcades.utils.placeholder').load_all()
end

-- Copy of the new_print Torch function but return a string
-- Special case, don't print big Tensors (more than 25 elements), just their size
local ndepth = 6
--- Build a prettyfied representation of anything
-- @param ... Whatever you want
-- @treturn string A pretty string
function module.pretty_repr(...)
  local result = {}
  local function rawprint(o)
    table.insert(result, tostring(o or '') .. '\n')
  end
  local function printtensor(obj)
    local size = ""
    for j = 1, obj:dim() do
      size = size .. obj:size(j)
      if j < obj:dim() then size = size .. "x" end
    end
    return "[" .. torch.type(obj) .. " of size " .. size .. "]"
  end
  local objs = {...}
  local function printrecursive(obj,depth)
    local depth = depth or 0
    local tab = depth*4
    local line = function(s) for i=1,tab do table.insert(result, ' ') end rawprint(s) end
    if next(obj) then
      line('{')
      tab = tab+2
      for k,v in pairs(obj) do
        if (
          string.match(torch.type(v), "torch%..*Tensor") and
          v:numel() > 25
        ) then
          line(tostring(k) .. ' : ' .. printtensor(v))
        elseif torch.type(v) == 'table' then
          if depth >= (ndepth-1) or next(v) == nil then
            line(tostring(k) .. ' : {...}')
          else
            line(tostring(k) .. ' : ') printrecursive(v,depth+1)
          end
        else
          line(tostring(k) .. ' : ' .. tostring(v))
        end
      end
      tab = tab-2
      line('}')
    else
      line('{}')
    end
  end
  for i = 1,select('#',...) do
    local obj = select(i,...)
    if type(obj) ~= 'table' then
      if (
        string.match(torch.type(obj), "torch%..*Tensor") and
        obj:numel() > 25
      ) then
        rawprint(printtensor(obj))
      elseif type(obj) == 'userdata' or type(obj) == 'cdata' then
        rawprint(obj)
      else
        table.insert(result, obj .. '\t')
        if i == select('#',...) then
          rawprint()
        end
      end
    elseif getmetatable(obj) and getmetatable(obj).__tostring then
      rawprint(obj)
    else
      printrecursive(obj)
    end
  end
  return table.concat(result)
end

return module
