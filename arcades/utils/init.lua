--- A set of utilities modules and classes.
--
-- List of sub-packages, modules and classes:
--   
-- List of sub-packages:
--
--   * @{utils.placeholder|placeholder}
--
-- List of modules:
--
--   * @{utils.ffi_cdef|ffi_cdef}
--   * @{utils.package_loader|package_loader}
--   * @{utils.setup|setup}
--   * @{utils.svg_renderer|svg_renderer}
--   * @{utils.utils|utils}
--
-- List of classes:
--
--   * @{utils.argparse.Parser|argparse.Parser}
--   * @{utils.Logger|Logger}
--
-- @package utils
-- @author Alexis BRENON <alexis.brenon@imag.fr>

return require('arcades.utils.package_loader')(...)
