--- Foreign function interface management.
--
-- Handle unique declaration of C functions
-- @module utils.ffi_cdef
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local ffi = require('ffi') -- Lua <-> C bindings

-- Do the FFI definition once
ffi.cdef([[
  void *rsvg_handle_new_from_data (const unsigned char * data, unsigned long data_len, void ** error);
  void *rsvg_handle_render_cairo	(void * handle, void * cr);

  typedef enum _cairo_format {
    CAIRO_FORMAT_INVALID   = -1,
    CAIRO_FORMAT_ARGB32    = 0,
    CAIRO_FORMAT_RGB24     = 1,
    CAIRO_FORMAT_A8        = 2,
    CAIRO_FORMAT_A1        = 3,
    CAIRO_FORMAT_RGB16_565 = 4,
    CAIRO_FORMAT_RGB30     = 5
  } cairo_format_t;
  void * cairo_image_surface_create (cairo_format_t format, int width, int height);
  void * cairo_create (void *target);
  void cairo_surface_flush (void *surface);
  unsigned char * cairo_image_surface_get_data (void *surface);
  void cairo_surface_destroy (void *surface);
  void cairo_destroy (void *cr);

  void g_object_unref (void *object);
]])

local function load_lib(alt_names)
  for _, name in ipairs(alt_names) do
    local status, output = pcall(ffi.load, name)
    if status then
      return output
    end
  end
end

---Table of the `gobject` library's function
-- @tfield table gobject 

---Table of the `cairo` library's function
-- @tfield table cairo 

---Table of the `rsvg` library's function
-- @tfield table rsvg 

--- @export
return {
  gobject = load_lib({
    'gobject',
    'libgobject-2.0.so',
    'libgobject-2.0.so.0'
  }),
  cairo = load_lib({
    'cairo',
    'libcairo.so.2',
  }),
  rsvg = load_lib({
    'rsvg',
    'librsvg-2.so.2',
  }),
}
