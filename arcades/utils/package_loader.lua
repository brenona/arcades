--- Package management.
--
-- Re-usable code to declare a package.
-- This package will automatically discover sub-packages and sub-modules
-- and export them on demand.
--
-- Each package contains a `load_all()` function which recurcivelly loads
-- all sub-packages and modules.
-- @usage
--  -- mypackage.lua
--  return require('arcades.utils.package_loader')(...)
--
-- @usage
--  -- myscript.lua
--  mypackage = import('mypackage').load_all()
-- @module utils.package_loader
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local paths = require('paths')

--- Build a filter function to find sub-modules.
-- @tparam string basepath Path of the current package
-- @treturn function A function which filter filenames below `basepath` to find sub-modules
local function filter_modules(basepath)
  return function(filename)
    return (
      paths.filep(paths.concat(basepath, filename)) and
      filename ~= "init.lua" and
      filename:match("^[^.].*%.lua$")
    )
  end
end

--- Build a filter function to find sub-packages.
-- @tparam string basepath Path of the current package
-- @treturn function A function which filter filenames below `basepath` to find sub-packages
local function filter_packages(basepath)
  return function(filename)
    return (
      paths.dirp(paths.concat(basepath, filename)) and
      paths.filep(paths.concat(basepath, filename, "init.lua")) and
      filename:match("^[^.]")
    )
  end
end

--- Public Methods
-- @section public-methods

--- Declare a package.
--
-- Declare a package named `package_name` and discover its sub-packages and modules.
-- @tparam string package_name Name of the package as used to `import` it
-- @treturn table The package
-- @function loader
local function loader(package_name)
  local package = {} -- Returned package
  local entities = {} -- Available modules and sub-packages (loaded on demand)

  local lua_package = require('package')
  local package_path = lua_package.searchpath(package_name, lua_package.path)
  local package_dir = paths.dirname(package_path)

  -- Discover modules of this package
  for entity in paths.files(package_dir, filter_modules(package_dir)) do
    entity = entity:gsub('%.lua', '')
    entities[entity] = "module"
  end

  -- Discover submodules of this package
  for entity in paths.files(package_dir, filter_packages(package_dir)) do
    entities[entity] = "package"
  end

  --- Load all sub-packages and modules.
  --
  -- Allow user to load all discovered modules and sub-packages.
  -- Useful for auto-completion in Torch
  function package.load_all()
    for name, type in pairs(entities) do
      local _ = package[name]
      if type == "package" then
        package[name].load_all()
      end
    end
    package.load_all = function() return package end
    return package
  end

  -- Define a metatable for automatic loading
  local metatable = {
    __index = function(table, index)
      if entities[index] then
        rawset(table, index, require(package_name .. "." .. index))
        return table[index]
      end
    end
  }

  return setmetatable(package, metatable)
end

return loader
