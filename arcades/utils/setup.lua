--- General configuration setup
-- @module utils.setup
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local ffi = require('ffi')
ffi.cdef("int setenv(const char *name, const char *value, int overwrite);")
local torch = require('torch')

local split = require('pl.utils').split

local arcades = require('arcades')
local Logger = require('arcades.utils.Logger')
local utils = require('arcades.utils.utils')

--- Private Methods
-- @section private-methods

--- Find a class in a given hierarchy.
-- @tparam string class_path A fully qualified class name
-- @tparam table base Hierarchy to search through
-- @treturn function Class constructor
-- @error nil The error message
-- @function _get_class
local function _get_class(class_path, base)
  base = base or _G
  local class = base
  local err, id
  for _, _id in ipairs(split(class_path, ".", true)) do
    if class == nil then
      id = _id
      break
    end
    class = class[_id]
  end

  if class == nil then
    err = string.format(
      "Unable to find '%s' when loading environment from '%s'",
      id,
      class_path
    )
  end

  return class, err
end

--- Initialize Torch dependant configuration.
--
-- The passed parameter `opt` is updated to reflect the actual values
-- that Torch returned after trying to configure it.
-- @tparam[opt={}] table opt
-- @tparam string opt.tensor_type Torch default tensor type
-- @tparam number opt.threads Number of threads torch can use
-- @tparam number opt.gpu GPU ID to use (-1 for CPU only, 0 for using GPU_ID environment variable)
-- @tparam number opt.seed Random number generator seed
-- @function _torchSetup
local function _torchSetup(opt)
  opt = opt or {}

  -- general setup
  torch.setdefaulttensortype("torch." .. opt.tensor_type)
  torch.setnumthreads(opt.threads)
  Logger.main_logger:info('Torch Threads: %d', torch.getnumthreads())

  -- set gpu device
  local cutorch = {}
  if opt.gpu and opt.gpu >= 0 then
    if opt.gpu == 0 then
      local gpu_id = tonumber(os.getenv('GPU_ID'))
      if gpu_id then opt.gpu = gpu_id+1 end
    end
    -- Restrict GPU usage
    if opt.gpu > 0 then
      ffi.C.setenv("CUDA_VISIBLE_DEVICES", tostring(opt.gpu-1), 1)
    end

    local _ = require('cunn') -- Load GPU backends
    cutorch = require('cutorch')

    opt.gpu = cutorch.getDevice()
    assert(opt.gpu > 0, "Unable to use a GPU...")
    Logger.main_logger:info(
      'Using GPU device: %d/%d',
      opt.gpu,
      cutorch.getDeviceCount())
  else
    opt.gpu = -1
    Logger.main_logger:info('Using CPU code only. GPU device id: %d', opt.gpu)
  end

  -- set up random number generators
  -- Seeding torch RNG with opt.seed and setting cutorch
  -- RNG seed to the first uniform random int32 from the previous RNG;
  -- this is preferred because using the same seed for both generators
  -- may introduce correlations; we assume that both torch RNGs ensure
  -- adequate dispersion for different seeds.
  opt.seed = opt.seed or os.time()
  math.randomseed(opt.seed)
  torch.manualSeed(opt.seed)
  Logger.main_logger:debug('Torch Seed: %f', torch.initialSeed())
  local firstRandInt = torch.random()
  if opt.gpu >= 0 then
    cutorch.manualSeed(firstRandInt)
    Logger.main_logger:debug('CUTorch Seed: %f', cutorch.initialSeed())
  end
end

--- Setup training and testing @{environment.BaseEnvironment|environments}.
-- @tparam table args Environments arguments
-- @treturn table `test` and `train` environments
-- @function _environment_setup
local function _environment_setup(args)
  local envs = {}
  local environment = require('arcades.environment')
  for _, env in ipairs({"train", "test"}) do
    if args[env.."ing_environment"] then
      local env_args = args[env.."ing_environment"]
      if env_args.class and env_args.class ~= "" then
        local env_class, err = _get_class(env_args.class, environment)
        if err then error(err) end
        envs[env] = env_class(env_args.params):reset()
      elseif env_args.dump and env_args.dump ~= "" then
        Logger.main_logger:debug("Loading an environment dump: " .. env_args.dump)
        envs[env] = arcades.ArcadesComponent.load(
          torch.load(env_args.dump)
        )
      end
    end
  end

  if not envs.test then
    envs.test = envs.train
  end

  local training_feature = envs.train:get_observable_state().observation
  local testing_feature = envs.test:get_observable_state().observation
  assert(
  training_feature:isSameSizeAs(testing_feature),
  "Training and testing environments must have the same features sizes... Aborting"
  )
  assert(
  #envs.train:actions() == #envs.test:actions(),
  "Training and testing environments must accept the same range of actions... Aborting"
  )
  require('image').savePNG(
    paths.concat(
      args.output.path,
      'training_features.png'
    ),
    training_feature)
  require('image').savePNG(
    paths.concat(
      args.output.path,
      'testing_features.png'
    ),
    testing_feature)

  return envs
end

--- Setup an @{agent.BaseAgent|agent}.
-- @tparam table args Agent arguments
-- @tparam table environments Environments in which agent will act
-- @treturn agent.BaseAgent Instanciated agent
-- @function _agent_setup
local function _agent_setup(args, environments)
  local agent

  if args.agent then
    local ag_args = args.agent
    if ag_args.class and ag_args.class ~= "" then
      local ag_class, err = _get_class(ag_args.class, require('arcades.agent'))
      if err then error(err) end
      if environments.train then
        ag_args.params.observation_size = (
          ag_args.params.observation_size or
          environments.train:get_observable_state().observation:size():totable()
        )
        ag_args.params.inference.params.output_size = (
          ag_args.params.inference.params.output_size or
          {
            1,
            #(environments.train:actions())
          }
        )
      end
      agent = ag_class(ag_args.params)
    elseif ag_args.dump and ag_args.dump ~= "" then
      Logger.main_logger:debug("Loading an agent dump: " .. ag_args.dump)
      agent = arcades.ArcadesComponent.load(
        torch.load(ag_args.dump)
      )
    end
  end

  return agent
end

--- Setup an @{experiment.BaseExperiment|experiment}.
-- @tparam table args Experiment arguments
-- @tparam agent.BaseAgent agent Agent used for experiment
-- @tparam table environments Environments used for experiment
-- @treturn experiment.BaseExperiment Instanciated experiment
-- @function _experiment_setup
local function _experiment_setup(args, agent, environments)
  local experiment

  if args.experiment then
    local exp_args = args.experiment
    if exp_args.class and exp_args.class ~= "" then
      local exp_class, err = _get_class(exp_args.class, require('arcades.experiment'))
      if err then error(err) end
      exp_args.params.environment = environments
      exp_args.params.agent = agent
      exp_args.params.output = args.output
      experiment = exp_class(exp_args.params)
    elseif exp_args.dump and exp_args.dump ~= "" then
      Logger.main_logger:debug("Loading an experiment dump: " .. exp_args.dump)
      local dump = torch.load(exp_args.dump)
      dump._args.environment = _environment_setup({
        training_environment = {dump = dump.environment.train},
        testing_environment = {dump = dump.environment.test}
      })
      dump._args.agent = _agent_setup(
        {agent = {dump = dump.agent}},
        dump._args.environment
      )
      experiment = arcades.ArcadesComponent.load(dump)
    end
  end

  return experiment
end

--- Public Methods
-- @section public-methods

--- Initialize configuration.
-- @tparam table opt Parsed command line arguments
-- @treturn experiment.BaseExperiment The loaded experiment, ready to run
-- @treturn table The updated `opt` table
-- @function setup
local function setup(opt)
  assert(opt)
  arcades.utils.utils.load_all()
  Logger.set_default_level(opt.output.log_level)

  -- first things first
  _torchSetup(opt.torch)

  local environments = _environment_setup(opt)
  local agent = _agent_setup(opt, environments)
  local experiment = _experiment_setup(opt, agent, environments)

  return experiment
end

--- @export
return {
  setup = setup
}
