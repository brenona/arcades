--- SVG symbols wrappers.
-- Classes used to easily change parts of an SVG document.
--
-- List of the packaged classes:
--
--   * @{utils.placeholder.PlaceHolder|PlaceHolder}
--   * @{utils.placeholder.IconPlaceHolder|IconPlaceHolder}
--   * @{utils.placeholder.ContinuousIconPlaceHolder|ContinuousIconPlaceHolder}
--   * @{utils.placeholder.DiscreteIconPlaceHolder|DiscreteIconPlaceHolder}
--   * @{utils.placeholder.GaugePlaceHolder|GaugePlaceHolder}
--   * @{utils.placeholder.IconPlaceHolderFactory|IconPlaceHolderFactory}
--   * @{utils.placeholder.PlaceHolderFactory|PlaceHolderFactory}
-- @package utils.placeholder
-- @author Alexis BRENON <alexis.brenon@imag.fr>

return require('arcades.utils.package_loader')(...)
