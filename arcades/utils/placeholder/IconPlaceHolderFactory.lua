--- Factory to build @{IconPlaceHolder|IconPlaceHolders}.
--
-- @classmod utils.placeholder.IconPlaceHolderFactory
-- @alias class
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local utils = require('arcades.utils.utils')
local placeholder = require('arcades.utils.placeholder')

local module = {}
local class, _ = torch.class('IconPlaceHolderFactory', module)

--- Static Functions
-- @section static-functions

--- Build an `IconPlaceHolder` from a given SVG element.
--
-- If no `IconPlaceHolder` can be built this function returns `nil`.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @treturn[1] IconPlaceHolder A sub-class of `IconPlaceHolder`
-- @return[2] `nil` if no `IconPlaceHolder` can be built
function class.get_PlaceHolder(element)
  -- Check that element has a description
  local description = element:child_with_name("svg:desc")
  if not description then return nil end

  local place_holder_spec = utils.load_table(description:get_text())
  if place_holder_spec.type ~= "icon" then return nil end

  if place_holder_spec.icon_type == "discrete" then return placeholder.DiscreteIconPlaceHolder(element, place_holder_spec) end
  if place_holder_spec.icon_type == "continuous" then return placeholder.ContinuousIconPlaceHolder(element, place_holder_spec) end

  return nil
end

return module.IconPlaceHolderFactory
