--- SVG icon placeholder base class.
--
-- These placeholders will contain a dynamic icon to represent
-- a sensor value. Relevant sensors are door switches, lamp,
-- blinds, etc.
--
-- @classmod utils.placeholder.IconPlaceHolder
-- @alias class
-- @inherit true
-- @see utils.placeholder.PlaceHolder
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local placeholder = require('arcades.utils.placeholder')
assert(placeholder.PlaceHolder)
local module = {}
local _, _ = torch.class('IconPlaceHolder', 'PlaceHolder', module)

return module.IconPlaceHolder
