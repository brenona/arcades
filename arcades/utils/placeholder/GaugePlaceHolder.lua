--- SVG gauge placeholder class.
--
-- These placeholders will contain a dynamic gradient used to represent
-- continuous values.
-- Relevant sensors are consumptions meters (water, power, etc.),
-- time (hour in day, day in month)(?), etc.
--
-- @classmod utils.placeholder.GaugePlaceHolder
-- @alias class
-- @inherit true
-- @see utils.placeholder.PlaceHolder
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>
local torch = require('torch')

local min = math.min
local max = math.max

local placeholder = require('arcades.utils.placeholder')
assert(placeholder.PlaceHolder)
local module = {}
local class, super = torch.class('GaugePlaceHolder', 'PlaceHolder', module)

--- Maximum value of the sensor.
-- @tfield number self.max_value

--- Minimum value of the sensor.
-- @tfield number self.min_value

--- Constructor.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @tparam table spec PlaceHolder's specification extracted from SVG description
function class:__init(element, spec)
  super.__init(self, element, spec)
  self.max_value = spec.max
  self.min_value = spec.min

  self.placed_values = { 0, 0 }
end

--- Private Methods
-- @section private-methods

--- Initialize @{PlaceHolder.string|string}
-- from given @{penlight/libraries/pl.xml.html/|XML element}.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element}
-- @override true
-- @see PlaceHolder._prepare_string
function class:_prepare_string(element)
  self.string = "<svg:linearGradient"
  self.string = self.string .. self.dump_attribs(element)
  self.string = self.string .. ">"
  for _, stop_tag in ipairs(element:get_elements_with_name('svg:stop')) do
    self.string = self.string .. "<svg:stop"
    self.string = self.string .. self.dump_attribs(stop_tag, {offset=true})

    self.string = self.string .. ' offset="%f"'

    self.string = self.string .. "/>"
  end
  self.string = self.string .. "</svg:linearGradient>"
end

--- Public Methods
-- @section public-methods

--- Change current value of the @{GaugePlaceHolder|placeholder}.
-- @tparam number value New value to apply
-- @treturn GaugePlaceHolder `self`
-- @treturn boolean Is the value modified
-- @override true
-- @see PlaceHolder:set_value
function class:set_value(value)
  value = max(min(value or self.value or self.min_value, self.max_value), self.min_value)
  local change_value = (value ~= self.value)
  if change_value then
    self.value = value
    local offset = (self.value - self.min_value)/(self.max_value - self.min_value)
    self.placed_values = { offset, offset }
    self.string_value = string.format(self.string, table.unpack(self.placed_values))
  end
  return self, change_value
end

--- Get a random value.
--
-- The value is sampled from the range `[``self.min_value``,``self.max_value``]`.
-- @treturn number A value in the range `[``self.min_value``,``self.max_value``]`
-- @override true
-- @see PlaceHolder:get_random_value
function class:get_random_value()
  return torch.random(self.min_value, self.max_value)
end

return module.GaugePlaceHolder
