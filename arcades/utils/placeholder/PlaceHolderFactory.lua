--- Factory to build @{PlaceHolder|PlaceHolders}.
--
-- @classmod utils.placeholder.PlaceHolderFactory
-- @alias class
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local utils = require('arcades.utils.utils')
local placeholder = require('arcades.utils.placeholder')

local module = {}
local class, _ = torch.class('PlaceHolderFactory', module)

--- Static Functions
-- @section static-functions

--- Build a `PlaceHolder` from a given SVG element.
--
-- If no `PlaceHolder` can be built this function returns `nil`.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @treturn[1] PlaceHolder A sub-class of `PlaceHolder`
-- @return[2] `nil` if no `PlaceHolder` can be built
function class.get_PlaceHolder(element)
  -- Check that element has a description
  local description = element:child_with_name("svg:desc")
  if not description then return nil end

  local place_holder_spec = utils.load_table(description:get_text()) or {}
  if place_holder_spec.type == "icon" then return placeholder.IconPlaceHolderFactory.get_PlaceHolder(element) end
  if place_holder_spec.type == "gauge" then return placeholder.GaugePlaceHolder(element, place_holder_spec) end

  return nil
end

return module.PlaceHolderFactory
