--- Continuous sensors SVG placeholder.
--
-- These placeholders will contain a dynamic icon to represent
-- the value of a continuous sensor.
-- Relevant sensors are lamp, thermometer, etc.
--
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>
-- @classmod utils.placeholder.ContinuousIconPlaceHolder
-- @alias class
-- @inherit true
-- @see utils.placeholder.IconPlaceHolder

local torch = require('torch')

local min = math.min
local max = math.max

local placeholder = require('arcades.utils.placeholder')
assert(placeholder.IconPlaceHolder)
local module = {}
local class, super = torch.class('ContinuousIconPlaceHolder', 'IconPlaceHolder', module)

--- Maximum value of the sensor.
-- @tfield number self.max_value

--- Minimum value of the sensor.
-- @tfield number self.min_value

--- Constructor.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @tparam table spec PlaceHolder's specification extracted from SVG description
function class:__init(element, spec)
  super.__init(self, element, spec)
  self.max_value = spec.max
  self.min_value = spec.min

  self.placed_values = {
    "display:none;",
    'active',
    "opacity:0"
  }
end

--- Private Methods
-- @section private-methods

--- Initialize @{PlaceHolder.string|string} and @{PlaceHolder.placed_values|placed_values}
-- from given @{penlight/libraries/pl.xml.html/|XML element}.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element}
-- @override true
-- @see PlaceHolder._prepare_string
function class:_prepare_string(element)
  self.string = "<svg:g"
  self.string = self.string .. self.dump_attribs(element, {style=true})
  self.string = self.string .. ' style="' ..
    (element:get_attribs().style and string.gsub(
      element:get_attribs().style, "display:none", ""
      ) or "") .. ' %s"'
  self.string = self.string .. ">"
  for _, use_tag in ipairs(element:get_elements_with_name('svg:use')) do
    local icon_basename
    local except = {}
    if use_tag:get_attribs().id == self.sensor_id then
      icon_basename = use_tag:get_attribs()["xlink:href"]
      icon_basename = string.gsub(icon_basename, "(.+/).*", "%1") -- Remove the state part of the icon ref
      except["xlink:href"] = true
      except["style"] = true
    end

    self.string = self.string .. "<svg:use"
    self.string = self.string .. self.dump_attribs(use_tag, except)

    if use_tag:get_attribs().id == self.sensor_id then
      self.string = self.string .. ' xlink:href="' .. icon_basename .. '%s"'
      self.string = self.string .. ' style="' .. (use_tag:get_attribs().style or "") .. ' %s"'
    end

    self.string = self.string .. "/>"
  end
  self.string = self.string .. "</svg:g>"
end

--- Public Methods
-- @section public-methods

--- Change current value of the @{ContinuousIconPlaceHolder|placeholder}.
-- @tparam number value New value to apply
-- @treturn ContinuousIconPlaceHolder `self`
-- @treturn boolean Is the value modified
-- @override true
-- @see PlaceHolder:set_value
function class:set_value(value)
  value = value and max(min(value, self.max_value), self.min_value) or value
  local change_value = (value ~= self.value)
  if change_value then
    self.value = value
    if not value then
      self.placed_values[1] = "display:none;"
    else
      local opacity_value = (self.value - self.min_value)/(self.max_value - self.min_value)
      if opacity_value < 0.001 then
        self.placed_values = {
          '',
          "inactive",
          "opacity:1"
        }
      else
        self.placed_values = {
          '',
          "active",
          string.format("opacity:%.3f", opacity_value)
        }
      end
    end
    self.string_value = string.format(self.string, table.unpack(self.placed_values))
  end

  return self, change_value
end

--- Get a random value from the @{ContinuousIconPlaceHolder|placeholder}.
--
-- The value is sampled from the range `[``self.min_value``,``self.max_value``]`.
-- @treturn number A value in the range `[``self.min_value``,``self.max_value``]`
-- @override true
-- @see PlaceHolder:get_random_value
function class:get_random_value()
  return torch.random(self.min_value, self.max_value)
end

return module.ContinuousIconPlaceHolder
