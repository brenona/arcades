--- Discrete sensors SVG placeholder.
--
-- These placeholders will contain a dynamic icon to represent
-- the value of a discrete sensor.
-- Relevant sensors are blinds, door switches, etc.
--
-- @classmod utils.placeholder.DiscreteIconPlaceHolder
-- @alias class
-- @inherit true
-- @see utils.placeholder.IconPlaceHolder
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local placeholder = require('arcades.utils.placeholder')
assert(placeholder.IconPlaceHolder)
local module = {}
local class, super = torch.class('DiscreteIconPlaceHolder', 'IconPlaceHolder', module)

--- Constructor.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @tparam table spec PlaceHolder's specification extracted from SVG description
function class:__init(element, spec)
  super.__init(self, element, spec)

  self.placed_values = {
    "display:none;",
    ""
  }
end

--- Private Methods
-- @section private-methods

--- Initialize @{PlaceHolder.string|string} and @{PlaceHolder.placed_values|placed_values}
-- from given @{penlight/libraries/pl.xml.html/|XML element}.
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element}
-- @override true
-- @see PlaceHolder._prepare_string
function class:_prepare_string(element)
  self.string = "<svg:g"
  self.string = self.string .. self.dump_attribs(element, {style=true})
  self.string = self.string .. ' style="' ..
    (element:get_attribs().style and string.gsub(
      element:get_attribs().style, "display:none", ""
      ) or "") .. ' %s"'
  self.string = self.string .. ">"
  for _, use_tag in ipairs(element:get_elements_with_name('svg:use')) do
    local icon_basename
    local except = {}
    if use_tag:get_attribs().id == self.sensor_id then
      icon_basename = use_tag:get_attribs()["xlink:href"]
      icon_basename = string.gsub(icon_basename, "(.+/).*", "%1") -- Remove the state part of the icon ref
      except["xlink:href"] = true
    end

    self.string = self.string .. "<svg:use"
    self.string = self.string .. self.dump_attribs(use_tag, except)

    if use_tag:get_attribs().id == self.sensor_id then
      self.string = self.string .. ' xlink:href="' .. icon_basename .. '%s"'
    end

    self.string = self.string .. "/>"
  end
  self.string = self.string .. "</svg:g>"
end

--- Public Methods
-- @section public-methods

--- Change current value of the @{DiscreteIconPlaceHolder|placeholder}.
-- @tparam number value New value to apply
-- @treturn DiscreteIconPlaceHolder `self`
-- @treturn boolean Is the value modified
-- @override true
-- @see PlaceHolder:set_value
function class:set_value(value)
  local change_value = (value ~= self.value)
  if change_value then
    self.value = value
    if not value then -- nil means no value (e.g. broken sensor)
      self.placed_values[1] = "display:none;"
    else -- Else, use 0 or 1
      self.placed_values = {
        '',
        value
      }
    end
    self.string_value = string.format(self.string, table.unpack(self.placed_values))
  end
  return self, change_value
end

-- luacheck: push no self

--- Get a random value from the @{DiscreteIconPlaceHolder|placeholder}.
--
-- The value is `0` or `1`.
-- @treturn int `0` or `1`
-- @override true
-- @see PlaceHolder:get_random_value
function class:get_random_value()
  return torch.random(0, 1)
end
-- luacheck: pop

return module.DiscreteIconPlaceHolder
