--- SVG placeholder base class.
--
-- @classmod utils.placeholder.PlaceHolder
-- @alias class
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local module = {}
local class, _ = torch.class('PlaceHolder', module)

--- Fields
-- @section fields

--- Identifier of the sensor represented by this `PlaceHolder`.
-- @string self.sensor_id

--- Current value of the `PlaceHolder`.
-- @field self.value

--- String representation of the current `value`.
-- @tfield string self.string_value

--- List of values to used to format `string` to build `string_value`.
-- @tfield table self.placed_values

--- @{string_value|String representation} template.
-- @tfield string self.string

--- @section end

--- Placeholder constructor.
--
-- @tparam Doc element @{penlight/libraries/pl.xml.html/|XML element} to parse
-- @tparam table spec PlaceHolder's specification extracted from SVG description
function class:__init(element, spec)
  self.sensor_id = spec.sensor_id
  self.value = nil
  self.string_value = nil
  self.placed_values = {}

  self:_prepare_string(element)
end

--- Return the string representation of the `PlaceHolder`.
--
-- If available, just return the current @{string_value|string representation}
-- or build and save it if necessary.
-- @treturn string `string_value`
function class:__tostring__()
  self.string_value = self.string_value or string.format(self.string, table.unpack(self.placed_values))
  return self.string_value
end

--- Abstract Methods
-- @section abstract-methods

--- Change current value of the `PlaceHolder`.
function class:set_value()
  error(string.format(
    "%s: abstract method not implemented.",
    torch.typename(self)
  ),
  2)
end

--- Get a random value from the `PlaceHolder`.
--
-- The value is sampled from the range of acceptable values
-- for this kind of `PlaceHolder`.
function class:get_random_value()
  error(
    string.format(
      "%s: abstract method not implemented.",
      torch.typename(self)
    ),
  2)
end

--- Initialize `string` and `placed_values` from given 
-- @{penlight/libraries/pl.xml.html/|XML element}.
-- @tparam Doc _ @{penlight/libraries/pl.xml.html/|XML element}
function class:_prepare_string(_)
  error(string.format(
    "%s: abstract method not implemented.",
    torch.typename(self)
  ),
  2)
end

--- Static Functions
-- @section static-functions

--- Dump attributes of an XML element.
--
-- Use this function to retrieve attributes of an
-- element if you need to build a new one with same attributes.
-- You can leave out some of them passing a map indexed with
-- names of attributes to dismiss.
-- @tparam Doc e @{penlight/libraries/pl.xml.html/|XML element}
-- @tparam[opt={}] {[string]=boolean} except Set of attributes to dismiss
-- @treturn string Suitable string to append to an XML string
function class.dump_attribs(e, except)
  except = except or {}
  local result = ""
  for k, v in pairs(e:get_attribs()) do
    if not except[k] then
      result = string.format(
      '%s %s="%s"',
      result,
      string.gsub(k, "%%", "%%%%"),
      string.gsub(v, "%%", "%%%%")
      )
    end
  end
  return result
end

return module.PlaceHolder
