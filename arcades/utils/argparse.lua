--- Argument parser class.
-- Declare arguments used by ARCADES.
-- @todo Don't rely on a class declaration (just a module instead).
-- @todo Use config file instad of complicated command line arguments.
-- @author Alexis BRENON <alexis.brenon@imag.fr>
-- @classmod utils.argparse.Parser
-- @alias class

local torch = require('torch')
local paths = require('paths')

local utils = require('arcades.utils.utils')

local module = {}
local class = torch.class("ArgumentParser", module)

--- Default constructor
function class:__init()
  self.parser = torch.CmdLine()
  self.parser_descriptor = {
    {"text", ""},
    {"text", "Experiment entry point:"},
    {"text", ""},
    {"text", "Options:"},

    {"option", "torch", "{}", "Torch parameters:", "table",
      fields = {
        {"field", "tensor_type", "DoubleTensor", "Default tensor type used in Torch", "string"},
        {"field", "threads", 1, "Number of BLAS threads", "number"},
        {"field", "gpu", -1, "ID of the GPU to use (-1 = none)", "number"},
        {"field", "seed", 1, "Fixed input seed for repeatable experiments", "number"},
      }
    },

    {"option", "output", "{}", "Output/Save parameters:", "table",
      fields = {
        {"field", "path", paths.concat("outputs", os.date("%Y%m%dT%H%M%S")), "Folder used for output", "string"},
        {"field", "log_level", 20, "The lower the level the more info {10|20|30|40|50}", "number"},
        {"field", "profiling", false, "Do you profile the experiment", "boolean"},
        {"field", "save_freq", 5*10^4, "Frequency of experiment saving", "number"},
        {"field", "save_versions", true, "Save experiment in a new files each time", "boolean"},
        {"field", "report", "", "Which kind of report to produce", "table",
          fields = {
            {"field", "metrics", true, "Produce TSV metrics report", "boolean"},
            {"field", "text", true, "Produce text/log report", "boolean"},
            {"field", "graphical", true, "Produce graphical report", "boolean"},
            {"field", "torch", true, "Produce torch object report", "boolean"},
            {"field", "input", 3, "Output some encountered input frames", "number"},
          },
        },
        {"field", "metrics", "", "Which kind of metrics to report", "table",
          fields = {
            {"field", "generic", true, "Report generic metrics (execution time and co.)", "boolean"},
            {"field", "rl", true, "Report reinforcement metrics", "boolean"},
            {"field", "classification", true, "Report classification metrics", "boolean"},
            {"field", "nn", true, "Report neural networks weights", "boolean"},
          },
        },
      },
    },

    {"text", ""},

    {"option", "training_environment", "{}", "Training environment parameters:", "table",
      fields = {
        {"field", "class", "", "Environment class to use", "string"},
        {"field", "params", "", "Environment parameters (see class documentation)", "table"},
        {"field", "file", "", "File to load as environment dump", "string"},
      }
    },

    {"option", "testing_environment", "{}", "Testing environment parameters:", "table",
      fields = {
        {"field", "class", "", "Environment class to use", "string"},
        {"field", "params", "", "Environment parameters (see class documentation)", "table"},
        {"field", "dump", "", "File to load as environment dump", "string"},
      }
    },

    {"option", "agent", "{}", "Agent parameters:", "table",
      fields = {
        {"field", "class", "", "Agent class to use", "string"},
        {"field", "params", "", "Agent parameters (see class documentation)", "table"},
        {"field", "dump", "", "File to load as agent dump", "string"},
      }
    },

    {"option", "experiment", "{}", "Experiment parameters:", "table",
      fields = {
        {"field", "class", "", "Experiment class to use", "string"},
        {"field", "params", "", "Experiment parameters (see class documentation)", "table"},
        {"field", "dump", "", "File to load as experiment dump", "string"},
      }
    },
  }

  self:_add_elements(self.parser_descriptor)
end

local indent = ""

--- Private Methods
-- @section private-methods

--- Add elements from a table to the parser.
--
-- There are 3 different element type:
--
--   * `text` which is just displayed in the usage description;
--   * `option` that is the top level option (it must be preceded by a dash);
--   * `field` if it is a suboption that is contained in a table.
--
-- Text elements are of the form:<br/>
-- `{"text", "my very useful piece of text"}`
--
-- Option and field elements are of the form:<br/>
-- `{"option"|"field", "option_name", default value, "Description", "type"}`
--
-- @tparam table elements A table representing the element.
function class:_add_elements(elements)
  local old_indent = indent
  indent = indent .. "  "

  for _, e in ipairs(elements) do
    if e[1] == "text" then
      self.parser:text(e[2])
    elseif e[1] == "option" then
      -- if e[5] == "table" and not e[3] then
      self.parser:option("-"..e[2], e[3], e[4])
    elseif e[1] == "field" then
      -- if e[5] == "table" and not e[3] then
      self.parser:text(string.format(
        "%s%s: %s [%s]",
        indent, e[2], e[4], e[3]
      ))
    end

    if e[1] == "option" or e[1] == "field" and e[5] == "table" and e.fields then
      self:_add_elements(e.fields)
    end
    if e[1] == "option" then
      self.parser:text("")
    end
  end
  indent = old_indent
end

--- Parse a string in depth.
--
-- Given the description of the arguments and the actual parsing result, it will
-- go deeper, casting string to the right type. This function works recursivelly.
-- @tparam table description The description of the arguments
-- @tparam table input Result of the previous parsing
-- @treturn table `input` parsed according to the `description`
function class:_format(description, input)
  local output = {}
  for _, e in ipairs(description) do
    -- Loop through elements of the description, discarding text elements
    if e[1] == "option" or e[1] == "field" then
      -- String option, nothing to do, just use default value if necessary
      if e[5] == "string" then
        output[e[2]] = tostring(input[e[2]] or e[3])
      -- Number option, try to cast it
      elseif e[5] == "number" then
        output[e[2]] = tonumber(input[e[2]] or e[3])
      -- Boolean option, cast it supporting values like 'true', 'TRUE', 'T', or '1'
      elseif e[5] == "boolean" then
        if type(input[e[2]]) ~= "boolean" then
          input[e[2]] = tostring(input[e[2]] or e[3])
          if string.sub(input[e[2]], 1, 1):upper() == "T" or input[e[2]] == "1" then
            output[e[2]] = true
          elseif string.sub(input[e[2]], 1, 1):upper() == "F" or input[e[2]] == "0" then
            output[e[2]] = false
          else
            output[e[2]] = nil
          end
        else
          output[e[2]] = input[e[2]]
        end
      end
      -- Table option, go deeper
      if e[5] == "table" then
        -- First load the table if possible
        input[e[2]] = utils.load_table(input[e[2]] or "{}")
        -- Then cast its element...
        if e.fields then
          -- ... given a format if available
          output[e[2]] = self:_format(e.fields, input[e[2]])
        else
          -- ... or try to guess
          output[e[2]] = {}
          for k, v in pairs(input[e[2]]) do
            output[e[2]][k] = utils.cast(v)
          end
        end
      end
    end
  end
  return output
end

--- Public Methods
-- @section public-methods

--- Actually parse `arg`
-- @tparam string arg Arguments string to parse
-- @treturn table Parsed arguments
function class:parse(arg)
  self.parsed = self.parser:parse(arg)
  self.parsed = self:_format(self.parser_descriptor, self.parsed)
  return self.parsed
end

return module
