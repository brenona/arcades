--- SVG rendering logic.
--
-- Parse and build a @{utils.placeholder|manipulable} SVG object.
-- Then allow to render them to rasterized image.
-- This rely on different C library and thus on the
-- `ffi_cdef` module.
-- @warn A reimplementation based on class could be great.
-- @module utils.svg_renderer
-- @alias M
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local ffi = require('ffi') -- Lua <-> C bindings
local clibs = require('arcades.utils.ffi_cdef') -- Assert FFI definitions have been done
-- Load required C libraries
local rsvg = clibs.rsvg -- SVG loading
local cairo = clibs.cairo -- SVG rendering
local gobject = clibs.gobject -- Gnome object management

local torch = require('torch')

local xml = require('pl.xml')

local placeholder = require('arcades.utils.placeholder')

local M = {}

--- Data Types
-- @section data-types

--- A manipulable SVG object.
-- @tfield string base_document Path to the SVG source file
-- @tfield integer width Dimen of the SVG file
-- @tfield integer height Dimen of the SVG file
-- @tfield table components List of SVG components (`string` or `placeholder`)
-- @tfield table placeholders @{utils.placeholder|Placeholders} indexed by their sensor ID
-- @table svg_object
-- @see utils.placeholder

--- Public Methods
-- @section public-methods

--- Instanciate an SVG object.
--
-- Build an SVG object from an SVG file.
-- @tparam string svg_path Path the SVG file to parse
-- @treturn svg_object The resulting `svg_object`
function M.prepare_svg_data(svg_path)
  assert(svg_path, "No path to the SVG given!")
  local document = xml.parse(svg_path, true) -- XML object of the SVG image
  assert(document, "Unable to parse SVG file '" .. svg_path .. '"')

  local svg = {
    base_document = svg_path,
    width = tonumber(document:get_attribs().width),
    height = tonumber(document:get_attribs().height),
    components = {},
    placeholders = {},
  }
  local function add_attribs_from(e)
    local result = ""
    for k, v in pairs(e:get_attribs()) do
      result = string.format(
        '%s %s="%s"',
        result, k, v
      )
    end
    return result
  end

  --- Recurcive component creator.
  --
  -- Parse SVG document replacing variable components with placeholders or
  -- concatenating constant components as single string.
  -- @tparam string component Current constant component
  -- @param element XML element to parse
  -- @treturn string Current constant component
  local function component_creator(component, element)
    if type(element) == 'string' then
      component = component .. element
    else
      -- Try to build a placeholder with the current element
      local ph = placeholder.PlaceHolderFactory.get_PlaceHolder(element)
      -- If no placeholder has been built, we must go deeper.
      if not ph then
        -- Save the current element with its attributes
        component = component .. "<" .. element.tag
        component = component .. add_attribs_from(element)
        component = component .. ">"
        -- Go one step deeper
        for child in element:children() do
          component = component_creator(component, child)
        end
        -- Close the current component after all its children
        component = component .. "</" .. element.tag .. ">"
      else -- A placeholder has been instanciated
        -- Save the static component if any
        if component ~= "" then
          table.insert(svg.components, component)
          component = ""
        end
        -- Save the placeholder
        table.insert(svg.components, ph)
        svg.placeholders[ph.sensor_id] = ph
      end
    end
    return component
  end

  local last_component = component_creator("", document)
  table.insert(svg.components, last_component)

  return svg
end

--- Convert an @{svg_object|SVG object} to a @{torch.Tensor/tensor.md/|Torch tensor}.
-- @tparam svg_object svg The `svg_object` to convert
-- @treturn torch.FloatTensor/tensor.md/ A @{torch.FloatTensor/tensor.md/} of size `depth x width x height`
function M.convert_SVG2Tensor(svg)
  -- Generate the full SVG data with right values
  local svg_data = {}
  for _, component in ipairs(svg.components) do
    table.insert(svg_data, tostring(component))
  end
  svg_data = table.concat(svg_data, "")

  -- Load SVG data in a C struct
  local rsvg_handle = rsvg.rsvg_handle_new_from_data(svg_data, #svg_data, nil)
  assert(rsvg_handle, "Unable to parse generated svg.")
  -- Initialize rendering context
  local surface = cairo.cairo_image_surface_create(
    "CAIRO_FORMAT_ARGB32",
    svg.width,
    svg.height
  )
  local cairo_t = cairo.cairo_create(surface)
  -- Render SVG
  rsvg.rsvg_handle_render_cairo(rsvg_handle, cairo_t)
  cairo.cairo_surface_flush(surface)
  -- Get pixels data
  local pixels = cairo.cairo_image_surface_get_data(surface)

  local dims = torch.LongStorage({
    svg.height,
    svg.width,
    4
  })
  local pixel_tensor = torch.ByteTensor(dims)

  -- Copy data to tensor
  ffi.copy(
    pixel_tensor:data(), -- destination
    pixels, -- source
    -- number of bytes to writes
    -- each pixel is 32bits (32/8 = 4 bytes)
    -- divided by arch dependent size of unsigned char (returned type)
    -- times the size of the image
    ((32/8)/ffi.sizeof('unsigned char')) * svg.height * svg.width
  )
  -- Put the depth dimension as the first dimension,
  -- trim the alpha channel,
  -- and convert data to floats between 0.0 and 1.0
  pixel_tensor = pixel_tensor:permute(3,1,2):sub(1,3):type(torch.getdefaulttensortype()):div(255)

  -- Clean up memory
  cairo.cairo_destroy(cairo_t)
  cairo.cairo_surface_destroy(surface)
  gobject.g_object_unref(rsvg_handle)

  return pixel_tensor
end

return M
