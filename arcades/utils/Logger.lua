--- A logger class.
--
-- A class that can be used by any object to instanciate dedicated logger.
-- See documentation of Python logging facility to see what I tried to
-- achieve.
--
-- This class also contains a @{main_logger|default/main logger} as a
-- static field for quick and dirty logging.
-- @classmod utils.Logger
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local module = {}
local class = torch.class('Logger', module)

--- Fields
-- @section field

--- Name of the logger to identify it.
-- @tfield string self.name

--- Function used to format log message.
-- @tfield function self.formatter

--- Output stream used by the logger.
-- @tfield io.file_descriptor self.output

--- Current logging level.
-- @tfield integer self.level_value

--- @section end

--- Class constructor.
-- @tparam string name @{name|Name} of the logger
-- @param[opt=default_level_value] level Logging level (string or integer) to use
-- @tparam[opt=timestamp_output] function formatter Function used to format log message
-- @tparam[opt=default_output] io.file_descriptor output Stream to use as logger output
function class:__init(name, level, formatter, output)
  self.name = name
  self.formatter = formatter or self.timestamp_output
  self.output = output or class.default_output
  self:set_level(level or class.default_level_value)
end

--- Public Methods
-- @section public-methods

--- Change logging level.
--
-- Change the current logging level to the given `level` witch can be passed as
-- @{level_values|integer} or @{level_strings|string} values.
-- @param level Logging level to use
-- @return `self`
function class:set_level(level)
  local int_level, str_level = self.get_levels(level)
  self.level_value = int_level
  self:debug("Changing level to : %s", str_level)
  return self
end

--- Get logging level.
--
-- Return current logging level as
-- @{level_values|integer} and @{level_strings|string} values.
-- @treturn integer Current logging integer value
-- @treturn string Current logging string value
function class:get_level()
  return self.get_levels(self.level_value)
end

--- Change the output.
-- @tparam[opt=nil] string filename Log to the given file or `io.stdout` if `nil`
function class:set_output(filename)
  self:info("Updating log output: " .. filename or "stdout")
  if filename then
    self.output = io.open(filename, "a")
  else
    self.output = io.output
  end
end

--- Log a message.
--
-- Generate a log message with the given `level`, and print it if current logging level allows it.
--
-- A function is also defined for each log level, using a lower cased version of @{level_strings} values.
-- @usage logger:log("DEBUG", ...)
-- -- equivalent to
-- logger:debug(...)
-- @param level Message level as @{level_values|integer} or @{level_strings|string} value.
-- @param ... Data to format and log. See @_get_log_message.
-- @return self
function class:log(level, ...)
  local int_level, _ = self.get_levels(level)
  if int_level >= self.level_value then
    local log_message = self:_get_log_message(int_level, ...)
    self.output:write(tostring(log_message))
    self.output:flush()
  end
  return self
end

--- Print a progressing wheel.
-- @warn This function is only available for the @{main_logger}.
-- @tparam[opt] integer step Step number to display
-- @function progress 

--- Private Methods
-- @section private-methods

--- Build and format a log message.
-- @tparam integer int_level Integer value of the level of the message
-- @tparam string str_format Base string to format
-- @param ... Values used for formatting
-- @treturn string Formatted log message
function class:_get_log_message(int_level, str_format, ...)
  local msg = str_format
  local str_level = self._get_level_string(int_level)
  if select('#', ...) > 0 then
    local status
    status, msg = pcall(string.format, str_format, ...)
    if not status then
      msg = "Error formatting log message: " .. msg
    end
  end
  return self:_format(str_level, msg)
end

--- Add logger informations to log message.
-- @tparam string str_level String value of the message level
-- @tparam string msg User log message
-- @treturn string Logger formatted message
function class:_format(str_level, msg)
  return self:formatter(str_level, msg)
end

--- Static Fields
-- @section static-fields

--- Available log levels.
--
-- String representations of the log levels.
-- @table level_strings
-- @tfield string DEBUG
-- @tfield string INFO
-- @tfield string WARN
-- @tfield string ERROR
-- @tfield string FATAL

--- Available log levels.
--
-- Integer representations of the log levels.
-- @table level_values
-- @tfield integer 10
-- @tfield integer 20
-- @tfield integer 30
-- @tfield integer 40
-- @tfield integer 50

class.level_strings = {"DEBUG", "INFO", "WARN", "ERROR", "FATAL"}
class.level_values  = { 10, 20, 30, 40, 50 }
for i, _ in ipairs(class.level_strings) do
  class[class.level_strings[i]] = class.level_values[i]
  class[string.lower(class.level_strings[i])] = function(self, ...)
    self:log(class.level_values[i], ...)
  end
end

--- Default output.
--
-- Output used by default by new logger instances.
-- @tfield io.file_descriptor default_output `io.stdout`
class.default_output = io.stdout
--- Default log level.
--
-- Log level used by default by new logger instances.
-- @tfield integer default_level_value 20
class.default_level_value = 20

--- Default logger.
--
-- A pre-instanciated logger for quick and dirty logging.
-- @tfield Logger main_logger

--- Static Functions
-- @section static-functions

--- Standard log formatter function.
--
-- The default standard log formatter function, adding timestamp, message level, file and line of issuing code.
-- @tparam Logger self The logger object
-- @tparam string level @{level_strings|String} representation of message level
-- @tparam string msg User log message
-- @treturn string Logger formatted message
function class.timestamp_output(self, level, msg)
  local result = ""
  if class.need_lf then result = result .. "\n" end
  result = result .. string.format(
    "%s ## % 5s ## %s:%s ## %s\n",
    os.date("%Y%m%dT%H%M%S"),
    level,
    self.name or debug.getinfo(2, "S").short_src,
    debug.getinfo(4, "l").currentline,
    msg
  )
  class.need_lf = false
  return result
end

--- Set default log level.
-- @param level Level value as @{level_values|integer} or @{level_strings|string} value.
-- @see default_level_value
function class.set_default_level(level)
  local int_level, str_level = class.get_levels(level)
  class.main_logger:debug("Updating default level to: " .. str_level)
  class.default_level_value = int_level
  class.main_logger:set_level(int_level)
end

--- Set default output.
-- @tparam[opt=nil] string f Name of the file to use as output or `nil` to set `io.stdout`
function class.set_default_output(f)
  class.main_logger:info("Updating log output: " .. f or "stdout")
  if f then
    class.default_output = io.open(f, "a")
  else
    class.default_output = io.output
  end
end


--- Get string representation of an integer log level.
-- @tparam integer int_level Log level as an @{level_values|integer}
-- @treturn string The @{level_strings|string} representation of the log level
function class._get_level_string(int_level)
  for i, v in ipairs(class.level_values) do
    if int_level <= v then
      return class.level_strings[i]
    end
  end
  return class.level_strings[1] -- Emergency case
end

--- Get integer representation of a string log level.
-- @tparam string str_level The @{level_strings|string} representation of the log level
-- @treturn integer Log level as an @{level_values|integer}
function class._get_level_value(str_level)
  for i, v in ipairs(class.level_strings) do
    if str_level == v then
      return class.level_values[i]
    end
  end
  return class.level_values[1] -- Emergency case
end

--- Get representations of the given level.
-- @param level Log level as @{level_values|integer} or @{level_strings|string} value.
-- @treturn integer Log level as @{level_values|integer}
-- @treturn string Log level as @{level_strings|string}
function class.get_levels(level)
  if type(level) == "number" then
    return level, class._get_level_string(level)
  else
    return class._get_level_value(level), level
  end
end

-- Create a special master logger
local main_logger = module.Logger()
main_logger.wheel = {"|", "/", "-", "\\"}
main_logger.wheel_step = 0
main_logger.last_step = 0

function main_logger:progress(step)
  if class.need_lf then self.output:write("\b\b\b\b\b\b\b\b\b\b\b") end
  self.last_step = step or self.last_step
  self.output:write(string.format(
    "%9s %s",
    self.last_step,
    self.wheel[self.wheel_step+1]
  ))
  self.output:flush()
  self.wheel_step = (self.wheel_step+1) % #self.wheel
  class.need_lf = true
end

class.main_logger = main_logger

return module.Logger
