--- A set of packages to implement the strategy design pattern.
--
-- This package contains sub-packages for each part/component of an
-- environment, each matching the strategy design pattern (cofounded with state DP, hence the name).
-- More info of this DP can be found on
-- [Wikipedia](https://en.wikipedia.org/wiki/Strategy_pattern) or
-- [TutorialsPoint](https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm).
--
-- List of sub-packages:
--
--   * `datarenderer`
--   * `datasource`
--   * `rewardfunction`
--
-- @package environment.states
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

return require('arcades.utils.package_loader')(...)
