--- Base class from which inherit to implement a corpus based data
-- providing strategy.
-- @classmod environment.states.datasource.labelled.DataLabelled
-- @alias class
-- @inherit true
-- @see environment.states.datasource.DataSource
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')
local paths = require('paths')

local splitv = require('pl.utils').splitv

local utils = require('arcades.utils.utils')

local module = {}
local environment = require('arcades.environment')
assert(environment.states.datasource.DataSource)
local class, super = torch.class('DataLabelled', "DataSource", module)

--- Data Types
-- @section data-types

--- `__init` argument.
-- @tfield environment.BaseEnvironment environment_model @{environment.BaseEnvironment|Environment} to which is linked this object
-- @tfield[opt=0.001] number input_time_scale Time scale of the corpus timestamps
-- @tfield[opt=1] number output_time_scale Time scale of the agent
-- @tfield[opt] number time_coef Coefficient used to translate data corpus to agent time scale
-- @tfield string data_path Path to the data corpus
-- @mtodo Don't mix with `dump`.
-- @table InitArgument

--- @section end

--- Default constructor.
-- @tparam InitArgument args
function class:__init(args)
  super.__init(self, args)
  self.environment_model = args.environment_model
  self.time_coef = args.time_coef or (args.input_time_scale or 0.001) / (args.output_time_scale or 1)
  self.data = self._parse_data(args.data_path, self.time_coef)
  self._no_dump_list.data = true
  self.data_path = args.data_path
  self.min_timestamp = self.data[1].timestamp
  self.max_timestamp = self.data[#self.data].timestamp
  self.current_timestamp = self.min_timestamp - 1
  self.last_data_index = 0
end

--- Public Methods
-- @section public-methods

--- Reset
-- @treturn `self`
function class:reset()
  return self
end

--- Private Methods
-- @section private-methods

--- Build state from corpus
-- @tfield integer data_index Index of sample to load
function class:_update_state_with_values(data_index)
  self.state = self.state or {}
  for k, v in pairs(self.data[data_index].values) do
    if k == "none-class.csv" then
      self.expected_action = {splitv(v, "\t")}
    else
      local sensor_id = string.match(k, "^[^-]+")
      self.state[sensor_id] = v
    end
  end
end

--- Fill state with values until timestamp.
-- @tparam integer timestamp Last value to use to fill the state
-- @tparam boolean from_current Fill from current state or start with a new one
function class:_fill_state(timestamp, from_current)
  self.state = from_current and self.state or {}
  self.last_data_index = from_current and self.last_data_index or 0
  while (
    self.data[self.last_data_index+1] and
    timestamp >= self.data[self.last_data_index+1].timestamp
  ) do
    self:_update_state_with_values(self.last_data_index + 1)
    self.last_data_index = self.last_data_index + 1
  end
  self.current_timestamp = timestamp
end

--- Compare timestamped data based on timestamp
-- @param i1 timestamped data 1
-- @param i2 timestamped data 2
-- @treturn boolean `i1 < i2`
function class._compare_timestamped_data(i1, i2)
  return i1.timestamp < i2.timestamp
end

--- Merge to data corpora checking timestamps consistency.
-- @tparam table d1 First corpus
-- @tparam table d2 Second corpus
-- @treturn table Merged corpus
function class._merge_data(d1, d2)
  local i, j = 1, 1
  local result = {}
  if #d1 == 0 then return d2 end
  if #d2 == 0 then return d1 end

  while i <= #d1 and j <= #d2 do
    if class._compare_timestamped_data(d1[i], d2[j]) then
      table.insert(result, d1[i])
      i = i+1
    elseif class._compare_timestamped_data(d2[j], d1[i]) then
      table.insert(result, d2[j])
      j = j+1
    else
      local values = d1[i].values
      for k, v in pairs(d2[j].values) do
        values[k] = v
      end
      table.insert(result, {
        timestamp = d1[i].timestamp,
        values = values
      })
      i = i+1
      j = j+1
    end
  end

  while i <= #d1 do
    table.insert(result, d1[i])
    i = i+1
  end
  while j <= #d2 do
    table.insert(result, d2[j])
    j = j+1
  end

  return result
end

--- Load a corpus from a csv file
-- @tparam string file_path Path to the CSV file
-- @tparam number time_coef Coefficent to apply to timestamp
-- @treturn table Loaded data
function class._load_csv_file(file_path, time_coef)
  assert(paths.filep(file_path))
  local data = {}
  local item_name = paths.basename(file_path)
  -- Parse CSV file
  for line in io.lines(file_path) do
    local timestamp, value = splitv(line, ";")
    timestamp = math.floor((tonumber(timestamp) * time_coef) + 0.5)
    value = utils.cast(value)
    assert(timestamp and value, string.format(
      "Malformed line '%s' in '%s'",
      line, file_path
    ))

    local values = {}
    values[item_name] = value
    table.insert(data, {
      timestamp = timestamp,
      values = values
    })
  end

  -- Sort data by timestamp
  table.sort(data, class._compare_timestamped_data)
  return data
end

--- Load data from different CSV file in a directory
-- @tparam string dir_path Path to the directory
-- @tparam number time_coef Coefficent to apply to timestamp
-- @treturn table Loaded data
function class._load_data_dir(dir_path, time_coef)
  assert(paths.dirp(dir_path))
  local dir_data = {}

  for item in paths.iterfiles(dir_path) do
    local file_data = class._load_csv_file(paths.concat(dir_path, item), time_coef)
    dir_data = class._merge_data(dir_data, file_data)
  end

  return dir_data
end

--- Parse plain text files to build a data table.
-- @tparam string data_path Path to the data
-- @tparam number time_coef Coefficent to apply to timestamp
-- @treturn table Loaded data
function class._parse_data(data_path, time_coef)
  assert(data_path, "Path to data not given.")
  assert(paths.dirp(data_path), string.format(
    "'%s' is not a folder.",
    data_path)
  )

  local recurse
  recurse = function(dir_path)
    local data = {}
    if paths.filep(paths.concat(dir_path, "none-class.csv")) then
      data = class._load_data_dir(dir_path, time_coef)
    else
      for item in paths.iterdirs(dir_path) do
        data = class._merge_data(
          data,
          recurse(paths.concat(dir_path, item))
        )
      end
    end
    return data
  end

  local data = recurse(data_path)
  return data
end

return module.DataLabelled
