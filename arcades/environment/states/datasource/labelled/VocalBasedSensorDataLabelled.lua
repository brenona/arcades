local torch = require('torch')

local module = {}
local environment = require('arcades.environment')
assert(environment.states.datasource.labelled.DataLabelled)
local class, super = torch.class('VocalBasedSensorDataLabelled', 'DataLabelled', module)

function class:__init(args)
  super.__init(self, args)

  self.history_length = args.history_length or 1
  self.terminal = false
end

function class:reset()
  super.reset(self)
  self.current_history = -1
  self.terminal = false

  return self
end

function class:update(args)
  if not self.terminal then
    self.current_timestamp = self.current_timestamp + 1
    -- If we're still in the history part (before vocal command)
    -- step one time increment, else go to next vocal command
    if self.current_history > 0 then
      self.current_history = self.current_history - 1
      -- Update state values if new values are available
      if (
        self.data[self.last_data_index+1] and
        self.current_timestamp >= self.data[self.last_data_index+1].timestamp
      ) then
        self:_update_state_with_values(self.last_data_index + 1)
        self.last_data_index = self.last_data_index + 1
      end
    elseif self.current_history == 0 then
      self.terminal = true
    else
      self:_goto_next_vocal_command()
    end
  end
  return self
end

function class:_align_timestamp(timestamp)
  local last_command_index
  for i, data in ipairs(self.data) do
    if data.timestamp > timestamp and last_command_index ~= nil then
      break
    end
    if (
      data.values['user1command-value.csv'] and
      self:_is_a_valid_command(data.values['user1command-value.csv'])
    ) then
      last_command_index = i
    end
  end

  return self.data[last_command_index].timestamp
end

function class:_goto_next_vocal_command()
  local next_command_index, next_command_timestamp = self.last_data_index, nil
  -- Walk through available data until to find a new vocal command
  while self.data[next_command_index+1] do
    next_command_index = next_command_index+1
    if (
      self.data[next_command_index].values["user1command-value.csv"] and
      self:_is_a_valid_command(self.data[next_command_index].values["user1command-value.csv"])
    ) then
      next_command_timestamp = self.data[next_command_index].timestamp
      break;
    end
  end

  -- If a command has been found...
  -- ... jump to it (minus history length)
  -- else, go back to the beginning
  if next_command_timestamp then
    local span = (next_command_timestamp - (self.history_length-1)) - self.current_timestamp
    self.current_history = (self.history_length - 1) + math.min(span, 0)
    self:_fill_state(next_command_timestamp - self.current_history, true)
  else
    self.current_timestamp = self.min_timestamp - 1
    self.last_data_index = 0
    self.state = {}
    self:_goto_next_vocal_command()
  end
end

function class:_is_a_valid_command(command)
  local result
  result = (
    self.environment_model.USER_COMMANDS_SET[command] ~= nil and
    command ~= "none"
  )
  return result
end

return module.VocalBasedSensorDataLabelled
