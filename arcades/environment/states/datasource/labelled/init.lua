--- @{DataSource|Data providers} that generate data from labelled corpus.
--
-- List of classes:
--
--  * @{environment.states.datasource.labelled.DataLabelled|DataLabelled}
--  * @{environment.states.datasource.labelled.AnnotatedLabelledDataProvider|AnnotatedLabelledDataProvider}
--  * @{environment.states.datasource.labelled.RealTimeSensorDataLabelled|RealTimeSensorDataLabelled}
--  * @{environment.states.datasource.labelled.VocalBasedSensorDataLabelled|VocalBasedSensorDataLabelled}
--
-- @package environment.states.datasource.labelled
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

return require('arcades.utils.package_loader')(...)
