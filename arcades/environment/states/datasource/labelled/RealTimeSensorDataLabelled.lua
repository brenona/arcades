local torch = require('torch')

local module = {}
local environment = require('arcades.environment')
assert(environment.states.datasource.labelled.DataLabelled)
local class, super = torch.class('RealTimeSensorDataLabelled', 'DataLabelled', module)

function class:__init(args)
  super.__init(self, args)
end

function class:update(args)
  args = args or {}
  if args.timestamp then
    if args.timestamp < self.min_timestamp then
      args.timestamp = torch.random(self.min_timestamp, self.max_timestamp)
    end
    local after_current = (args.timestamp > self.current_timestamp)
    self:_fill_state(args.timestamp, after_current)
  else
    self.current_timestamp = self.current_timestamp + 1
    if (
      self.data[self.last_data_index+1] and
      self.current_timestamp >= self.data[self.last_data_index+1].timestamp
    ) then
      self:_update_state_with_values(self.last_data_index + 1)
      self.last_data_index = self.last_data_index + 1
    end
  end
  return self
end

return module.RealTimeSensorDataLabelled
