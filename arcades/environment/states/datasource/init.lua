--- A set of data providing strategies.
--
-- Contains some classes to provide data.
--
-- List of classes:
--
--  * @{environment.states.datasource.DataSource|DataSource}
--
-- List of sub-packages:
--
--  * @{environment.states.datasource.labelled|labelled}
--  * @{environment.states.datasource.simulated|simulated}
--
-- @mtodo Rename it to `dataprovider`
-- @package environment.states.datasource
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

return require('arcades.utils.package_loader')(...)
