--- Class that generate inferred data.
-- @classmod environment.states.datasource.simulated.SweetHomeDataInferredSimulated
-- @alias class
-- @inherit true
-- @see environment.states.datasource.datasimulated.DataSimulated
-- @see environment.states.datasource.DataSource
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')
local tablex = require('pl.tablex')

local utils = require('arcades.utils.utils')

local environment = require('arcades.environment')

local module = {}
assert(environment.states.datasource.simulated.DataSimulated)
local class, super = torch.class('SweetHomeDataInferredSimulated', 'DataSimulated', module)

--- Data Types
-- @section data-types

--- `__init` argument.
-- @tfield boolean deterministic Is the simulation deterministic?
-- @tfield number no_action_ratio Ratio of `no_action` over `action` states
-- @tfield integer history_length Length of the history to simulate
-- @tfield integer max_tries Maximum numbers of tries before simulating the next state
-- @tfield environment.BaseEnvironment environment_model @{environment.BaseEnvironment|Environment} to which is linked this object
-- @mtodo Don't mix with `dump`.
-- @table InitArgument

--- @section end

--- Default constructor.
-- @tparam InitArgument args
function class:__init(args)
  args = args or {}
  super.__init(self, args)

  self.deterministic = args.deterministic == nil or args.deterministic
  self.no_action_ratio = args.no_action_ratio or 0 -- ratio of no_action/action state
  self.history_length = args.history_length or 1
  self.max_tries = args.max_tries or 1
  self.remaining_tries = args.remaining_tries or self.max_tries
  self.environment_model = args.environment_model

  if self.no_action_ratio > 0 then
    self._USER_COMMANDS = tablex.copy(self.environment_model.USER_COMMANDS)
    for _ = 0, self.no_action_ratio*(#self.environment_model.USER_COMMANDS) do
      table.insert(self._USER_COMMANDS, "none")
    end
    self.environment_model.USER_COMMANDS_SET["none"] = true
  end
end

--- Public Methods
-- @section public-methods

--- Reset data provider.
-- @return `self`
function class:reset()
  super.reset(self)
  self.current_history = -1
  self.terminal = false
  self.remaining_tries = self.max_tries
  return self
end

--- Update provider according to the executed action.
-- @tparam integer action_index Index of the action executed
-- @treturn `self`
-- @override true
-- @see DataSource:update
function class:update(action_index)
  if self.current_history > 0 then -- Still in history frames
    if self.current_history == 1 then -- Last frame, update to actual value
      self.state.user1inferredcommand = self.state._user1inferredcommand
      self.expected_action = self:_build_expected_action(
        self.state.user1inferredcommand,
        self.state.location,
        self.state.user1inferredactivity
      )
    end
    self.current_history = self.current_history - 1
  elseif self.current_history == 0 then -- Actual state value provided
    local expected_action_index = (
      self.environment_model.actions_index[self.expected_action[1]][self.expected_action[2]]
    )
    if (
      self.remaining_tries <= 1 or
      action_index == expected_action_index
      ) then
      self.terminal = true
    end
    self.remaining_tries = self.remaining_tries - 1
  elseif self.current_history < 0 then -- Generate a new sample
    self.current_history = self.history_length - 1
    local command, location, activity = self:_get_random_sample()
    self:set_state(command, location, activity)
  end
  return self
end

function class:get_expected_action()
  return self.expected_action
end

--- Update state to the given one.
-- @tparam string command Inferred command
-- @tparam string location Inferred location
-- @tparam string activity Inferred activity
-- @treturn `self`
function class:set_state(command, location, activity)
  -- TODO: remove the location field, rely on _location instead
  self.state = {
    _command = command, -- For debug, not use in placeholders
    _location = location, -- For debug, not use in placeholders
    _activity = activity, -- For debug, not use in placeholders
    user1inferredcommand = command,
    location = location, -- Just for information, not use in placeholders
    user1inferredactivity = activity
  }
  for _, loc in ipairs(self.environment_model.USER_LOCATIONS) do
    self.state["user1inferredlocation_" .. loc] = false -- remove old locs
  end
  self.state["user1inferredlocation_"..location] = 1

  -- Use none command during the history frames
  if self.history_length > 1 then
    self.state._user1inferredcommand = self.state.user1inferredcommand
    command = "none"
    self.state.user1inferredcommand = command
  end

  self.expected_action = self:_build_expected_action(
    command, location, activity
  )
  return self
end

--- Private Methods
-- @section private-methods

--- Get a valid state sample.
-- @treturn string A valid inferred command
-- @treturn string A valid inferred location
-- @treturn string A valid inferred activity
function class:_get_random_sample()
  local command = utils.sample_in(self._USER_COMMANDS or self.environment_model.USER_COMMANDS)
  local location = utils.sample_in(self.environment_model.USER_LOCATIONS)
  local activity = utils.sample_in(self.environment_model.USER_ACTIVITIES)
  return command, location, activity
end

--- Build the expected action from a given inferred state
-- @tparam string command Inferred command
-- @tparam string location Inferred location
-- @tparam string activity Inferred activity
-- @treturn table Expected action
function class:_build_expected_action(command, location, activity)
  local expected_action = self.environment_model.BASE_MODEL[command]
  expected_action = expected_action[location] or expected_action['any']
  expected_action = expected_action[activity] or expected_action['any']
  expected_action = (self.deterministic and expected_action[1]) or utils.sample_in(expected_action)
  return expected_action
end

return module.SweetHomeDataInferredSimulated
