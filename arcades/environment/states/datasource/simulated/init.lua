--- @{environment.states.datasource.DataSource|Data providers} that generate simulated data.
--
-- List of classes:
--
--  * @{environment.states.datasource.simulated.DataSimulated|DataSimulated}
--  * @{environment.states.datasource.simulated.SweetHomeDataInferredSimulated|SweetHomeDataInferredSimulated}
--  * @{environment.states.datasource.simulated.SweetHomeDataSensorSimulated|SweetHomeDataSensorSimulated}
--
-- @package environment.states.datasource.simulated
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

return require('arcades.utils.package_loader')(...)
