--- Base class from which inherit to implement a simulated data
-- providing strategy.
-- @classmod environment.states.datasource.simulated.DataSimulated
-- @alias class
-- @inherit true
-- @see environment.states.datasource.DataSource
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local module = {}
local environment = require('arcades.environment')
assert(environment.states.datasource.DataSource)
local class, _ = torch.class('DataSimulated', "DataSource", module)

--- Public Methods
-- @section public-methods

--- Reset the data provider
-- @return `self`
function class:reset()
  return self
end

return module.DataSimulated
