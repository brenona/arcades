--- Class that generate raw data.
-- @classmod environment.states.datasource.simulated.SweetHomeDataSensorSimulated
-- @alias class
-- @inherit true
-- @see environment.states.datasource.datasimulated.SweetHomeDataInferredSimulated
-- @see environment.states.datasource.datasimulated.DataSimulated
-- @see environment.states.datasource.DataSource
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local module = {}
local environment = require('arcades.environment')
assert(environment.states.datasource.simulated.SweetHomeDataInferredSimulated)
local class, super = torch.class('SweetHomeDataSensorSimulated', 'SweetHomeDataInferredSimulated', module)

--- Data Types
-- @section data-types

--- `__init` argument.
-- @tfield table sensors Sensors to generate data for
-- @see SweetHomeDataInferredSimulated.InitArgument
-- @table InitArgument

--- @section end

--- Default constructor
-- @tparam InitArgument args
function class:__init(args)
  args = args or {}
  super.__init(self, args)

  self.sensors = args.sensors or {}
end

--- Public Methods
-- @section public-methods

--- Update provider according to the executed action.
-- @tparam integer action_index Index of the action executed
-- @treturn `self`
-- @override true
-- @see DataSource:update
function class:update(action_index)
  super.update(self, action_index)

  -- Get value from relevant sensors
  local constraint_sensors = self:_get_constraints()
  -- Fill sensors values
  for sensor_id, ph in pairs(self.sensors) do
    local value
    if constraint_sensors[sensor_id] then
      value = constraint_sensors[sensor_id]
    else
      value = ph:get_random_value()
    end
    self.state[sensor_id] = value
  end
  if self.sensors.user1command then
    self.state.user1command = self.state.user1inferredcommand
  end
  -- Remove inferences
  self.state.user1inferredcommand = nil
  self.state.user1inferredactivity = nil
  self.state["user1inferredlocation_" ..
  self.state.location] = nil
  return self
end

--- Private Methods
-- @section private-methods

--- Get constrained values of sensors.
--
-- Apply constrained values according to the @{SENSOR_MODEL|sensor model}.
-- @treturn table Constrained sensors values
function class:_get_constraints()
  local command = self.state.user1inferredcommand
  local location = self.state.location
  local activity = self.state.user1inferredactivity
  local constraint_sensors = {}
  for _, c in ipairs({'any', command}) do
    if self.SENSOR_MODEL[c] then
      for _, l in ipairs({'any', location}) do
        if self.SENSOR_MODEL[c][l] then
          for _, a in ipairs({'any', activity}) do
            if self.SENSOR_MODEL[c][l][a] then
              for sensor_id, value in pairs(self.SENSOR_MODEL[c][l][a]) do
                constraint_sensors[sensor_id] = value or nil
              end
            end
          end
        end
      end
    end
  end
  return constraint_sensors
end

--- Static Fields
-- @section static-fields

--- Model to constrain some sensor value according to inferred state.
-- @tfield table class.SENSOR_MODEL

--- @section end

class.SENSOR_MODEL = {
  ['any'] = {
    ['kitchen'] = {
      ['any'] = {
        fffecc84d66b = 1,
        fffe9992f78d = 0,
        fffea8eaa68b = 0,
        fffe725aa973 = 1,
        fffe6a74e37b = 1,
        fffedddcca35 = 0,
        fffe6d8b552a = 0,
        chan0rsb = 40,
        chan1rsb = 40,
      },
    },
    ['bedroom'] = {
      ['any'] = {
        fffecc84d66b = 0,
        fffea8eaa68b = 0,
        fffed97b3486 = 1,
        fffe725aa973 = 0,
        fffe9289ac6e = 1,
        fffedddcca35 = 0,
        chan3rsb = 40,
        chan4rsb = 40,
      },
    },
    ['study'] = {
      ['any'] = {
        fffecc84d66b = 0,
        fffe9992f78d = 1,
        fffea8eaa68b = 0,
        fffe725aa973 = 0,
        fffedddcca35 = 0,
        fffe6d8b552a = 1,
        chan5rsb = 40,
        chan6rsb = 40,
      },
    }
  },
  ['light - on'] = {
    ['kitchen'] = {
      ['any'] = {
        fffe2b37eab9 = 0,
        fffecbb299a8 = 0,
      },
      ['eat'] = {
        fffecbb299a8 = false,
        chan1rsb = 20,
      },
      ['dish'] = {
        fffe2b37eab9 = false,
      },
      ['cook'] = {
        fffe2b37eab9 = false,
      },
    },
    ['bedroom'] = {
      ['any'] = {
        fffea4acfdcc = 0,
        fffef9ef7d48 = 0,
        fffeb6a85dbb = 0,
        fffefa94abc7 = 0,
      },
      ['sleep'] = {
        fffea4acfdcc = false,
        fffef9ef7d48 = false,
      }
    },
    ['study'] = {
      ['any'] = {
        fffedc6caa6a = 0,
        fffe8cc9bad0 = 0,
        fffefb6453c9 = 0,
      },
    }
  },

  ['light - off'] = {
    ['kitchen'] = {
      ['any'] = {
        fffe2b37eab9 = 1,
        fffecbb299a8 = 1,
      },
      ['eat'] = {
        fffecbb299a8 = false,
        chan1rsb = 20,
      },
      ['dish'] = {
        fffe2b37eab9 = false,
      },
      ['cook'] = {
        fffe2b37eab9 = false,
      },
    },
    ['bedroom'] = {
      ['any'] = {
        fffea4acfdcc = 1,
        fffef9ef7d48 = 1,
        fffeb6a85dbb = 1,
        fffefa94abc7 = 1,
      },
      ['sleep'] = {
        fffea4acfdcc = false,
        fffef9ef7d48 = false,
      }
    },
    ['study'] = {
      ['any'] = {
        fffedc6caa6a = 1,
        fffe8cc9bad0 = 1,
        fffefb6453c9 = 1,
      },
    }
  },

  ['curtain - open'] = {
    ['any'] = {
      ['any'] = {
        fffe3b6e8d53 = -1,
      }
    }
  },
  ['curtain - close'] = {
    ['any'] = {
      ['any'] = {
        fffe3b6e8d53 = 1,
      }
    }
  },

  ['blind - open'] = {
    ['kitchen'] = {
      ['any'] = {
        fffe63c3a7b2 = -1,
      }
    },
    ['bedroom'] = {
      ['any'] = {
        fffec7396cb7 = -1,
        fffec659b23d = -1,
      }
    },
    ['study'] = {
      ['any'] = {
        fffeabb51aca = -1,
        fffed50b88c7 = -1,
      }
    },
  },
  ['blind - close'] = {
    ['kitchen'] = {
      ['any'] = {
        fffe63c3a7b2 = 1,
      }
    },
    ['bedroom'] = {
      ['any'] = {
        fffec7396cb7 = 1,
        fffec659b23d = 1,
      }
    },
    ['study'] = {
      ['any'] = {
        fffeabb51aca = 1,
        fffed50b88c7 = 1,
      }
    },
  },
}

return module.SweetHomeDataSensorSimulated
