--- Base abstract class from which inherit to implement any data
-- providing strategy.
-- @classmod environment.states.datasource.DataSource
-- @alias class
-- @inherit true
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local arcades = require('arcades')

local module = {}
local class, super = torch.class('DataSource', 'ArcadesComponent', module)

--- Fields
-- @section fields

--- Current state.
-- @tfield {[string]=number,...} self.state

--- Terminal signal for current `state`.
-- @tfield boolean self.terminal

--- @section end

--- Default constructor.
-- @tparam ArcadesComponent.InitArgument args
-- @mtodo Fix usage of `dump`
function class:__init(args)
  super.__init(self, args)
end

--- Public Methods
-- @section public-methods

--- Get the current state.
-- @treturn {[string]=number,...} Current state
function class:get_state()
  return self.state or {}
end

--- Get the terminal signal of the current state.
-- @treturn boolean `self.terminal`
function class:is_terminal()
  local t = self.terminal
  if t == nil then
    t = false
  end
  return t
end

--- Return expected action for the current state.
-- @treturn table Expected action
-- @mtodo Use DP for polymorph actions.
function class:get_expected_action()
  return self.expected_action or {'none', 'none'}
end

--- Abstract Methods
-- @section abstract-methods

--- Move to the next state.
function class:update()
  error(
    string.format(
      "%s:%s - Not implemented",
      torch.typename(self),
      debug.getinfo(1, 'n').name
    ),
  2)
end

--- Reset provider.
function class:reset()
  error(
    string.format(
      "%s:%s - Not implemented",
      torch.typename(self),
      debug.getinfo(1, 'n').name
    ),
  2)
end

return module.DataSource
