local torch = require('torch')

local environment = require('arcades.environment')
assert(environment.states.rewardfunction.RewardFunction)
local module = {}
local class, super = torch.class('LabelledReward', "RewardFunction", module)

function class:__init(args)
  super.__init(self, args)

  self.coef = args.coef or 1
  self.no_action_penalty = args.no_action_penalty or 0
end


function class:get_reward(expected_action, executed_action)
  local reward, need_update

  if expected_action[1] == "none" then
    need_update = true -- Nothing intended, time continue to flow
    if executed_action[1] == "none" then
      reward = -0.005 -- Small penalty to discourage it to do nothing
    else
      reward = -1
    end
  else
    need_update = false -- Wait for the right action before updating
    if executed_action[1] == "none" then
      -- Big penalty to force the agent to act
      reward = self.no_action_penalty
    else
      -- Suppose that it's the right action
      reward = 1
      need_update = true
      -- Check that it's actually the right action,
      -- and if not, penalize the agent.
      for i = 1,#executed_action do
        if executed_action[i] ~= expected_action[i] then
          reward = -1
          need_update = false
          break
        end
      end
    end
  end

  return reward*self.coef, need_update
end

return module.LabelledReward
