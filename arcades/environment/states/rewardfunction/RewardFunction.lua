local torch = require('torch')

local arcades = require('arcades')

local module = {}
local class, super = torch.class('RewardFunction', 'ArcadesComponent', module)

function class:__init(args)
  super.__init(self, args)
end

function class:get_reward()
  error(
    string.format(
      "%s:%s - Not implemented",
      torch.typename(self),
      debug.getinfo(1, 'n').name
    ),
  2)
end

return module.RewardFunction
