--- A set of data rendering strategies.
--
-- Contains some classes to render data. Only concrete class for the moment
-- is one to generate an image from data.
--
-- List of classes:
--
--  * @{environment.states.datarenderer.DataRenderer|DataRenderer}
--  * @{environment.states.datarenderer.GraphicalDataRenderer|GraphicalDataRenderer}
--
-- @package environment.states.datarenderer
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

return require('arcades.utils.package_loader')(...)
