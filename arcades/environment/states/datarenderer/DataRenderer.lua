--- Base abstract class from which inherit to implement any data
-- rendering strategy.
-- @classmod environment.states.datarenderer.DataRenderer
-- @alias class
-- @inherit true
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local arcades = require('arcades')

local _M = {}
local class, super = torch.class("DataRenderer", "ArcadesComponent", _M)

--- Default constructor.
-- @tparam ArcadesComponent.InitArgument args
-- @mtodo Fix usage of `dump`
function class:__init(args)
  super.__init(self, args)
end

--- Abstract Methods
-- @section abstract-methods

--- Render given data.
function class:render()
  if torch.typename(self) == "DataRenderer" then
    error(
      string.format(
        "%s : abstract class is not intented to be instanciated.",
        torch.typename(self)
      ),
    2)
  end
end


return _M.DataRenderer
