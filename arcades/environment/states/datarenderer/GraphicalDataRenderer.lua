--- A class to generate an image from raw data.
-- @classmod environment.states.datarenderer.GraphicalDataRenderer
-- @alias class
-- @inherit true
-- @see environment.states.datarenderer.DataRenderer
-- @see ArcadesComponent
-- @author Alexis BRENON <brenon.alexis+arcades@gmail.com>

local torch = require('torch')

local svg_renderer = require('arcades.utils.svg_renderer')

local environment = require('arcades.environment')
assert(environment.states.datarenderer.DataRenderer)
local module = {}
local class, super = torch.class('GraphicalDataRenderer', "DataRenderer", module)

--- Data Types
-- @section data-types

--- `__init` argument.
-- @tfield string map_path Path to the base SVG path
-- @table InitArgument

--- Fields
-- @section fields

--- The table containing SVG data
-- @tfield utils.svg_renderer.svg_object self.svg

--- @section end

--- Default constructor
-- @tparam InitArgument args
-- @mtodo Fix usage of `dump`
function class:__init(args)
  args = args or {}
  super.__init(self, args)

  if args.svg then
    self.svg = args.svg
  else
    assert(args.map_path, "Path to the map not provided.")
    self.svg = svg_renderer.prepare_svg_data(args.map_path)
  end

  self.last_render = nil
end

--- Public Methods
-- @section public-methods

--- Return @{utils.placeholder|placeholders} of the underlying SVG.
-- @treturn table @{utils.svg_renderer.svg_object|self.svg.placeholders}
function class:get_placeholders()
  return self.svg.placeholders
end

--- Apply given `state` to the image.
-- @tparam {[string]=integer,...} state Map of values for sensors
-- @treturn torch.Tensor/tensor.md/ Rendered image
-- @override true
-- @see DataRenderer:render
function class:render(state)
  if state == nil then
    for _, ph in pairs(self.svg.placeholders) do
      ph:set_value(nil)
    end
    return torch.ones(3, self.svg.height, self.svg.width)
  else
    local must_rerender = false
    -- Update placeholders according to the current state
    for sensor_id, value in pairs(state) do
      local ph = self.svg.placeholders[sensor_id]
      if ph then
        local _, value_is_modified = ph:set_value(value)
        must_rerender = must_rerender or value_is_modified
      end
    end
    if must_rerender then
      self.last_render = svg_renderer.convert_SVG2Tensor(self.svg)
    end
    return self.last_render
  end
end

return module.GraphicalDataRenderer
