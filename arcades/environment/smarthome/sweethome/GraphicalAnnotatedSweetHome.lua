--- Inferred graphic domus.
-- @classmod environment.smarthome.sweethome.GraphicalAnnotatedSweetHome
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local environment = require('arcades.environment')

assert(environment.smarthome.sweethome.AnnotatedSweetHome)
local module = {}
local class, super = torch.class('GraphicalAnnotatedSweetHome', 'AnnotatedSweetHome', module)

function class:__init(args)
  args = args or {}
  super.__init(self, args)
  
  args.environment_model = self

  args.no_action_ratio = args.no_action_ratio or 0
  args.no_action_penalty = args.no_action_penalty or (
    -2 * (
      (#self:actions()-1) * (args.history_length or 1)
    )
  )
  args.max_tries = args.max_tries or math.huge
  args.remaining_tries = args.remaining_tries or args.max_tries

  self.last_observation = nil
  self.last_reward = 0

  self.data_renderer = environment.states.datarenderer.GraphicalDataRenderer(args)
  local placeholders = self.data_renderer:get_placeholders()
  local sensors = {}
  for _, id in ipairs(self.SENSORS) do
    sensors[id] = placeholders[id]
  end
  args.sensors = sensors

  local data_sources = environment.states.datasource
  if not args.data_path then
    self._logger:info("Using synthetic data")
    self.data_source = data_sources.simulated.SweetHomeDataInferredSimulated(
      args
    )
  else
    self._logger:info("Using real data")
    self.data_source = data_sources.labelled.AnnotatedLabelledDataProvider(args)
  end

  self.reward_function = environment.states.rewardfunction.LabelledReward(
    args
  )
end

--- Overriden methods.
-- @section overriden

function class:get_observable_state()
  if self.last_observation == nil then
    self.last_observation = self.data_renderer:render(self.data_source:get_state())
  end
  return {
    observation = self.last_observation,
    terminal = (
      (self.last_reward == 1) or
      (self.data_source:is_terminal())
    )
  }
end

--- Overriden method
-- @see BaseEnvironment:perform_action
function class:perform_action(action_index)
  local action = self:actions()[action_index]
  local expected_action = self.data_source:get_expected_action()

  -- TODO: use actions indexes for consistency
  if action then
    self.last_reward = self.reward_function:get_reward(expected_action, action)
  else
    self.last_reward = 0
  end

  self.data_source:update(action_index)
  self.last_observation = nil

  return self
end

--- Overriden method
-- @see BaseEnvironment:get_reward
function class:get_reward()
  return self.last_reward
end

function class:reset()
  self.data_source:reset()
  self.data_source:update({timestamp = -1})
  self.last_observation = nil
  self.last_reward = 0
  return self
end

function class:get_true_action()
  if not self.actions_index then self:actions() end
  local expected_action = self.data_source:get_expected_action()
  local result
  if (
    self.actions_index[expected_action[1]] and
    self.actions_index[expected_action[1]][expected_action[2]]
  ) then
    result = self.actions_index[expected_action[1]][expected_action[2]]
  else
    result = self.actions_index["none"]["none"]
  end
  return result
end

return module.GraphicalAnnotatedSweetHome
