local torch = require('torch')

local environment = require('arcades.environment')
assert(environment.smarthome.sweethome.SweetHome)
local module = {}
local class, super = torch.class('AnnotatedSweetHome', "SweetHome", module)

function class:__init(args)
  args = args or {}
  args.restricted = nil
  super.__init(self, args)
end

class.AVAILABLE_COMMANDS_GLOBAL = {
  'light - on',
  'light - off',
  'radio - on',
  'radio - off',
  'blind - open',
  'blind - close',
  'curtain - open',
  'curtain - close',
  'tts - time',
  'tts - temperature',
  'phone - help',
  'phone - son',
}
class.SENSORS_GLOBAL = {
  "user1inferredcommand",
  "user1inferredactivity",
  "user1inferredlocation_study", "user1inferredlocation_kitchen", "user1inferredlocation_bedroom"
}

return module.AnnotatedSweetHome
