local torch = require('torch')

local environment = require('arcades.environment')
assert(environment.BaseEnvironment)
local module = {}
local class, super = torch.class('SweetHome', "BaseEnvironment", module)

function class:__init(args)
  super.__init(self, args)
  self.use_restricted_model = args.restricted or false
  for _, schema in ipairs({
    "USER_LOCATIONS",
    "USER_ACTIVITIES",
    "USER_COMMANDS",
    "AVAILABLE_COMMANDS",
    "COMMAND_LOCATION",
    "BASE_MODEL",
    "SENSORS",
  }) do
    self._no_dump_list[schema] = true
    self._no_dump_list[schema .. "_RESTRICTED"] = true
    self._no_dump_list[schema .. "_GLOBAL"] = true
    self._no_dump_list[schema .. "_SET"] = true
    if self.use_restricted_model then
      self[schema] = self[schema .. "_RESTRICTED"]
    else
      self[schema] = self[schema .. "_GLOBAL"]
    end
    self[schema .. "_SET"] = {}
    for _, v in ipairs(self[schema]) do
      self[schema .. "_SET"][v] = true
    end
  end

  if args.faulty_sensors then
    for _, id in ipairs(args.faulty_sensors) do
      self.SENSORS_SET[id] = nil
      for i, s_id in ipairs(self.SENSORS) do
        if id == s_id then
          table.remove(self.SENSORS, i)
          break
        end
      end
    end
  end
end

function class:actions()
  if not self.possible_actions then
    self._no_dump_list.possible_actions = true
    self.possible_actions = {}
    self.actions_index = {}
    for _, action in ipairs(self.AVAILABLE_COMMANDS) do
      local sep_start = string.find(action, ' ', 1, true)
      local base_action
      if sep_start then
        base_action = string.sub(action, 1, sep_start-1)
      else
        base_action = action
      end
      for _, location in ipairs(self.COMMAND_LOCATION[base_action]) do
        table.insert(self.possible_actions, {action, location})
        self.actions_index[action] = self.actions_index[action] or {}
        self.actions_index[action][location] = #self.possible_actions
      end
    end
  end
  return self.possible_actions
end

class.USER_LOCATIONS_GLOBAL = {
  'kitchen',
  'bedroom',
  'study'
}
class.USER_ACTIVITIES_GLOBAL = {'eat', 'clean', 'sleep', 'read', 'phone', 'dish', 'cook', 'tidy', 'none'}
class.USER_COMMANDS_GLOBAL = {
  'light - on',
  'light - off',
  'radio - on',
  'radio - off',
  'blind - open',
  'blind - close',
  'curtain - open',
  'curtain - close',
  'tts - time',
  'tts - temperature',
  'phone - help',
  'phone - son'
}

class.AVAILABLE_COMMANDS_GLOBAL = {
  'light - on',
  'light - off',
  'radio - on',
  'radio - off',
  'blind - open',
  'blind - close',
  'curtain - open',
  'curtain - close',
  'tts - time',
  'tts - temperature',
  'phone - help',
  'phone - son',
  'none'
}
class.COMMAND_LOCATION_GLOBAL = {
  light = {
    "kitchen - all",
    "kitchen - sink",
    "kitchen - ceiling",
    "bedroom - all",
    "bedroom - bed",
    "bedroom - ceiling",
    "study - ceiling"
  },
  radio = {'bedroom'},
  blind = {
    "kitchen",
    "bedroom",
    "study"
  },
  curtain = {'bedroom'},
  tts = {
    "kitchen",
    "bedroom",
    "study"
  },
  phone = {'study'},
  none = {'none'},
}

class.BASE_MODEL_GLOBAL = {
  ['none'] = {
    ['any'] = {
      ['any'] = {
        {'none', 'none'}
      }
    }
  },

  ['light - on'] = {
    ['kitchen'] = {
      ['eat'] = {{'light - on', 'kitchen - ceiling'}},
      ['dish'] = {{'light - on', 'kitchen - sink'}},
      ['cook'] = {{'light - on', 'kitchen - sink'}},
      ['any'] = {
        {'light - on', 'kitchen - all'},
        {'light - on', 'kitchen - sink'},
        {'light - on', 'kitchen - ceiling'},
      }
    },
    ['bedroom'] = {
      ['sleep'] = {{'light - on', 'bedroom - bed'}},
      ['any'] = {
        {'light - on', 'bedroom - all'},
        {'light - on', 'bedroom - bed'},
        {'light - on', 'bedroom - ceiling'},
      }
    },
    ['study'] = {
      ['any'] = {{'light - on', 'study - ceiling'}},
    }
  },

  ['light - off'] = {
    ['kitchen'] = {
      ['eat'] = {{'light - off', 'kitchen - ceiling'}},
      ['dish'] = {{'light - off', 'kitchen - sink'}},
      ['cook'] = {{'light - off', 'kitchen - sink'}},
      ['any'] = {
        {'light - off', 'kitchen - all'},
        {'light - off', 'kitchen - sink'},
        {'light - off', 'kitchen - ceiling'},
      }
    },
    ['bedroom'] = {
      ['sleep'] = {{'light - off', 'bedroom - bed'}},
      ['any'] = {
        {'light - off', 'bedroom - all'},
        {'light - off', 'bedroom - bed'},
        {'light - off', 'bedroom - ceiling'},
      }
    },
    ['study'] = {
      ['any'] = {{'light - off', 'study - ceiling'}},
    }
  },

  ['curtain - open'] = {
    ['any'] = {
      ['any'] = {{'curtain - open', 'bedroom'}}
    }
  },
  ['curtain - close'] = {
    ['any'] = {
      ['any'] = {{'curtain - close', 'bedroom'}}
    }
  },

  ['blind - open'] = {
    ['kitchen'] = {
      ['any'] = {{'blind - open', 'kitchen'}}
    },
    ['bedroom'] = {
      ['any'] = {{'blind - open', 'bedroom'}}
    },
    ['study'] = {
      ['any'] = {{'blind - open', 'study'}}
    },
  },
  ['blind - close'] = {
    ['kitchen'] = {
      ['any'] = {{'blind - close', 'kitchen'}}
    },
    ['bedroom'] = {
      ['any'] = {{'blind - close', 'bedroom'}}
    },
    ['study'] = {
      ['any'] = {{'blind - close', 'study'}}
    },
  },

  ['radio - on'] = {
    ['any'] = {
      ['any'] = {{'radio - on', 'bedroom'}}
    }
  },
  ['radio - off'] = {
    ['any'] = {
      ['any'] = {{'radio - off', 'bedroom'}}
    }
  },

  ['tts - time'] = {
    ['kitchen'] = {
      ['any'] = {{'tts - time', 'kitchen'}},
    },
    ['bedroom'] = {
      ['any'] = {{'tts - time', 'bedroom'}},
    },
    ['study'] = {
      ['any'] = {{'tts - time', 'study'}},
    },
  },
  ['tts - temperature'] = {
    ['kitchen'] = {
      ['any'] = {{'tts - temperature', 'kitchen'}},
    },
    ['bedroom'] = {
      ['any'] = {{'tts - temperature', 'bedroom'}},
    },
    ['study'] = {
      ['any'] = {{'tts - temperature', 'study'}},
    },
  },

  ['phone - help'] = {
    ['any'] = {
      ['any'] = {{'phone - help', 'study'}},
    }
  },
  ['phone - son'] = {
    ['any'] = {
      ['any'] = {{'phone - son', 'study'}},
    }
  },
}

class.SENSORS_GLOBAL = {
  "fffe2b37eab9", "fffecbb299a8", "fffeb88797bc", "fffedc6caa6a", "fffe8cc9bad0",
  "fffea4acfdcc", "fffef9ef7d48", "fffeb6a85dbb", "fffefa94abc7", "fffefb6453c9",

  "fffe67daa0c3", "fffeaa3de849", "fffe8ea1e6dd", "fffed3e7eceb", "fffeda97bb69",
  "fffeb853ba76", "fffeb67c8973", "fffeab498e8a", "fffe9e7ea9a6", "fffeaa6a458e",
  "fffebb9b539b", "fffe98d7daa8",

  "fffe63c3a7b2", "fffe61dc66ba", "fffec7396cb7", "fffec659b23d", "fffeabb51aca",
  "fffed50b88c7", "fffe3b6e8d53",

  "fffe793bf9a3", "fffe8d5b3199", "fffecaaa7d8f", "fffe79b6b39c",
  "fffe1ec74cb8", "fffe9c5ebcd8", "fffecaba26b6", "fffeb595a7b1", "fffe8658caaa",
  "fffe7aab3aa5",
  "fffe17bab4ba", "fffeaa5ace9d", "fffeb8a8da6a", "fffe9b5b62c8", "fffe919b82b3",

  "fffecc84d66b", "fffe9992f78d", "fffea8eaa68b", "fffed97b3486", "fffe725aa973",
  "fffe6a74e37b", "fffe9289ac6e", "fffedddcca35", "fffe6d8b552a",

  "fffe48ca9395", "fffe659cdfb3",

  "fffe9da3a50a", "fffeb8a8ccbb", "fffe97678aad", "fffe3ce1bcaa",

  "chan0rsb", "chan1rsb", "chan2rsb", "chan3rsb", "chan4rsb", "chan5rsb", "chan6rsb", "chan7rsb",

  "user1command",
}

class.USER_LOCATIONS_RESTRICTED = {
  'kitchen',
  'bedroom',
--  'study'
}
class.USER_ACTIVITIES_RESTRICTED = {
  'eat',
--  'clean',
  'sleep',
--  'read',
--  'phone',
  'dish',
  'cook',
  'tidy',
  'none',
}
class.USER_COMMANDS_RESTRICTED = {
  'light - on',
  'light - off',
  'radio - on',
  'radio - off',
  'blind - open',
  'blind - close',
--  'curtain - open',
--  'curtain - close',
--  'tts - time',
--  'tts - temperature',
--  'phone - help',
--  'phone - son'
}

class.AVAILABLE_COMMANDS_RESTRICTED = {
  'light - on',
  'light - off',
  'radio - on',
  'radio - off',
  'blind - open',
  'blind - close',
--  'curtain - open',
--  'curtain - close',
--  'tts - time',
--  'tts - temperature',
--  'phone - help',
--  'phone - son',
  'none'
}
class.COMMAND_LOCATION_RESTRICTED = {
  light = {
--    "kitchen - all",
    "kitchen - sink",
    "kitchen - ceiling",
--    "bedroom - all",
    "bedroom - bed",
    "bedroom - ceiling",
--    "study - ceiling"
  },
  radio = {'bedroom'},
  blind = {
    "kitchen",
    "bedroom",
--    "study"
  },
  curtain = {'bedroom'},
  tts = {
    "kitchen",
    "bedroom",
    "study"
  },
  phone = {'study'},
  none = {'none'},
}

class.BASE_MODEL_RESTRICTED = {
  ['none'] = {
    ['any'] = {
      ['any'] = {
        {'none', 'none'}
      }
    }
  },

  ['light - on'] = {
    ['kitchen'] = {
      ['eat'] = {{'light - on', 'kitchen - ceiling'}},
      ['dish'] = {{'light - on', 'kitchen - sink'}},
      ['cook'] = {{'light - on', 'kitchen - sink'}},
      ['any'] = {
--        {'light - on', 'kitchen - all'},
        {'light - on', 'kitchen - sink'},
        {'light - on', 'kitchen - ceiling'},
      }
    },
    ['bedroom'] = {
      ['sleep'] = {{'light - on', 'bedroom - bed'}},
      ['any'] = {
--        {'light - on', 'bedroom - all'},
        {'light - on', 'bedroom - bed'},
        {'light - on', 'bedroom - ceiling'},
      }
    },
    ['study'] = {
      ['any'] = {{'light - on', 'study - ceiling'}},
    }
  },

  ['light - off'] = {
    ['kitchen'] = {
      ['eat'] = {{'light - off', 'kitchen - ceiling'}},
      ['dish'] = {{'light - off', 'kitchen - sink'}},
      ['cook'] = {{'light - off', 'kitchen - sink'}},
      ['any'] = {
--        {'light - off', 'kitchen - all'},
        {'light - off', 'kitchen - sink'},
        {'light - off', 'kitchen - ceiling'},
      }
    },
    ['bedroom'] = {
      ['sleep'] = {{'light - off', 'bedroom - bed'}},
      ['any'] = {
--        {'light - off', 'bedroom - all'},
        {'light - off', 'bedroom - bed'},
        {'light - off', 'bedroom - ceiling'},
      }
    },
    ['study'] = {
      ['any'] = {{'light - off', 'study - ceiling'}},
    }
  },

  ['curtain - open'] = {
    ['any'] = {
      ['any'] = {{'curtain - open', 'bedroom'}}
    }
  },
  ['curtain - close'] = {
    ['any'] = {
      ['any'] = {{'curtain - close', 'bedroom'}}
    }
  },

  ['blind - open'] = {
    ['kitchen'] = {
      ['any'] = {{'blind - open', 'kitchen'}}
    },
    ['bedroom'] = {
      ['any'] = {{'blind - open', 'bedroom'}}
    },
    ['study'] = {
      ['any'] = {{'blind - open', 'study'}}
    },
  },
  ['blind - close'] = {
    ['kitchen'] = {
      ['any'] = {{'blind - close', 'kitchen'}}
    },
    ['bedroom'] = {
      ['any'] = {{'blind - close', 'bedroom'}}
    },
    ['study'] = {
      ['any'] = {{'blind - close', 'study'}}
    },
  },

  ['radio - on'] = {
    ['any'] = {
      ['any'] = {{'radio - on', 'bedroom'}}
    }
  },
  ['radio - off'] = {
    ['any'] = {
      ['any'] = {{'radio - off', 'bedroom'}}
    }
  },

  ['tts - time'] = {
    ['kitchen'] = {
      ['any'] = {{'tts - time', 'kitchen'}},
    },
    ['bedroom'] = {
      ['any'] = {{'tts - time', 'bedroom'}},
    },
    ['study'] = {
      ['any'] = {{'tts - time', 'study'}},
    },
  },
  ['tts - temperature'] = {
    ['kitchen'] = {
      ['any'] = {{'tts - temperature', 'kitchen'}},
    },
    ['bedroom'] = {
      ['any'] = {{'tts - temperature', 'bedroom'}},
    },
    ['study'] = {
      ['any'] = {{'tts - temperature', 'study'}},
    },
  },

  ['phone - help'] = {
    ['any'] = {
      ['any'] = {{'phone - help', 'study'}},
    }
  },
  ['phone - son'] = {
    ['any'] = {
      ['any'] = {{'phone - son', 'study'}},
    }
  },
}
class.SENSORS_RESTRICTED = class.SENSORS_GLOBAL

return module.SweetHome
