--- Environments for smart-homes
--
-- List of the classes in this package:
-- <ul>
--  <li>TODO</li>
-- </ul>
--
-- After creation, environments need to be initialized/reset to set them in a
-- coherent state, calling the @{environment.BaseEnvironment:reset|reset}
-- function.
--
-- @module environment.SmartHome
-- @alias package
-- @author Alexis BRENON <alexis.brenon@imag.fr>

return require('arcades.utils.package_loader')(...)
