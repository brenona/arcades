--- A set of different environments on which an @{agent} can act.
--
-- List of the classes in this package:
--
--   * @{environment.BaseEnvironment|BaseEnvironment}
--   * @{environment.DebugEnvironment|DebugEnvironment}
--
-- List of sub-packages:
--
--   * @{environment.smarthome}
--   * @{environment.states|states}
--
-- After creation, environments need to be initialized/reset to set them in a
-- coherent state, calling the @{environment.BaseEnvironment:reset|reset}
-- function.
--
-- @package environment
-- @author Alexis BRENON <alexis.brenon@imag.fr>

--- Data Types
-- @section data-types

--- Observable state of an environment perceived by an agent.
-- @tfield torch.Tensor/tensor.md/ observation Observation data
-- @tfield boolean terminal Is the state terminal?
-- @table ObservableState

--- @section end

return require('arcades.utils.package_loader')(...)
