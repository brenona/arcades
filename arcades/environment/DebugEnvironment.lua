--- Debug environment that produces dummy states
-- @classmod environment.DebugEnvironment
-- @alias class
-- @inherit true
-- @see environment.BaseEnvironment
-- @see ArcadesComponent
-- @author Alexis BRENON <alexis.brenon@imag.fr>

-- luacheck: no self

local torch = require('torch')

local environment = require('arcades.environment')
assert(environment.BaseEnvironment)
local module = {}
local class, super = torch.class('DebugEnvironment', 'BaseEnvironment', module)

--- Fields
-- @section fields

--- Is current state terminal?
-- @tfield boolean self.terminal

--- @section end

--- Default constructor.
-- @tparam ArcadesComponent.InitArgument args
function class:__init(args)
  super.__init(self, args)
  self.terminal = nil
end

--- Public Methods
-- @section public-methods

--- Returns the current visible state of the environment
-- @treturn environment.ObservableState Observable state of the environment
-- @override true
-- @see BaseEnvironment:get_observable_state
function class:get_observable_state()
  return {observation = torch.Tensor(1,1,1):fill((self.terminal and 2) or 1), terminal=self.terminal}
end

--- Perform an action on the world that change its internal state.
--
-- For this debug environment, just switch the terminal signal.
-- @return `self`
-- @override true
-- @see BaseEnvironment:perform_action
function class:perform_action()
  self.terminal = not self.terminal
end

--- Get reward obtained from the last action.
-- @treturn number A reward (`1`)
-- @override true
-- @see BaseEnvironment:get_reward
function class:get_reward()
  return 1
end

--- Get the possible actions that environment accepts.
-- @treturn table A table of accepted actions (`{1}`)
-- @override true
-- @see BaseEnvironment:actions
function class:actions()
  return {1}
end

--- Return the action index of the expected action for the current state.
-- @treturn integer Index of the expected action (`1`)
-- @override true
-- @see BaseEnvironment:get_true_action
function class:get_true_action()
  return 1
end

--- (Re)Initialize the environment.
-- @return `self`
-- @override true
-- @see BaseEnvironment:reset
function class:reset()
  self.terminal = false
  return self
end

return module.DebugEnvironment
