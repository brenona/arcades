--- Abstract class from which inherit to implement an environment
-- @classmod environment.BaseEnvironment
-- @alias class
-- @inherit true
-- @see ArcadesComponent
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local arcades = require('arcades')
local module = {}
local class, super = torch.class('BaseEnvironment', 'ArcadesComponent', module)


--- Default constructor.
-- @tparam ArcadesComponent.InitArgument args
function class:__init(args)
  super.__init(self, args)
end

--- Abstract Methods
-- @section abstract-methods

--- Returns the current visible state of the environment
-- @warn Do two calls to this function return same values (stochastic or not?)
-- @treturn environment.ObservableState Observable state of the environment
function class:get_observable_state()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ), 2)
end

-- luacheck: push no unused args

--- Perform an action on the world that change its internal state.
-- @tparam integer action_index The index of the action to perform
-- @return `self`
function class:perform_action(action_index)
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end
-- luacheck: pop

--- Get reward obtained from the last action.
-- @treturn number A reward
function class:get_reward()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end

--- Get the possible actions that environment accepts.
-- @treturn table A table of accepted actions
function class:actions()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ), 2)
end

--- Return the action index of the expected action for the current state.
-- @treturn integer Index of the expected action
function class:get_true_action()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ), 2)
end

--- (Re)Initialize the environment.
-- @return `self`
function class:reset()
  return self
end


return module.BaseEnvironment
