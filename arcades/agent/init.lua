--- A set of different agents (implementing different policies).
-- An agent can provide an action to do given an environment state.
-- List of the classes in this package:
--
--  * @{agent.BaseAgent|BaseAgent}
--  * @{agent.RandomAgent|RandomAgent}
--  * @{agent.NeuralQLearner|NeuralQLearner}
-- 
-- There is also a utility class, named @{agent._ExperiencePool|_ExperiencePool}
-- used by @{agent.NeuralQLearner|NeuralQLearner}.
--
-- @package agent
-- @author Alexis BRENON <alexis.brenon@imag.fr>

return require('arcades.utils.package_loader')(...)
