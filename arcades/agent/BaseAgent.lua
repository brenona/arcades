--- Abstract class from which inherit to implement an agent.
-- @inherit true
-- @see ArcadesComponent
-- @classmod agent.BaseAgent
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local arcades = require('arcades')

local module = {}
local class, super = torch.class('BaseAgent', 'ArcadesComponent', module)

--- Abstract constructor.
-- @tparam table args
function class:__init(args)
  super.__init(self, args)
end

--- Abstract Methods
-- @section abstract-methods

-- luacheck: push no unused args

--- Integrate current observation from the environment.
-- @tparam environment.ObservableState state The current state of the environment
-- @return `self`
function class:integrate_observation(state)
  io.stderr:write(string.format(
    "WARNING: %s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ))
end
-- luacheck: pop

--- Return an action to do.
-- @treturn number Action to execute
function class:get_action()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end

-- luacheck: push no unused args

--- Reward or punish the agent.
-- @tparam number reward Reward if positive, punishment if negative
-- @return `self`
function class:give_reward(reward)
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end
-- luacheck: pop

--- Return how many interactions the agent lived.
-- @treturn number Number of interactions done
function class:get_experienced_interactions()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end

--- Return how many times the agent actually learned from its experience.
-- @treturn number Number of times the agent learned
function class:get_learned_epoch()
  error(string.format(
    "%s:%s - Not implemented",
    torch.typename(self),
    debug.getinfo(1, 'n').name
  ),
  2)
end

--- Public Methods
-- @section public-methods

--- Put the agent in a training mode.
--
-- This is the default mode of an agent.
-- @return `self`
function class:training()
  return self
end

--- Put the agent in an evaluation mode.
--
-- This can change some internal values of the agent, like the epsilon value
-- for epsilon-greedy strategy.
-- @return `self`
function class:evaluate()
  return self
end

-- luacheck: push no self

--- Return pretty-printed informations about the agent.
-- @treturn string A human readable string about the agent
function class:report()
  return ""
end

return module.BaseAgent
