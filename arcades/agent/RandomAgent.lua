--- A random agent.
-- This agent acts randomly chosing an action given an uniform probability in
-- all possible actions
-- @inherit true
-- @see agent.BaseAgent
-- @see ArcadesComponent
-- @classmod agent.RandomAgent
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local torch = require('torch')

local agent = require('arcades.agent')
assert(agent.BaseAgent)
local module = {}
local class, super = torch.class('RandomAgent', 'BaseAgent', module)

--- Default constructor.
-- @tparam table args
-- @tparam table args.actions Available actions
function class:__init(args)
  super.__init(self, args)
  self.num_actions = #args.actions
  self.actions_frequencies = torch.zeros(self.num_actions)
  self.current_is_terminal = true
end

--- Public Methods
-- @section public-methods

--- Integrate current observation from the environment.
-- @tparam table state The current state of the environment
-- @tparam torch.Tensor state.observation The actual observations
-- @tparam boolean state.terminal Is this state terminal?
-- @return `self`
-- @override true
-- @see BaseAgent:integrate_observation
function class:integrate_observation(state)
  self.current_is_terminal = state.terminal
  return self
end

--- Return an action to do.
-- @treturn number Action to execute
-- @override true
-- @see BaseAgent:get_action
function class:get_action()
  -- Select action
  local action = 0
  if not self.current_is_terminal then
    action = torch.random(1, self.num_actions)
    self.actions_frequencies[action] = self.actions_frequencies[action] + 1
  end
  return action
end

-- luacheck: push no unused args

--- Reward or punish the agent.
-- @tparam number reward Reward if positive, punishment if negative
-- @return `self`
-- @override true
-- @see BaseAgent:give_reward
function class:give_reward(reward)
  return self
end
-- luacheck: pop

--- Return how many interactions the agent lived.
-- @treturn number Number of interactions done
-- @override true
-- @see BaseAgent:get_experienced_interactions
function class:get_experienced_interactions()
  return self.actions_frequencies:sum()
end

-- luacheck: push no self

--- Return how many times the agent actually learned from its experience.
-- @treturn number `0`
-- @override true
-- @see BaseAgent:get_learned_epoch
function class:get_learned_epoch()
  return 0
end
-- luacheck: pop

--- Return pretty-printed informations about the agent.
-- For this agent, it returns the actions frequecies.
-- @treturn string A human readable string about the agent
-- @override true
-- @see BaseAgent:report
function class:report()
  local result = "Random Agent: actions frequencies"
  local actions_width = math.ceil(math.log(self.num_actions, 10))
  local total_actions = self:get_experienced_interactions()
  for action, value in ipairs(torch.totable(torch.div(self.actions_frequencies, total_actions))) do
    result = result .. string.format("\n %" .. actions_width .. "d: %.3f",
      action, value)
  end
  return result
end

return module.RandomAgent
