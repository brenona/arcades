--- DQN based agent.
-- This agent mix DNN and Q-Learning as stated in the Nature letter :<br/>
-- ["Human-level control through deep reinforcement learning" (Mnih et al.)](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)
-- @inherit true
-- @see agent.BaseAgent
-- @see ArcadesComponent
-- @classmod agent.NeuralQLearner
-- @alias class
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local nn = require('nn')
local torch = require('torch')

local arcades = require('arcades')

local agent = require('arcades.agent')
assert(agent.BaseAgent)
local module = {}
local class, super = torch.class('NeuralQLearner', 'BaseAgent', module)

--- Data Types
-- @section data-types

--- Table used as arguments for the DQN @{__init|constructor}.
-- @tfield {int,...} observation_size Size of the observations from the environment `{d, w, h}`
-- @tfield table actions Available actions from the environment (`actions[0]` is `noop`)
-- @tfield[opt] ClassArgument preprocess Parameters of the network to use to preprocess states
-- @tfield ClassArgument inference Parameters of the network used for inference
-- @tfield _ExperiencePool.InitArguments experience_pool Parameters of the memory of the agent
-- @tfield[opt=0] number learn_start Number of steps after which learning starts
-- @tfield[opt=1] number update_freq Learning frequency (epoch size)
-- @tfield[opt=1] number minibatch_size Number of samples to take to learn
-- @tfield[opt=1] number n_replay Number of minibatch learning during a learning epoch
-- @tfield[opt=false] boolean rescale_r Scale rewards
-- @tfield[opt] number max_reward Reward maximum value clipping
-- @tfield[opt] number min_reward Reward minimum value clipping
-- @tfield[opt=1] number ep_start Initial value of epsilon
-- @tfield[opt=ep_start] number ep_end Final value of epsilon
-- @tfield[opt=1000000] number ep_endt Epsilon annealing time
-- @tfield[opt=0.01] number ep_eval Epsilon value when evaluating
-- @tfield[opt=0.001] number lr Learning rate
-- @tfield[opt=0.99] number discount Q-learning Discount factor (0 < x < 1)
-- @tfield[opt=nil] number clip_delta Clipping value for delta
-- @tfield[opt=nil] number target_q How long a target network is valid
-- @tfield[opt=0] number wc L2 weight cost.
-- @tfield[opt={}] RMSPropArgument rmsprop Pre-initialized RMSProp arguments.
-- @warn Some mixes between @{Dump|dump} and @{InitArguments|arguments} should be cleared...
-- @table InitArguments

--- Dump extracted from a `NeuralQLearner`.
-- All @{torch.Tensor/tensor.md/|Tensors} are converted to the 
-- @{torch.default_type/utility.md#string-torchgetdefaulttensortype/|default type}
-- to avoid GPU incompatibilities.
-- @table Dump

--- Arguments used to instanciate a class.
-- @tfield string class Name of the class to instantiate
-- @tfield table params Parameters of the class (see class documentation)
-- @table ClassArgument

--- Parameters for RMSProp implementation
-- @tfield torch.Tensor/tensor.md/ mean_square Accumulated average of the squared gradient
-- @tfield torch.Tensor/tensor.md/ mean Accumulated average of the gradient
-- @tfield number decay Decay factor of the means
-- @tfield number mu Smoothing term
-- @mtodo Implement gradient descent in sub-classes?
-- @table RMSPropArgument

--- Preprocessing network.
-- This network is used to preprocess an observation from the environment
-- to change it to a simpler state
-- @tfield nn.Module/module.md/ network The actual network
-- @tfield table input_size The size of the input `{d, w, h}`
-- @tfield table output_size The size of the output `{d, w, h}`
-- @table PreprocessNetwork

--- Main deep neural network.
-- This network is used to get the best action given a history of preprocessed states
-- @tfield nn.Module/module.md/ network The actual network
-- @tfield table input_size The size of the input `{d, w, h}`
-- @tfield table output_size The size of the output `{w, h}`
-- @tfield torch.Tensor/tensor.md/ parameters Flat view of learnable parameters
-- @tfield torch.Tensor/tensor.md/ grad_parameters Flat view of gradient of energy wrt the learnable parameters
-- @table InferenceNetwork

--- @section end

--- Default constructor.
-- @tparam InitArguments args
-- @tparam[opt={}] Dump dump
function class:__init(args, dump)
  super.__init(self, args, dump)
  if not args then
    self._logger:warn("No argument provided!")
  end
  args = args or {}; dump = dump or {}

  --- Does the agent must use GPU or not?
  local use_gpu = package.loaded["cutorch"]
  --- Function to convert tensors if necessary.
  --
  -- This function must be called to convert tensors/network to the appropriate
  -- format (CUDA or default Tensor type) to avoid computation errors caused by
  -- inconsistent types
  -- @tfield function self._convert_tensor
  self._convert_tensor = nil
  if use_gpu then
    self._convert_tensor = function(t) return t:cuda() end
  else
    local default_type = torch.getdefaulttensortype()
    self._convert_tensor = function(t) return t:type(default_type) end
  end


  --- Preprocessing network.
  -- @tfield PreprocessNetwork self.preprocess
  self.preprocess = nil
  self:_init_preprocessing_network(args.preprocess, args.observation_size)

  args.experience_pool = args.experience_pool or {}
  args.experience_pool.state_size = self.preprocess.output_size
  --- @{agent._ExperiencePool|Experience pool} recording interactions.
  --
  -- This experience pool will act as a memory for the agent, recording
  -- interactions, and returning them when necessary (e.g. when learning).
  -- @tfield agent._ExperiencePool self.experience_pool
  self.experience_pool = agent._ExperiencePool(args.experience_pool, dump.experience_pool)


  --- Main neural network
  -- @tfield InferenceNetwork self.inference
  self:_init_inference_network(args.inference, dump.inference)

  --- Learning frequency (epoch size).
  -- @tfield number self.update_freq
  self.update_freq = args.update_freq or 12
  --- Number of samples to take to learn.
  -- @tfield number self.minibatch_size
  self.minibatch_size = args.minibatch_size or 32
  --- Number of minibatch learning during a learning epoch.
  -- @tfield number self.n_replay
  self.n_replay = args.n_replay or 1
  --- Number of steps after which learning starts.
  -- Add delay to populate the experience pool
  -- @tfield number self.learn_start
  self.learn_start = args.learn_start or math.max(self.minibatch_size + 1, (0.1 * self.experience_pool.pool.max_size))

  --- Scale rewards delta.
  -- @tfield boolean self.rescale_r
  self.rescale_r = args.rescale_r
  --- Reward maximum value clipping.
  -- @tfield number self.max_reward
  self.max_reward = args.max_reward
  --- Reward minimum value clipping.
  -- @tfield number self.min_reward
  self.min_reward = args.min_reward
  --- Maximal encountered reward for reward scaling.
  -- @see self.rescale_r
  -- @tfield number self.r_max
  self.r_max = 1

  -- epsilon annealing

  --- Initial value of epsilon.
  -- @tfield number self.ep_start
  self.ep_start = args.ep_start or 1
  --- Current espilon value.
  -- @tfield number self.ep
  self.ep = dump.ep or self.ep_start
  --- Final value of epsilon.
  -- @tfield number self.ep_end
  self.ep_end = args.ep_end or 0.25
  --- Epsilon annealing time.
  -- @tfield number self.ep_endt
  self.ep_endt = args.ep_endt or 200000
  --- Epsilon value when evaluating.
  -- @tfield number self.ep_eval
  self.ep_eval = args.ep_eval or 0

  --- Learning rate.
  -- @tfield number self.lr
  self.lr = args.lr or 0.00025

  --- Q-learning discount factor (0 < x < 1).
  -- @tfield number self.discount
  self.discount = args.discount or 0.99
  --- Clipping value for differences between expected Q-Value and actual Q-Value.
  -- @tfield number self.clip_delta
  self.clip_delta = args.clip_delta or nil
  --- How long a target network is valid.
  --
  -- If not nil a target network will be used during Q-Learning to improve
  -- the algorithm convergence.
  -- @tfield number self.target_q
  self.target_q = args.target_q or 4096
  --- L2 weight cost.
  -- @tfield number self.wc
  self.wc = args.wc or 0

  --- Number of perceived states.
  -- @tfield number self.experienced_steps
  self.experienced_steps = args.experienced_steps or 0
  --- Number of time the agent has learned.
  -- @tfield number self.learning_epoch
  self.learning_epoch = args.learning_epoch or 0

  --- Parameters for RMSProp implementation.
  -- @tfield RMSPropArgument self.rmsprop
  -- @mtodo Use dedicated classes for GD implementations.
  self.rmsprop = args.rmsprop or {
    mean_square = self._convert_tensor(torch.zeros(self.inference.parameters:size())),
    mean = self._convert_tensor(torch.zeros(self.inference.parameters:size())),
    decay = 0.95,
    mu = 0.01
  }
  if dump.rmsprop then
    self.rmsprop.mean_square = self._convert_tensor(self.rmsprop.mean_square)
    self.rmsprop.mean = self._convert_tensor(self.rmsprop.mean)
  end

  --- Current target network.
  -- @tfield nn.Container/containers.md/ self.target_network
  self.target_network = args.target_network and self._convert_tensor(args.target_network) or nil
  if not self.target_network and self.target_q then
    self.target_network = self.inference.network:clone()
  end

  if (dump.evaluating ~= nil) then
    -- Put the agent in training mode
    self:training()
  else
    self.evaluating = dump.evaluating
  end
end

--- Public Methods
-- @section public-methods

--- Integrate current observation from the environment.
-- @tparam table state The current state of the environment
-- @tparam torch.Tensor state.observation The actual observations
-- @tparam boolean state.terminal Is this state terminal?
-- @return `self`
-- @override true
-- @see BaseAgent:integrate_observation
function class:integrate_observation(state)
  self.experience_pool:record_state(
    self.preprocess.network:forward(
      state.observation),
    state.terminal
  )
  return self
end

--- Return an action to do.
-- @treturn number Action to execute
-- @override true
-- @see BaseAgent:get_action
function class:get_action()
  -- Fetch the last state with all its history
  local full_historic_state, terminal = self.experience_pool:get_state()
  local action_index = 0
  if not terminal then
    action_index = self:_eGreedy(full_historic_state)
    self.experience_pool:record_action(action_index)
  end
  return action_index
end

--- Reward or punish the agent.
-- @tparam number reward Reward if positive, punishment if negative
-- @return `self`
-- @override true
-- @see BaseAgent:give_reward
function class:give_reward(reward)
  -- Threshold the reward
  if self.max_reward then
    reward = math.min(reward, self.max_reward)
  end
  if self.min_reward then
    reward = math.max(reward, self.min_reward)
  end
  if self.rescale_r then
    self.r_max = math.max(self.r_max, reward)
  end

  self.experience_pool:record_reward(reward)

  if not self.evaluating then
    self.experienced_steps = self.experienced_steps + 1
  end

  if self.target_q and self.experienced_steps % self.target_q == 1 then
    self.target_network = self.inference.network:clone()
  end

  self:_learn()

  return self
end

--- Return how many interactions the agent lived.
-- @treturn number Number of interactions done
-- @override true
-- @see BaseAgent:get_experienced_interactions
function class:get_experienced_interactions()
  return self.experienced_steps
end

--- Return how many times the agent actually learned from its experience.
-- @treturn number Number of times the agent learned
-- @override true
-- @see BaseAgent:get_learned_epoch
function class:get_learned_epoch()
  return self.learning_epoch
end

--- Put the agent in a training mode.
--
-- This is the default mode of an agent.
-- @return `self`
-- @override true
-- @see BaseAgent:training
function class:training()
  self._logger:debug("Passing in TRAINING mode")
  self.evaluating = false
  self.experience_pool:pop()
  return self
end

--- Put the agent in an evaluation mode.
--
-- The agent will used a separated @{_ExperiencePool|experience pool}
-- and a dedicated epsilon value.
-- @return `self`
-- @override true
-- @see BaseAgent:evaluate
function class:evaluate()
  self._logger:debug("Passing in EVALUATING mode")
  self.evaluating = true
  self.experience_pool:push()
  return self
end

--- Dump the agent.
--
-- Redundant informations (like network parameters) are removed
-- to save space.
-- Tensors are converted to
-- @{torch.default_type/utility.md#string-torchgetdefaulttensortype/|default type}
-- to avoid GPU incompatibilities.
-- @tparam table cycles Set of already dumped objects
-- @treturn Dump A reloadable dump
-- @override true
-- @see ArcadesComponent:dump
function class:dump(cycles)
  local copy_table = require('pl.tablex').copy
  local default_type = torch.getdefaulttensortype()
  -- Save arguments and state variables
  local dump = super.dump(self, cycles)

  -- Convert tensors to CPU loadable ones
  dump.target_network = self.target_network:clone():type(default_type)
  dump.preprocess.network = dump.preprocess.network:clone():type(default_type)
  dump.inference.network = dump.inference.network:clone():type(default_type)
  dump.rmsprop.mean_square = dump.rmsprop.mean_square:type(default_type)
  dump.rmsprop.mean = dump.rmsprop.mean:type(default_type)
  dump.inference.parameters = nil
  dump.inference.grad_parameters = nil

  return dump
end

--- Private Methods
-- @section private-methods

--- Initialize the preprocessing network.
-- @tparam ClassArgument args
-- @tparam table observation_size Size of input tensor `{d, w, h}`
function class:_init_preprocessing_network(args, observation_size)
  local preprocess = {}
  if args and args.network then
    -- Reload a dumped agent
    preprocess = args
  elseif args then
    local network = require("arcades.network")
    if not network[args.class] then
      error(string.format(
        "Unable to find '%s' in network package",
        args.class
      ), 2)
    end

    args.params.input_size = args.params.input_size or observation_size
    preprocess.input_size = args.params.input_size
    preprocess.output_size = args.params.output_size
    preprocess.network = network[args.class](args.params)
    preprocess.output_size = preprocess.network:forward(
      torch.zeros(unpack(preprocess.input_size))
    ):size():totable()
  else
    -- Create an empty network
    preprocess.network = nn.Sequential()
    preprocess.input_size = observation_size
    preprocess.output_size = observation_size
  end
  preprocess.network = self._convert_tensor(preprocess.network)
  self.preprocess = preprocess
end

--- Initialize the main inference network.
-- @tparam ClassArgument args
-- @tparam[opt={}] Dump dump
function class:_init_inference_network(args, dump)
  args = args or {}
  -- Check that we reload a dump agent, or load a saved file or instantiate a class
  assert(args.network or args.file or args.class or dump, "No network was given to the agent.")
  local network = require("arcades.network")
  local inference

  if dump then
    -- Restore from a dumped agent (new architecture 2017-07-06)
    inference = dump
  elseif args.file then
    network = network.load_all() -- luacheck: no unused
    self._logger:info('Loading network from ' .. args.file)
    local status, result = pcall(torch.load, args.file)
    if not status then
      error(string.format(
        "Unable to load file '%s':\n\t%s",
        args.file, result
      ), 2)
    end
    inference = result
  else
    local params = args.params or {}
    params.input_size = params.input_size or self.experience_pool.history_stacked_state_size
    params.output_size = params.output_size or error("Network output size not provided.")
    inference = {
      input_size = params.input_size,
      output_size = params.output_size
    }
    if not network[args.class] then
      error(string.format(
        "No '%s' class founded in the 'network' package.",
        args.class
      ), 2)
    end
    self._logger:info('Creating network from network.' .. args.class)
    inference.network = network[args.class](args.params)
  end
  inference.network = self._convert_tensor(inference.network)
  inference.parameters, inference.grad_parameters = inference.network:getParameters()
  self.inference = inference
end

--- Learn from the past experiences.
--
-- This does nothing if agent is in @{evaluate|evaluating} mode.
-- @return `self`
function class:_learn()
  if not self.evaluating and
      self.experienced_steps > self.learn_start and
      self.experienced_steps % self.update_freq == 0 then

    --Do some Q-learning updates
    self.learning_epoch = self.learning_epoch + 1
    for _ = 1, self.n_replay do
      self:_qLearnMinibatch()
    end

  end
  return self
end

--- Get the action to execute according to an epsilon-greedy policy.
-- @param state Current state
-- @treturn number Choosed action
-- @mtodo Use dedicated classes for strategies.
function class:_eGreedy(state)
  if self.evaluating then
    self.ep = self.ep_eval
  else
    self.ep = (
      self.ep_end +
      math.max(
        0,
        (self.ep_start - self.ep_end) *
        (self.ep_endt - math.max(
          0,
          self.experienced_steps - self.learn_start
        )) /
        self.ep_endt
      )
    )
  end

  -- Epsilon greedy
  if torch.uniform() < self.ep then
    return torch.random(1, torch.Tensor(self.inference.output_size):prod())
  else
    return self:_greedy(state)
  end
end

--- Get the action to execute according to greedy policy.
-- @param state Current state
-- @treturn number Choosed action
function class:_greedy(state)
  -- Turn single state into minibatch. Needed for convolutional nets.
  if state:dim() == 2 then
    self._logger:warn('ConvNet input must be at least 3D. Adding a new dimension of size 1')
    state = state:resize(1, state:size(1), state:size(2))
  end

  state = self._convert_tensor(state)

  local q = self.inference.network:forward(state)
  q = q:reshape(q:nElement()) -- get Q-values of each action given state
  local maxq = q[1]
  local besta = {1}

  -- Evaluate all other actions (with random tie-breaking)
  for a = 2, torch.Tensor(self.inference.output_size):prod() do
    if q[a] > maxq then
      besta = { a }
      maxq = q[a]
    elseif q[a] == maxq then
      besta[#besta+1] = a
    end
  end

  local r = torch.random(1, #besta)
  return besta[r]
end

--- Compute expected action-values as targets of the neural network.
-- @tparam table args An interaction sample
-- @param args.s Initial state
-- @param args.a Action
-- @param args.r Reward
-- @param args.s2 Final state
-- @param args.t Is state final?
-- @treturn torch.Tensor/tensor.md/ Expected action-values
function class:_getQUpdate(args)
  local s = self._convert_tensor(args.s)
  local a = self._convert_tensor(args.a)
  local r = self._convert_tensor(args.r)
  local s2 = self._convert_tensor(args.s2)
  local t = self._convert_tensor(args.t)


  -- The order of calls to forward is a bit odd in order
  -- to avoid unnecessary calls (we only need 2).

  -- Compute the approximate target action-values (the best action-value for state s)
  -- (r + (1-terminal) * gamma * max_a Q(s2, a))
  -- i.e.
  -- r_j if episode ends at step j+1
  -- r_j + gamma * max_a Q(s_(j+1), a) otherwise

  -- Then compute delta (difference between the best action-value and the current one)
  -- delta = (r + (1-terminal) * gamma * max_a Q(s2, a)) - Q(s, a)

  t = t:clone():mul(-1):add(1)

  local target_q_net
  if self.target_q then
    target_q_net = self.target_network
  else
    target_q_net = self.inference.network
  end

  -- Compute max_a Q(s_2, a).
  local q2_max = target_q_net:forward(s2):max(3)

  -- Compute q2 = (1-terminal) * gamma * max_a Q(s2, a)
  local q2 = q2_max:clone():mul(self.discount):cmul(t)

  -- q = Q(s,a)
  local q_all = self.inference.network:forward(s)
  local q = self._convert_tensor(torch.Tensor(q_all:size(1)))
  for i=1,q_all:size(1) do
    q[i] = q_all[{i, {}, a[i]}]
  end

  local delta = r:clone()
  if self.rescale_r then
    delta:div(self.r_max)
  end
  delta:add(q2)
  delta:add(-1, q)
  if self.clip_delta then
    delta:clamp(-self.clip_delta, self.clip_delta)
  end

  local targets = self._convert_tensor(
    torch.zeros(a:size(1), unpack(self.inference.output_size))
  )
  for i = 1, a:size(1) do
    targets[{i, {}, a[i]}] = delta[i]
  end

  return targets
end

--- Apply Q-Learning on a @{self.minibatch_size|minibatch}.
function class:_qLearnMinibatch()
  -- Perform a minibatch Q-learning update:
  -- w += alpha * (r + gamma * max_a2 Q(s2,a2) - Q(s,a)) * dQ(s,a)/dw
  assert(self.experience_pool:size() > self.minibatch_size,
    string.format(
      "Minibatch size too high (%d) compared to available transitions (%d)",
      self.minibatch_size, self.experience_pool:size()
    )
  )

  -- Samples some interactions in the experience pool
  local s, a, r, s2, t = self.experience_pool:sample(self.minibatch_size)
  s = self._convert_tensor(s)
  a = self._convert_tensor(a)
  r = self._convert_tensor(r)
  s2 = self._convert_tensor(s2)
  t = self._convert_tensor(t)

  -- Computes approximate expected targets
  local targets = self:_getQUpdate({
    s=s, a=a, r=r, s2=s2, t=t
  })

  -- zero gradients of parameters
  self.inference.grad_parameters:zero()

  -- get new gradient
  self.inference.network:backward(s, targets)

  -- add weight cost to gradient
  self.inference.grad_parameters:add(-self.wc, self.inference.parameters)

  -- use gradients (based on RMSProp strategy)
  -- w = w + deltas
  -- delta = lr * dw/sqrt(MeanSquare(w, t) - Mean(w,t) ^ 2 + epsilon)
  -- MeanSquare(w,t) = decay * MeanSquare(w, t-1) + (1-decay) * dw ^ 2
  -- Mean(w,t) = decay * Mean(w, t-1) + (1-decay) * dw

  -- Update mean_square
  self.rmsprop.mean_square:mul(self.rmsprop.decay):addcmul(
    1 - self.rmsprop.decay,
    self.inference.grad_parameters, self.inference.grad_parameters
  )
  -- Update mean
  self.rmsprop.mean:mul(self.rmsprop.decay):add(
    1 - self.rmsprop.decay,
    self.inference.grad_parameters
  )

  -- Compute weights deltas
  local deltas = torch.cdiv(
    self.inference.grad_parameters,
    torch.sqrt(
      torch.add(
        torch.add(
          self.rmsprop.mean_square,
          -1,
          torch.pow(
            self.rmsprop.mean,
            2
          )
        ),
        self.rmsprop.mu
      )
    )
  ):mul(self.lr)

  -- Update weights
  self.inference.parameters:add(deltas)
end

return module.NeuralQLearner
