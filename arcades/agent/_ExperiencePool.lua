--- An experience pool for agents.
-- This class will memorize the experiences of an agent and return them when
-- required taking account of a possible history.
-- @classmod agent._ExperiencePool
-- @alias class
-- @inherit true
-- @see ArcadesComponent
-- @author Alexis BRENON <alexis.brenon@imag.fr>

local hash = require('hash')
local torch = require('torch')

local arcades = require('arcades')

local copy_table = require('pl.tablex').copy

local module = {}
local class, super = torch.class('_ExperiencePool', 'ArcadesComponent', module)

--- Data Types
-- @section data-types

--- Table used as arguments for the ExperiencePool @{__init|constructor}.
-- @tfield number pool_size Size of the experience pool
-- @tfield table state_size Size of the states `{d, w, h}`
-- @tfield number history_length Length of the history for inference
-- @tfield string history_type Type of history
-- @tfield number history_spacing Spacing in history
-- @table InitArguments

--- Serializable dump of an `_ExperiencePool`.
-- @table Dump

--- Tables used to record interactions/experiences.
-- @tfield number max_size Maximum number of saved experiences
-- @tfield number last_index Index of the last saved experience
-- @tfield {number,...} states The hashes of the recorded states
-- @tfield {number,...} terminals Is the states terminal (`1`) or not (`0`)
-- @tfield {number,...} actions Actions executed
-- @tfield {number,...} rewards Rewards received
-- @table Pool

--- @section end

--- Default constructor.
-- @tparam InitArguments args
-- @tparam Dump dump
function class:__init(args, dump)
  super.__init(self, args, dump)
  args = args or {}; dump = dump or {}
  local use_gpu = package.loaded["cutorch"]
  --- Function to convert tensors if necessary.
  --
  -- This function must be called to convert tensors/network to the appropriate
  -- format (CUDA or default Tensor type) to avoid computation errors caused by
  -- inconsistent types
  -- @tfield function self._convert_tensor
  self._convert_tensor = nil
  if use_gpu then
    self._convert_tensor = function(t) return t:cuda() end
  else
    local default_type = torch.getdefaulttensortype()
    self._convert_tensor = function(t) return t:type(default_type) end
  end

  --- Hash table to associate a hash (double) to a
  -- @{torch.Tensor/tensor.md/|Tensor} representing a state.
  -- @usage s = self.states[self.hasher:hash(s)]
  -- @tfield {[number]=torch.Tensor} self.states
  self.states = dump.states or {}
  self.states =  self._convert_states(self.states, self._convert_tensor)

  --- The number of elements of the hash table.
  -- @tfield integer self.hashed_states
  self.hashed_states = dump.hashed_states or 0
  --- A nil state (full of `0`) used in history.
  -- @tfield torch.Tensor/tensor.md/ self.nil_state
  self.nil_state = self._convert_tensor(
    dump.nil_state or
    torch.zeros(1):repeatTensor(
      table.unpack(
        (assert(
          args.state_size,
          "State size must be provided when initializing ExperiencePool"
        ))
      )
    )
  )
  --- Hasher object used to compute state hashes.
  -- @tfield hash.XXH64 self.hasher
  self.hasher = hash.XXH64(0)
  self._no_dump_list.hasher = true

  --- Actual pool.
  -- @tfield Pool self.pool
  if dump.pool then
    -- Restore a dumped pool
    self.pool = dump.pool
  else
    self.pool = {}
    self.pool.max_size = args.pool_size or 4096
    self:clear()
  end
  --- @{push|Pushed} pools that can be restore by successive calls to `pop`.
  -- @tfield {Pool,...} self.pushed_pools
  self.pushed_pools = dump.pushed_pools or {}

  --- Number of states in a full historic state.
  -- @tfield number self.history_length
  self.history_length = args.history_length or 1
  --- Function to compute indexes of the historic state.
  -- @see _compute_history_offsets
  -- @tfield string self.history_type
  self.history_type = args.history_type or "linear"
  --- Parameter of the `history_type` function.
  -- @see _compute_history_offsets
  -- @tfield number self.history_spacing
  self.history_spacing = args.history_spacing or 1
  --- Offsets of the states to add to las one,
  -- when fetching a full historic state.
  -- @tfield table self.history_offsets
  self.history_offsets = self:_compute_history_offsets()

  --- Size of a full historic state.
  -- @tfield {number,...} self.history_stacked_state_size
  self.history_stacked_state_size = self.nil_state:size():totable()
  self.history_stacked_state_size[1] = self.history_stacked_state_size[1] * self.history_length
end

--- Private Methods
-- @section private-methods

--- Fill in @{history_offsets}.
--
-- This function will compute the offsets of the states to retrieve to build a
-- full historic state. The offsets depend on the @{history_type} and the
-- @{history_spacing}.
--
-- if @{history_type} is `'linear'` :<br/>
-- `offset[i] = history_spacing * i, ∀ i ∈ [1, history_length-1]`
--
-- if @{history_type} is `'exp'` :<br/>
-- `offset[i] = history_spacing ^ i, ∀ i ∈ [1, history_length-1]`
--
-- @treturn torch.IntTensor/tensor.md/ @{self.history_offsets}
function class:_compute_history_offsets()
  local history_length = self.history_length
  local history_spacing = self.history_spacing

  local index_func
  if self.history_type == "linear" then
    index_func = function(i) return history_spacing * i end
  elseif self.history_type == "exp" then
    index_func = function(i) return history_spacing ^ i end
  end

  self.history_offsets = {}
  for i = 1, history_length-1 do
    self.history_offsets[i] = index_func(i)
  end
  return self.history_offsets
end

--- Shift a given index according to `last_index`.
--
-- This function is used to manage a circular memory.
-- `history_offsets` are computed according to a `0` indexed
-- array. `Pool` arrays are circular and
-- use a `last_index` to point the initial element. Thus
-- we need a small computation to shift the offsets dynamicly.
-- @tparam number index `0` based index
-- @treturn number A `last_index` based index
function class:_shift_index(index)
  return (
    (
      (
        self.pool.last_index -
        (index - 1) -
        (self.pool.max_size + 1)
      ) %
      self.pool.max_size
    ) +
    1
  )
end

--- Return an index where to sample a non-terminal state.
-- @treturn number Index of the sample in the pool
function class:_get_sampling_index()
  local index
  while true do
    index = torch.random(2, self:size())
    if self.pool.terminals[self:_shift_index(index)] == 0 then
      break;
    end
  end
  return index
end

--- Remove useless states from `states` hash map to reduce memory size.
-- @return `self`
function class:_clean_states()
  -- Make a list of useful states
  local hashes = {}
  for _, state_hash in ipairs(self.pool.states) do
    hashes[state_hash] = true
  end
  for _, pool in ipairs(self.pushed_pools) do
    for _, state_hash in ipairs(pool[1]) do
      hashes[state_hash] = true
    end
  end

  -- Make a list of unused states
  local to_delete = {}
  for state_hash, _ in pairs(self.states) do
    if not hashes[state_hash] then table.insert(to_delete, state_hash) end
  end

  -- Delete useless states
  for _, state_hash in ipairs(to_delete) do
    self.states[state_hash] = nil
    self.hashed_states = self.hashed_states - 1
  end

  return self
end

--- Convert `states` according to the given function `f`.
--
-- This is used to convert the states to GPU or CPU Tensors.
-- @tparam {[number]=torch.Tensor} states States hash map
-- @tparam function f Function used to convert Tensors
-- @treturn {[number]=torch.Tensor} A new hash map with converted Tensors
function class._convert_states(states, f)
  local result = {}
  for h, s in pairs(states) do
    result[h] = f(s);
  end
  return result
end

--- Public Methods
-- @section public-methods

--- Record a state in the pool.
-- @tparam torch.Tensor/tensor.md/ s The state
-- @tparam boolean t Is the state terminal?
-- @return `self`
function class:record_state(s, t)
  assert(s, "State cannot be nil")

  -- Save the state if not already encountered
  local state_hash = self.hasher:hash(s)
  if not self.states[state_hash] then
    if self.hashed_states > 1.5*self.pool.max_size*(#self.pushed_pools+1) then
      self:_clean_states()
    end
    self.states[state_hash] = self._convert_tensor(s:clone())
    self.hashed_states = self.hashed_states + 1
  end

  self.pool.last_index = (self.pool.last_index % self.pool.max_size) + 1

  self.pool.states[self.pool.last_index] = state_hash
  self.pool.terminals[self.pool.last_index] = (t and 1) or 0
  self.pool.actions[self.pool.last_index] = 0
  self.pool.rewards[self.pool.last_index] = 0

  return self
end

--- Record an action in the pool.
--
-- This function is intended to be called after @{record_state} to record the
-- action executed for the last recorded state.
-- @tparam number a The action index
-- @return `self`
function class:record_action(a)
  assert(a, 'Action cannot be nil')
  assert(self.pool.actions[self.pool.last_index] == 0, "You must record a state before recording an action.")

  self.pool.actions[self.pool.last_index] = a

  return self
end

--- Record a reward in the pool.
--
-- This function is intended to be called after @{record_state} and @{record_action}
-- to record the received reward for last action executed in last state.
-- @tparam number r The reward
-- @return `self`
function class:record_reward(r)
  assert(r, "Reward cannot be nil")
  assert(self.pool.rewards[self.pool.last_index] == 0, "You must record a state before recording a reward.")

  self.pool.rewards[self.pool.last_index] = r

  return self
end

--- Return a full historic state.
-- @tparam[opt=1] number index Index of the state to get (1 is last recorded state)
-- @treturn torch.Tensor/tensor.md/ A full historic state, history stacked on first dimension
-- @treturn boolean Is the returned state terminal?
function class:get_state(index)
  index = index or 1
  local full_historic_state_size = torch.LongStorage({
    self.history_length,
    table.unpack(self.nil_state:size():totable())
  })
  local full_historic_state = self._convert_tensor(torch.Tensor(full_historic_state_size))

  -- Fetch the asked state
  full_historic_state[1] = self.states[self.pool.states[self:_shift_index(index)]]
  -- Then walk through historic ones
  local useless_history = false
  for i, offset in ipairs(self.history_offsets) do
    local history_index = index + offset
    -- Add a state to the historic state if its index is in the pool AND
    if history_index <= self:size() then
      -- Old state are from the same episode
      local last_history_index = (i > 1 and self.history_offsets[i-1]) or index
      for j = last_history_index+1, history_index do
        if self.pool.terminals[self:_shift_index(j)] == 1 then
          useless_history = true
          break
        end
      end
    else
      useless_history = true
    end

    if not useless_history then
      local state_hash = self.pool.states[self:_shift_index(history_index)]
      full_historic_state[i+1] = self.states[state_hash]
    else
      -- If historic states are irrelevant, fill with empty states
      for j = i+1, self.history_length do
        full_historic_state[j] = self.nil_state
      end
      break
    end
  end

  return full_historic_state:view(table.unpack(self.history_stacked_state_size)), (self:get_terminal(index) == 1)
end

--- Get the terminal signal of a record.
-- @tparam[opt=1] number index Index of the record to look for (1 is last recorded state)
-- @treturn boolean Is recorded state terminal?
function class:get_terminal(index)
  index = index or 1
  return self.pool.terminals[self:_shift_index(index)]
end

--- Get the action of a record.
-- @tparam[opt=1] number index Index of the record to look for (1 is last recorded state)
-- @treturn number Action executed/recorded
function class:get_action(index)
  index = index or 1
  return self.pool.actions[self:_shift_index(index)]
end

--- Get the reward of a record.
-- @tparam[opt=1] number index Index of the record to look for (1 is last recorded state)
-- @treturn number Reward obtained/recorded
function class:get_reward(index)
  index = index or 1
  return self.pool.rewards[self:_shift_index(index)]
end

--- Return samples from the experience pool.
--
-- Samples returned don't start with terminal states
-- @tparam[opt=1] integer batch_size Number of samples to return
-- @treturn torch.Tensor/tensor.md/ `batch_size` states
-- @treturn torch.Tensor/tensor.md/ `batch_size` actions
-- @treturn torch.Tensor/tensor.md/ `batch_size` rewards
-- @treturn torch.Tensor/tensor.md/ `batch_size` final states
-- @treturn torch.Tensor/tensor.md/ `batch_size` final states terminal signal
function class:sample(batch_size)
  batch_size = batch_size or 1
  assert(batch_size <= self:size()-1,
    string.format(
      "Trying to fetch %d samples (needing at least %d interactions) from a pool of size %d",
      batch_size, batch_size+1, self:size()
    )
  )

  -- Get the size to allocate for states batch (with history)
  local states_batch_size = torch.LongStorage({
    batch_size,
    table.unpack(self:get_state():size():totable())
  })
  local s = self.nil_state.new(states_batch_size)
  local a = self._convert_tensor(torch.Tensor(batch_size))
  local r = self._convert_tensor(torch.Tensor(batch_size))
  local s2 = self.nil_state.new(states_batch_size)
  local t = self._convert_tensor(torch.Tensor(batch_size))

  for i = 1, batch_size do
    local index = self:_get_sampling_index()

    s[i] = self:get_state(index) -- Old state
    a[i] = self:get_action(index) -- Old action
    r[i] = self:get_reward(index) -- Old reward
    s2[i] = self:get_state(index-1) -- New state
    t[i] = self:get_terminal(index-1) -- New terminal
  end

  return s, a, r, s2, t
end

--- Get the size of the experience pool.
-- @treturn number `self.pool.max_size`
-- @mtodo Check which size is actually returned (current, max?)
function class:size()
  return #self.pool.rewards
end

--- Clear the current pool (forget all what you experienced).
-- @return `self`
function class:clear()
  self.pool.states = {}
  self.pool.terminals = {}
  self.pool.actions = {}
  self.pool.rewards = {}
  self.pool.last_index = 0
end

--- Push the current pool.
--
-- This allow you to save the current pool and to restore it later using @{pop}.
-- @return `self`
function class:push()
  table.insert(self.pushed_pools, {
    copy_table(self.pool.states),
    copy_table(self.pool.terminals),
    copy_table(self.pool.actions),
    copy_table(self.pool.rewards),
    self.pool.last_index
  })
  return self
end

--- Restore a saved pool.
--
-- Restore a pool (if any) saved by a previous @{push} call.
-- @return `self`
function class:pop()
  if #self.pushed_pools > 0 then
    self.pool.states,
      self.pool.terminals,
      self.pool.actions,
      self.pool.rewards,
      self.pool.last_index = table.unpack(table.remove(self.pushed_pools))
  end
  return self
end

--- Dump current state of `_ExperiencePool`.
-- 
-- @{torch.Tensor/tensor.md/|Tensors} are converted to CPU tensors if
-- necessary
-- @tparam[opt={}] table cycles Already dumped components
-- @treturn Dump
-- @override true
-- @see ArcadesComponent:dump
function class:dump(cycles)
  self:_clean_states()
  local dump = super.dump(self, cycles)
  local default_type = torch.getdefaulttensortype()
  local cpu_converter = function(s) return s:type(default_type) end
  dump.states = self._convert_states(self.states, cpu_converter)
  dump.nil_state = cpu_converter(self.nil_state)
  return dump
end


return module._ExperiencePool
