#! /usr/bin/env th

--- A small configuration to check that an experiment works.
-- @script test.lua
-- @author Alexis BRENON <brenon.alexis@gmail.com>

local paths = require('paths')

local args = {
  -- Torch
  torch = {
    gpu = arg[1] and (arg[1] == "gpu" and tonumber(arg[2])) or -1,
    threads = 4,
    seed = 1,
  },

  -- Output
  output = {
    path = paths.concat("outputs", "test"),
    log_level = 10,
    save_freq = 50,
    profiling = true,
  },

  -- Environments
  training_environment = {
    class = "smarthome.sweethome.GraphicalSensorSweetHome",
    params = {
      map_path = paths.concat("assets", "domus.svg"),
      data_path = paths.concat("data", "SweetHomeRaw"),
      input_time_scale = 0.001,
      vocal_based = true,
      history_length = 3
    },
  },

  -- Agent
  agent = {
    class = "NeuralQLearner",
    params = {
      -- Networks
      preprocess = {
        class = "Downsample",
        params = {}
      },
      inference = {
        class = "Inference",
        params = {}
      },
      --  Experience pool params
      memory = {
        pool_size = 10,
        history_length = 3,
        history_type = 'linear',
        history_spacing = 1
      },

      learn_start = 5,
      update_freq = 1,
      minibatch_size = 2,
      n_replay = 1,
      rescale_r = false,

      max_reward = 1,
      min_reward = -1,

      ep_start = 1,
      ep_end = 0.1,
      ep_endt = 100,
      ep_eval = 0.05,

      lr = 0.01,

      discount = 0.99,
      clip_delta = 1,
      target_q = 5,
      wc = 0,
    },
  },

  -- Experiment
  experiment = {
    class = "BaseExperiment",
    params = {
      steps = 100,
      eval_freq = 20,
      eval_steps = 5,
    },
  },
}

require('run')(args)
